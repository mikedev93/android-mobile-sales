package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class UsuariosContract {

    class UsuariosEntry : BaseColumns {
        companion object {
            const val TABLE_NAME: String = "musuarios"

            const val _ID: String = BaseColumns._ID
            const val ID_USUARIO: String = "idusuario"
            const val NOMBRE_USUARIO: String = "nombre_usuario"
            const val APELLIDO_NOMBRES: String = "apellido_nombres"
            const val CONTRASENIA: String = "contrasenia"
            const val FECHA_EXPIRA: String = "fechaexpira"
            const val ESTADO: String = "estado"
            const val ID_ENTIDAD_SUCURSAL: String = "id_entidad_sucursal"
        }
    }
}
