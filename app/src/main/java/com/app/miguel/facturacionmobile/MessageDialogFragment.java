package com.app.miguel.facturacionmobile;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;


public class MessageDialogFragment extends DialogFragment {
    public interface MessageDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
    }

    private String mTitle;
    private String mMessage;
    private MessageDialogListener mListener;
    private String mButtonText;
    private Resources resources;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
        resources = getActivity().getResources();
    }

    public static MessageDialogFragment newInstance(String title, String message, MessageDialogListener listener) {
        MessageDialogFragment fragment = new MessageDialogFragment();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mListener = listener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(mMessage)
                .setCancelable(false)
                .setTitle(mTitle);

        if (mTitle.equals(resources.getString(R.string.dialog_error_title))){
            mButtonText = resources.getString(R.string.dialog_error_button);
        } else {
            mButtonText = resources.getString(R.string.dialog_success_button);
        }

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (!mTitle.equals(resources.getString(R.string.dialog_error_title))){
                    getActivity().finish();
                }
            }
        });

        builder.setPositiveButton(mButtonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (mListener != null) {
                    if (mTitle.equals(resources.getString(R.string.dialog_error_title))){
                        mListener.onDialogPositiveClick(MessageDialogFragment.this);
                    } else {
                        getActivity().finish();
                    }
                }
            }
        });

        return builder.create();
    }
}