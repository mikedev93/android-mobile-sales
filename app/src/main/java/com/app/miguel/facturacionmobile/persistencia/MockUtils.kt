package com.app.miguel.facturacionmobile.persistencia
/*
 * Clase con utilidades para generar todos los flujos para DEMO
 */

import com.app.miguel.facturacionmobile.persistencia.models.*
import com.google.gson.Gson


class MockUtils {

    companion object {

        fun getEntidades(): ListEntidad {
            var entidades: ListEntidad = Gson().fromJson(entidadesMock, ListEntidad::class.java)
            return entidades
        }

        fun getEntidadesSucursal(): ListEntidadSucursal {
            var entidadesSucursales: ListEntidadSucursal = Gson().fromJson(entidadesSucursalMock, ListEntidadSucursal::class.java)
            return entidadesSucursales
        }

        fun getProductosMock(): ListProductStock {
            var productos = Gson().fromJson(productosMock, ListProductStock::class.java)
            return productos
        }

        fun getParametrosVenta(): ParametroVentaModel {
            var parametroVenta = Gson().fromJson(parametrosMock, ListParametroVenta::class.java)
            return parametroVenta.parametros[0]
        }

        const val entidadesMock = "{\n" +
                "    \"entidades\": [\n" +
                "        {\n" +
                "            \"dv\": null,\n" +
                "            \"ruc\": \"80082805-4\",\n" +
                "            \"identidad\": 1198,\n" +
                "            \"altausuario\": null,\n" +
                "            \"identidad_m\": 1,\n" +
                "            \"razonsocial\": \"CENTRO COMERCIAL GUARANI\",\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"CENTRO COMERCIAL GUARANI\",\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"dv\": null,\n" +
                "            \"ruc\": \"1260959-5\",\n" +
                "            \"identidad\": 1841,\n" +
                "            \"altausuario\": null,\n" +
                "            \"identidad_m\": 2,\n" +
                "            \"razonsocial\": \"FULVIA PINTOS DE CAÑETE (COMERCIAL CAÑETE)\",\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"FULVIA PINTOS DE CAÑETE (COMERCIAL CAÑETE)\",\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"dv\": null,\n" +
                "            \"ruc\": \"2239002-2\",\n" +
                "            \"identidad\": 1889,\n" +
                "            \"altausuario\": null,\n" +
                "            \"identidad_m\": 3,\n" +
                "            \"razonsocial\": \"GLORIA MARTINEZ(COMERCIAL SANTA TERESA)\",\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"GLORIA MARTINEZ(COMERCIAL SANTA TERESA)\",\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"dv\": null,\n" +
                "            \"ruc\": \"2344073-2\",\n" +
                "            \"identidad\": 5485,\n" +
                "            \"altausuario\": null,\n" +
                "            \"identidad_m\": 4,\n" +
                "            \"razonsocial\": \"ALDO ROLON\",\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"ALDO ROLON\",\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"dv\": null,\n" +
                "            \"ruc\": \"80078857-5\",\n" +
                "            \"identidad\": 5487,\n" +
                "            \"altausuario\": null,\n" +
                "            \"identidad_m\": 5,\n" +
                "            \"razonsocial\": \"COMERCIAL TAVA´I S.A.\",\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"COMERCIAL TAVA´I S.A.\",\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"dv\": null,\n" +
                "            \"ruc\": \"183202-5\",\n" +
                "            \"identidad\": 5490,\n" +
                "            \"altausuario\": null,\n" +
                "            \"identidad_m\": 6,\n" +
                "            \"razonsocial\": \"BRIGIDO LOPEZ CACERES\",\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"BRIGIDO LOPEZ CACERES\",\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"dv\": null,\n" +
                "            \"ruc\": \"3172756-5\",\n" +
                "            \"identidad\": 5491,\n" +
                "            \"altausuario\": null,\n" +
                "            \"identidad_m\": 7,\n" +
                "            \"razonsocial\": \"COMERCIAL ARMANDO\",\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"COMERCIAL ARMANDO\",\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"dv\": null,\n" +
                "            \"ruc\": \"4030550-3\",\n" +
                "            \"identidad\": 5492,\n" +
                "            \"altausuario\": null,\n" +
                "            \"identidad_m\": 8,\n" +
                "            \"razonsocial\": \"COMERCIAL EMANUEL\",\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"COMERCIAL EMANUEL\",\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"dv\": null,\n" +
                "            \"ruc\": \"1768364-5\",\n" +
                "            \"identidad\": 5493,\n" +
                "            \"altausuario\": null,\n" +
                "            \"identidad_m\": 9,\n" +
                "            \"razonsocial\": \"JUANA CRISTALDO\",\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"JUANA CRISTALDO\",\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"dv\": null,\n" +
                "            \"ruc\": \"875467-5\",\n" +
                "            \"identidad\": 5494,\n" +
                "            \"altausuario\": null,\n" +
                "            \"identidad_m\": 10,\n" +
                "            \"razonsocial\": \"VILDA OVANDO DE AGUILAR\",\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"VILDA OVANDO DE AGUILAR\",\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        }\n" +
                "    ]\n" +
                "}"

        const val entidadesSucursalMock = "{\n" +
                "    \"entidad_sucursales\": [\n" +
                "        {\n" +
                "            \"barrio\": 0,\n" +
                "            \"ciudad\": 0,\n" +
                "            \"estado\": null,\n" +
                "            \"entidad\": 1198,\n" +
                "            \"telefono\": \"\",\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"direccion\": \"\",\n" +
                "            \"altausuario\": null,\n" +
                "            \"tipocliente\": null,\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"CENTRO COMERCIAL GUARANI S. A.\",\n" +
                "            \"identidadsucursal\": 1158,\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"barrio\": 0,\n" +
                "            \"ciudad\": 0,\n" +
                "            \"estado\": null,\n" +
                "            \"entidad\": 1841,\n" +
                "            \"telefono\": \"\",\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"direccion\": \"\",\n" +
                "            \"altausuario\": null,\n" +
                "            \"tipocliente\": null,\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"FULVIA PINTOS DE CAÑETE (COMERCIAL CAÑETE)\",\n" +
                "            \"identidadsucursal\": 1800,\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"barrio\": 0,\n" +
                "            \"ciudad\": 0,\n" +
                "            \"estado\": null,\n" +
                "            \"entidad\": 1889,\n" +
                "            \"telefono\": \"\",\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"direccion\": \"\",\n" +
                "            \"altausuario\": null,\n" +
                "            \"tipocliente\": null,\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"GLORIA MARTINEZ(COMERCIAL SANTA TERESA)\",\n" +
                "            \"identidadsucursal\": 1848,\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"barrio\": 0,\n" +
                "            \"ciudad\": 0,\n" +
                "            \"estado\": null,\n" +
                "            \"entidad\": 5485,\n" +
                "            \"telefono\": \"\",\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"direccion\": \"\",\n" +
                "            \"altausuario\": null,\n" +
                "            \"tipocliente\": null,\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"ALDO ROLON\",\n" +
                "            \"identidadsucursal\": 5029,\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"barrio\": 0,\n" +
                "            \"ciudad\": 0,\n" +
                "            \"estado\": null,\n" +
                "            \"entidad\": 5487,\n" +
                "            \"telefono\": \"\",\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"direccion\": \"\",\n" +
                "            \"altausuario\": null,\n" +
                "            \"tipocliente\": null,\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"COMERCIAL TAVA´I S.A.\",\n" +
                "            \"identidadsucursal\": 5031,\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"barrio\": 0,\n" +
                "            \"ciudad\": 0,\n" +
                "            \"estado\": null,\n" +
                "            \"entidad\": 5490,\n" +
                "            \"telefono\": \"\",\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"direccion\": \"\",\n" +
                "            \"altausuario\": null,\n" +
                "            \"tipocliente\": null,\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"BRIGIDO LOPEZ CACERES\",\n" +
                "            \"identidadsucursal\": 5032,\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"barrio\": 0,\n" +
                "            \"ciudad\": 0,\n" +
                "            \"estado\": null,\n" +
                "            \"entidad\": 5491,\n" +
                "            \"telefono\": \"\",\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"direccion\": \"\",\n" +
                "            \"altausuario\": null,\n" +
                "            \"tipocliente\": null,\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"COMERCIAL ARMANDO\",\n" +
                "            \"identidadsucursal\": 5033,\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"barrio\": 0,\n" +
                "            \"ciudad\": 0,\n" +
                "            \"estado\": null,\n" +
                "            \"entidad\": 5492,\n" +
                "            \"telefono\": \"\",\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"direccion\": \"\",\n" +
                "            \"altausuario\": null,\n" +
                "            \"tipocliente\": null,\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"COMERCIAL EMANUEL\",\n" +
                "            \"identidadsucursal\": 5034,\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"barrio\": 0,\n" +
                "            \"ciudad\": 0,\n" +
                "            \"estado\": null,\n" +
                "            \"entidad\": 5493,\n" +
                "            \"telefono\": \"\",\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"direccion\": \"\",\n" +
                "            \"altausuario\": null,\n" +
                "            \"tipocliente\": null,\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"JUANA CRISTALDO\",\n" +
                "            \"identidadsucursal\": 5035,\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"barrio\": 0,\n" +
                "            \"ciudad\": 0,\n" +
                "            \"estado\": null,\n" +
                "            \"entidad\": 5494,\n" +
                "            \"telefono\": \"\",\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"direccion\": \"\",\n" +
                "            \"altausuario\": null,\n" +
                "            \"tipocliente\": null,\n" +
                "            \"altafechahora\": null,\n" +
                "            \"nombrecomercial\": \"VILDA OVANDO DE AGUILAR\",\n" +
                "            \"identidadsucursal\": 5036,\n" +
                "            \"idpedido_remision\": 120809,\n" +
                "            \"modificadousuario\": null,\n" +
                "            \"modificadofechahora\": null\n" +
                "        }\n" +
                "    ]\n" +
                "}"

        const val productosMock = "{\n" +
                "    \"stock\": [\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 40,\n" +
                "            \"codigo\": \"2030403\",\n" +
                "            \"nombre\": \"WAFER CLASICO 45 GR X 24 UNI\",\n" +
                "            \"id\": 101,\n" +
                "            \"presentacion\": \"Caja x 24 unid\",\n" +
                "            \"unidadMedida\": \"CJ\",\n" +
                "            \"precioFabrica\": 14500,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        },\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 75,\n" +
                "            \"codigo\": \"2030401\",\n" +
                "            \"nombre\": \"WAFER CHOCOLATE 45 GR X 24 UNI\",\n" +
                "            \"id\": 102,\n" +
                "            \"presentacion\": \"Caja x 24 unid\",\n" +
                "            \"unidadMedida\": \"CJ\",\n" +
                "            \"precioFabrica\": 14500,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        },\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 45,\n" +
                "            \"codigo\": \"2030402\",\n" +
                "            \"nombre\": \"WAFER FRUTILLA 45 GR X 24 UNI\",\n" +
                "            \"id\": 103,\n" +
                "            \"presentacion\": \"Caja x 24 unid\",\n" +
                "            \"unidadMedida\": \"CJ\",\n" +
                "            \"precioFabrica\": 14500,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        },\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 2,\n" +
                "            \"codigo\": \"2030901\",\n" +
                "            \"nombre\": \"WAFER CHOCOLATE 90 GR X 24 UNI\",\n" +
                "            \"id\": 243,\n" +
                "            \"presentacion\": \"CAJA X 24 UNIDADES\",\n" +
                "            \"unidadMedida\": \"CJ\",\n" +
                "            \"precioFabrica\": 27400,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        },\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 130,\n" +
                "            \"codigo\": \"2020724\",\n" +
                "            \"nombre\": \"GALL RELL 75 GRS X 28 UNI - LIMON\",\n" +
                "            \"id\": 283,\n" +
                "            \"presentacion\": \"FARDO X 28 UNIDAD\",\n" +
                "            \"unidadMedida\": \"FR\",\n" +
                "            \"precioFabrica\": 18000,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        },\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 210,\n" +
                "            \"codigo\": \"2020723\",\n" +
                "            \"nombre\": \"GALL RELL 75 GRS X 28 UNI - VAINILLA\",\n" +
                "            \"id\": 284,\n" +
                "            \"presentacion\": \"FARDO X 28 UNIDAD\",\n" +
                "            \"unidadMedida\": \"FR\",\n" +
                "            \"precioFabrica\": 18000,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        },\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 185,\n" +
                "            \"codigo\": \"2020722\",\n" +
                "            \"nombre\": \"GALL RELL 75 GRS X 28 UNI - FRUTILLA\",\n" +
                "            \"id\": 286,\n" +
                "            \"presentacion\": \"FARDO X 28 UNIDAD\",\n" +
                "            \"unidadMedida\": \"FR\",\n" +
                "            \"precioFabrica\": 18000,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        },\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 380,\n" +
                "            \"codigo\": \"2020721\",\n" +
                "            \"nombre\": \"GALL RELL 75 GRS X 28 UNI - CHOCOLATE\",\n" +
                "            \"id\": 288,\n" +
                "            \"presentacion\": \"FARDO X 28 UNIDAD\",\n" +
                "            \"unidadMedida\": \"FR\",\n" +
                "            \"precioFabrica\": 18000,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        },\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 25,\n" +
                "            \"codigo\": \"2020101\",\n" +
                "            \"nombre\": \"GALL RELL 115 GRS X 18 UNI - CHOCOLATE\",\n" +
                "            \"id\": 292,\n" +
                "            \"presentacion\": \"FARDO X 18 UNIDAD\",\n" +
                "            \"unidadMedida\": \"FR\",\n" +
                "            \"precioFabrica\": 16500,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        },\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 115,\n" +
                "            \"codigo\": \"2020102\",\n" +
                "            \"nombre\": \"GALL RELL 115 GRS X 18 UNI - FRUTILLA\",\n" +
                "            \"id\": 298,\n" +
                "            \"presentacion\": \"FARDO X 18 UNIDAD\",\n" +
                "            \"unidadMedida\": \"FR\",\n" +
                "            \"precioFabrica\": 16500,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        },\n" +
                "        {\n" +
                "            \"iva\": 10,\n" +
                "            \"stock\": 6,\n" +
                "            \"codigo\": \"2020303\",\n" +
                "            \"nombre\": \"GALL RELL 30 GRS X 48 UNI- VAINILLA\",\n" +
                "            \"id\": 95,\n" +
                "            \"presentacion\": \"30 GRS X 48 UNI\",\n" +
                "            \"unidadMedida\": \"FR\",\n" +
                "            \"precioFabrica\": 13500,\n" +
                "            \"idPedidoRemision\": 120809\n" +
                "        }\n" +
                "    ]\n" +
                "}"

        const val parametrosMock = "{\n" +
                "    \"parametros\": [\n" +
                "        {\n" +
                "            \"vendedor\": 5019,\n" +
                "            \"nroIniFcauto\": 20050001252,\n" +
                "            \"idPedidoRemision\": 120809,\n" +
                "            \"nroDesdeFcmanual\": 20040001000,\n" +
                "            \"nroHastaFcmanual\": 20040001000,\n" +
                "            \"id\": 1\n" +
                "        }\n" +
                "    ]\n" +
                "}"
    }
}
