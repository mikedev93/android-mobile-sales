package com.app.miguel.facturacionmobile

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.servicios.tasks.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_sync.*

class SyncActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sync)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

        initViews()
    }

    fun initViews() {
        btn_download.setOnClickListener {
            Utils.hideSoftKeyboard(this)
            var parametros = DBUtils.getSaleParameters(applicationContext)
            input_layout_remision.isErrorEnabled = false
            if (et_remision.text!!.isEmpty()) {
                input_layout_remision.error = "¡Ingrese un ID Operativo Venta!"
            } else if (parametros != null && et_remision.text.toString().toInt() == parametros.idpedido_remision) {
                showSnackbarError("¡Ya se descargó ese Operativo de Venta!")
            } else {
                var cantidad = DBUtils.getSalesNotSyncd(applicationContext)
                if (cantidad > 0) {
                    showSnackbarError("¡No se puede descargar!\nTiene ventas sin enviar")
                } else if (cantidad == 0 && parametros != null && parametros.idpedido_remision != et_remision.text.toString().toInt()) {
                    var alertDialog = AlertDialog.Builder(this, R.style.ThemeOverlay_AppCompat_Dialog_Alert).create()
                    alertDialog.setTitle("Descargar Nuevo Operativo Venta")
                    alertDialog.setMessage("Esta a punto de descargar un nuevo Operativo de Venta.\nEsto borrará todos los datos ya existentes en la aplicación y descargará nuevos.\n\n¿Esta seguro?")
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
                            "Estoy seguro",
                            DialogInterface.OnClickListener { dialog, which ->
                                if (which == DialogInterface.BUTTON_POSITIVE) {
                                    syncParameters(et_remision.text.toString().toInt())
                                    dialog.dismiss()
                                }
                            })
                    alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                            "Cancelar",
                            DialogInterface.OnClickListener { dialog, which ->
                                if (which == DialogInterface.BUTTON_NEGATIVE) {
                                    dialog.dismiss()
                                }
                            })
                    alertDialog.show()
                } else {
                    syncParameters(et_remision.text.toString().toInt())
                }
            }
        }

        btn_upload.setOnClickListener {
            Utils.hideSoftKeyboard(this)
            syncNewEntidades()
        }

        back_button.setOnClickListener {
            Utils.hideSoftKeyboard(this)
            this.finish()
        }
    }

    fun syncParameters(idRemision: Int) {
        showLoading(true)
        var task = SyncParametersTask(applicationContext, this, idRemision)
        task.execute()
    }

    fun syncProductos(idRemision: Int) {
        var productsTask = SyncProductsTask(applicationContext, this, idRemision)
        productsTask.execute()
    }

    fun syncEntidades() {
        var task = SyncEntidadesTask(applicationContext, this)
        task.execute()
    }

    fun syncCiudades() {
        var task = SyncGeoCiudadesTask(applicationContext, this)
        task.execute()
    }

    fun syncNewEntidades() {
        showLoading(true)
        var task = SyncNewEntidadesTask(applicationContext, this)
        task.execute()
    }

    fun syncVentas() {
        var task = SyncVentasTask(applicationContext, this)
        task.execute()
    }

    fun showSnackbarError(message: String) {
        Utils.showSnackbar(applicationContext, findViewById(R.id.rl_main), message, Utils.TYPE_ERROR)
    }

    fun showSnackbar(message: String) {
        Utils.showSnackbar(applicationContext, findViewById(R.id.rl_main), message, Utils.TYPE_SYNC)
    }

    fun showSnackbarGoHome(message: String) {
        var callback: Snackbar.Callback = object : Snackbar.Callback() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                goHome()
            }
        }
        Utils.showSnackbar(applicationContext, findViewById(R.id.rl_main), message, Utils.TYPE_SYNC, callback)
    }

    fun showLoading(show: Boolean) {
        rl_curtain.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun goHome(){
        Thread.sleep(50)
        this.finish()
    }
}
