package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class EntidadesSucursalesContract {

    class EntidadSucursalesEntry : BaseColumns {
        companion object {
            const val TABLE_NAME: String = "mentidadsucursales"

            const val _ID = BaseColumns._ID
            const val ID_ENTIDAD_SUCURSAL_M: String = "identidadsucursal_m"
            const val ID_ENTIDAD_SUCURSAL: String = "identidadsucursal"
            const val ENTIDAD: String = "entidad"
            const val RAZON_SOCIAL: String = "razon_social"
            const val RUC: String = "ruc"
            const val NOMBRE_COMERCIAL: String = "nombrecomercial"
            const val TIPO_CLIENTE: String = "tipocliente"
            const val VENDEDOR: String = "vendedor"
            const val ESTADO: String = "estado"
            const val DEPARTAMENTO: String = "departamento"
            const val CIUDAD: String = "ciudad"
            const val BARRIO: String = "barrio"
            const val DIRECCION: String = "direccion"
            const val TELEFONO: String = "telefono"
            const val ID_PEDIDO_REMISION: String = "idpedido_remision"
            const val ALTA_FECHA_HORA: String = "altafechahora"
            const val ALTA_USUARIO: String = "altausuario"
            const val MODIFICADO_FECHA_HORA: String = "modificadofechahora"
            const val MODIFICADO_USUARIO: String = "modificadousuario"
        }
    }
}