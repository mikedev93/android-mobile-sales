package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class VentasDetMobileContract {

    class VentasDetMobileEntry: BaseColumns {
        companion object {
            const val TABLE_NAME: String = "mobile_ventas_det"

            const val _ID = BaseColumns._ID

            const val ID_VENTA_DET: String = "id_venta_det"
            const val ID_VENTA: String = "id_venta"
            const val ID_PRODUCTO: String = "id_producto"
            const val IVA: String = "iva" 	//5 -> IVA 5%, 10->IVA10%, 0->Exenta
            const val CANTIDAD: String = "cantidad"
            const val PRECIO: String = "precio"
            const val PRECIO_VENTA: String = "precio_venta"
            const val TOTAL_DET: String = "total_det"
            const val EXENTO: String = "exento"
            const val GRAVADO_5: String = "gravado_5"
            const val IVA_5: String = "iva_5"
            const val GRAVADO_10: String = "gravado_10"
            const val IVA_10: String = "iva_10"
            const val FECHA_ALTA: String = "fecha_alta"
            const val USUARIO_ALTA: String = "usuario_alta"
            const val PRODUCTO_CODIGO: String = "producto_codigo"
        }
    }
}