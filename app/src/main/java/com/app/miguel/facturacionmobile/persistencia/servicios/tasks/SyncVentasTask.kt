package com.app.miguel.facturacionmobile.persistencia.servicios.tasks

import android.content.Context
import android.os.AsyncTask
import com.app.miguel.facturacionmobile.HomeActivity
import com.app.miguel.facturacionmobile.SyncActivity
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasCabMobileContract
import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasDetMobileContract
import com.app.miguel.facturacionmobile.persistencia.servicios.SyncAPI
import com.app.miguel.facturacionmobile.persistencia.servicios.VentasResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.OkHttpClient
import org.apache.commons.lang3.StringEscapeUtils
import org.json.JSONObject
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SyncVentasTask (var context: Context, var mainActivity: SyncActivity): AsyncTask<Void, Void, String>(){

    private var syncroApi: SyncAPI? = null
    private var mRestAdapter: Retrofit? = null
    private var cantVentas: Int = -1

    override fun doInBackground(vararg params: Void?): String {
        val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(40, TimeUnit.MINUTES)
                .writeTimeout(40, TimeUnit.MINUTES)
                .build()

        mRestAdapter = Retrofit.Builder()
                .baseUrl(Utils.getBaseURL(context)!!)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        syncroApi = mRestAdapter!!.create(SyncAPI::class.java)
        var ventas = DBUtils.getSaleRequest(context)
        var json = Gson().toJson(ventas)
        if (ventas.pedidos!!.isNotEmpty()) {
            cantVentas = ventas.pedidos!!.size
            syncroApi!!.postSales(ventas).enqueue(object : retrofit2.Callback<List<VentasResponse>> {
                override fun onFailure(call: retrofit2.Call<List<VentasResponse>>?, t: Throwable?) {
                    mainActivity.showLoading(false)
                    mainActivity.showSnackbarError(t!!.message!!)
                }

                override fun onResponse(call: retrofit2.Call<List<VentasResponse>>?, response: Response<List<VentasResponse>>?) {
                    var servResponse = response!!.body()
                    try {
                        for (item in servResponse){
                            DBUtils.updateSaleSync(context, item)
                        }
                        Thread.sleep(2000)
                        mainActivity.showLoading(false)
                        mainActivity.showSnackbarGoHome("Sincronización exitosa\nSe " + if (cantVentas > 1) {
                            "enviaron "
                        } else {
                            "envió "
                        } + cantVentas + if (cantVentas > 1) " ventas" else " venta")
                    } catch (e: Exception){
                        mainActivity.showLoading(false)
                        mainActivity.showSnackbarError("¡Error al enviar las ventas! ${e.message}")
                    }
                }
            })
        } else {
            return "empty"
        }

        return ""
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        if (result.equals("empty")){
            var message = "¡No existen ventas que sincronizar!"
            Thread.sleep(2000)
            mainActivity.showLoading(false)
            mainActivity.showSnackbar(message)
        }
    }
}