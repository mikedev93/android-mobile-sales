package com.app.miguel.facturacionmobile.stock


import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.models.ProductModel
import com.app.miguel.facturacionmobile.persistencia.models.ProductStockModel
import kotlinx.android.synthetic.main.fragment_stock.*

class StockFragment : Fragment() {

    var mainActivity: StockActivity? = null
    var productsList: ArrayList<ProductStockModel>? = null
    var searchList: ArrayList<ProductStockModel>? = null
    var recyclerView: RecyclerView? = null
    var recyclerAdapter: StockRecyclerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_stock, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back_button.setOnClickListener {
            mainActivity!!.finish()
        }

        productsList = DBUtils.getAllProductos(context!!)
        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerAdapter = StockRecyclerAdapter(context, productsList)
        recyclerView!!.adapter = recyclerAdapter

        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                searchProducts(s.toString())
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchProducts(s.toString())
            }

        })
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as StockActivity
    }

    fun searchProducts(terms: String) {
        if (terms!!.trim().length > 1) {
            searchList = ArrayList()
            for (index in productsList!!.indices) {
                val item = productsList!!.get(index)
                if (item.nombre.toLowerCase().contains(terms.toLowerCase()) || item.codigo?.toString()!!.contains(terms.toLowerCase())) {
                    searchList!!.add(item)
                }
            }
            recyclerAdapter = StockRecyclerAdapter(context, searchList)
            recyclerView!!.adapter = recyclerAdapter
            recyclerAdapter!!.notifyDataSetChanged()
        } else {
            recyclerAdapter = StockRecyclerAdapter(context, productsList)
            recyclerView!!.adapter = recyclerAdapter
            recyclerAdapter!!.notifyDataSetChanged()
        }
    }
}
