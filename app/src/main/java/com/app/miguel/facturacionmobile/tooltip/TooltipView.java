package com.app.miguel.facturacionmobile.tooltip;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.app.miguel.facturacionmobile.R;
import com.app.miguel.facturacionmobile.tooltip.TooltipManager.ClosePolicy;
import com.app.miguel.facturacionmobile.tooltip.TooltipManager.Gravity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.app.miguel.facturacionmobile.tooltip.TooltipManager.getActivity;


/**
 * TooltipView
 */
@SuppressLint("ViewConstructor")
class TooltipView extends ViewGroup implements Tooltip {

    private static final List<Gravity> GRAVITIES = new ArrayList<>(
            Arrays.asList(
                    Gravity.LEFT,
                    Gravity.RIGHT,
                    Gravity.TOP,
                    Gravity.BOTTOM,
                    Gravity.CENTER
            )
    );
    private final List<Gravity> viewGravities = new ArrayList<>(GRAVITIES);
    private final long showDelay;
    private final int arrowIndicatorPaddingLeftDimenRes;
    private final int arrowIndicatorPosXPercentage;
    private final int customColor;
    private final String acessibilityMessage;
    private final int toolTipId;
    private final Rect drawRect;
    private final Rect tempRect;
    private final long showDuration;
    private final ClosePolicy closePolicy;
    private final Point point;
    private final int textResId;
    private final int topRule;
    private final int maxWidth;
    private final boolean hideArrow;
    private final long activateDelay;
    private final boolean restrict;
    private final long fadeDuration;
    private final TooltipManager.OnTooltipClosingCallback closeCallback;
    private final ToolTipTextDrawable drawable;
    Gravity gravity;
    Animator mShowAnimation;
    boolean mShowing;
    private boolean attached;
    private boolean initialized;
    private boolean activated;
    Runnable activateRunnable = new Runnable() {
        @Override
        public void run() {
            activated = true;
        }
    };
    private int padding;
    private String title;
    private String description;
    private String descriptionAcessibility;
    private Rect viewRect;
    private View view;
    private ImageView tooltipImageAction;
    private TextView tooltipTextTitle;
    private TextView tooltipTextDescription;
    private View tooltipArrowIndicator;
    private int marginBottom;
    private int marginTop;
    private int marginRight;
    private int marginLeft;
    private boolean marginBasedOnArrowIndicator;
    private OnCloseListener closeListener;
    Runnable hideRunnable = new Runnable() {
        @Override
        public void run() {
            onClose(false, false);
        }
    };
    private OnToolTipListener tooltipListener;

    TooltipView(Context context, TooltipManager.Builder builder) {
        super(context);

        TypedArray theme =
                context.getTheme().obtainStyledAttributes(null, R.styleable.TooltipLayout,
                        builder.defStyleAttr, builder.defStyleRes);
        this.padding = theme.getDimensionPixelSize(R.styleable.TooltipLayout_ttlm_padding, 30);
        theme.recycle();

        this.toolTipId = builder.id;
        this.title = builder.title;
        this.description = builder.description;
        this.descriptionAcessibility = builder.descriptionAcessibility;
        this.gravity = builder.gravity;
        this.textResId = builder.textResId;
        this.customColor = builder.customColor;
        builder.customColor = 0;
        this.marginBottom = builder.marginBottom;
        this.marginTop = builder.marginTop;
        this.marginRight = builder.marginRight;
        this.marginLeft = builder.marginLeft;
        this.marginBasedOnArrowIndicator = builder.marginBasedOnArrowIndicator;
        this.acessibilityMessage = builder.acessibilityMessage;
        this.maxWidth = builder.maxWidth;
        this.topRule = builder.actionbarSize;
        this.closePolicy = builder.closePolicy;
        this.showDuration = builder.showDuration;
        this.showDelay = builder.showDelay;
        this.hideArrow = builder.hideArrow;
        this.activateDelay = builder.activateDelay;
        this.restrict = builder.restrictToScreenEdges;
        this.fadeDuration = builder.fadeDuration;
        this.closeCallback = builder.closeCallback;
        this.arrowIndicatorPaddingLeftDimenRes = builder.arrowIndicatorPaddingLeftDimenRes;
        this.arrowIndicatorPosXPercentage = builder.arrowIndicatorPosXPercentage;
        if (null != builder.point) {
            this.point = new Point(builder.point);
            this.point.y += topRule;
        } else {
            this.point = null;
        }

        this.drawRect = new Rect();
        this.tempRect = new Rect();

        if (null != builder.view) {
            viewRect = new Rect();
            builder.view.getGlobalVisibleRect(viewRect);
        }

        if (!builder.isCustomView) {
            this.drawable = new ToolTipTextDrawable(context, builder);
        } else {
            this.drawable = null;
        }

        setVisibility(INVISIBLE);
    }

    int getTooltipId() {
        return toolTipId;
    }

    @Override
    public void show() {
        if (!isAttached()) {
            return;
        }
        fadeIn();
    }

    @Override
    public void hide(boolean remove) {
        if (!isAttached()) {
            return;
        }
        fadeOut(remove);
    }

    protected void fadeIn() {
        if (mShowing) {
            return;
        }

        if (null != mShowAnimation) {
            mShowAnimation.cancel();
        }

        mShowing = true;

        if (fadeDuration > 0) {
            mShowAnimation = ObjectAnimator.ofFloat(this, "alpha", 0, 1);
            mShowAnimation.setDuration(fadeDuration);
            if (this.showDelay > 0) {
                mShowAnimation.setStartDelay(this.showDelay);
            }
            mShowAnimation.addListener(new AnimationFadeInListener());
            mShowAnimation.start();
        } else {
            setVisibility(View.VISIBLE);
            tooltipListener.onShowCompleted(TooltipView.this);
            if (!activated) {
                postActivate(activateDelay);
            }
        }

        if (showDuration > 0) {
            getHandler().removeCallbacks(hideRunnable);
            getHandler().postDelayed(hideRunnable, showDuration);
        }
    }

    boolean isShowing() {
        return mShowing;
    }

    void postActivate(long ms) {
        if (ms > 0) {
            if (isAttached()) {
                postDelayed(activateRunnable, ms);
            }
        } else {
            activated = true;
        }
    }

    void removeFromParent() {
        ViewParent parent = getParent();
        if (null != parent) {
            if (null != getHandler()) {
                getHandler().removeCallbacks(hideRunnable);
            }
            ((ViewGroup) parent).removeView(TooltipView.this);

            if (null != mShowAnimation && mShowAnimation.isStarted()) {
                mShowAnimation.cancel();
            }
        }
    }

    protected void fadeOut(final boolean remove) {
        if (!isAttached() || !mShowing) {
            return;
        }

        if (null != mShowAnimation) {
            mShowAnimation.cancel();
        }

        mShowing = false;

        if (fadeDuration > 0) {
            float alpha = getAlpha();
            mShowAnimation = ObjectAnimator.ofFloat(this, "alpha", alpha, 0);
            mShowAnimation.setDuration(fadeDuration);
            mShowAnimation.addListener(new AnimationFadeOutListener(remove));
            mShowAnimation.start();
        } else {
            setVisibility(View.INVISIBLE);
            checkRemoval(remove);
        }
    }

    private void checkRemoval(boolean remove) {
        if (remove) {
            fireOnHideCompleted();
        }
    }

    private void fireOnHideCompleted() {
        if (null != tooltipListener) {
            tooltipListener.onHideCompleted(TooltipView.this);
        }
    }

    @Override
    protected void onLayout(final boolean changed, final int l, final int t, final int r,
                            final int b) {

        //  The layout has actually already been performed and the positions
        //  cached.  Apply the cached values to the children.
        final int count = getChildCount();

        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                child.layout(child.getLeft(), child.getTop(), child.getMeasuredWidth(),
                        child.getMeasuredHeight());
            }
        }

        if (changed) {
            viewGravities.clear();
            viewGravities.addAll(GRAVITIES);
            viewGravities.remove(gravity);
            viewGravities.add(0, gravity);
            calculatePositions(viewGravities);
        }
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int myWidth = 0;
        int myHeight = 0;

        final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        final int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        final int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        // Record our dimensions if they are known
        if (widthMode != MeasureSpec.UNSPECIFIED) {
            myWidth = widthSize;
        }

        if (heightMode != MeasureSpec.UNSPECIFIED) {
            myHeight = heightSize;
        }

        final int count = getChildCount();

        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                int childWidthMeasureSpec =
                        MeasureSpec.makeMeasureSpec(myWidth, MeasureSpec.AT_MOST);
                int childHeightMeasureSpec =
                        MeasureSpec.makeMeasureSpec(myHeight, MeasureSpec.AT_MOST);
                child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
            }
        }

        setMeasuredDimension(myWidth, myHeight);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        attached = true;

        initializeView();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        attached = false;
    }

    private void initializeView() {
        if (!isAttached() || initialized) {
            return;
        }
        initialized = true;

        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        view = LayoutInflater.from(getContext()).inflate(textResId, this, false);
        view.setLayoutParams(params);

        if (null != drawable) {
            view.setBackgroundDrawable(drawable);
            int pad = hideArrow ? padding / 2 : padding;
            view.setPadding(pad, pad, pad, pad);
        }

        tooltipArrowIndicator = view.findViewById(R.id.text_tooltip_arrow_indicator);

        setupBackgroundColorFilter();
        setupTooltipTextTitle();
        setupTootipTextDescription();
        setupTooltipImageAction();
        setupTooltipTextMaxWidth();

        this.addView(view);
    }

    private void setupTooltipTextTitle() {
        tooltipTextTitle = (TextView) view.findViewById(R.id.text_tooltip_title);
        if (tooltipTextTitle != null) {
            tooltipTextTitle.setText(this.title);
        }
    }

    private void setupBackgroundColorFilter() {
        if (customColor != 0) {
            Drawable background = tooltipArrowIndicator.getBackground();
            background.setColorFilter(customColor, PorterDuff.Mode.SRC);
        }
    }

    private void setupTooltipTextMaxWidth() {
        if (maxWidth > -1) {
            if (tooltipTextTitle != null) {
                tooltipTextTitle.setMaxWidth(maxWidth);
            }
            if (tooltipTextDescription != null) {
                tooltipTextDescription.setMaxWidth(maxWidth);
            }
        }
    }

    private void setupTooltipImageAction() {
        tooltipImageAction = (ImageView) view.findViewById(R.id.image_tooltip_close);
        if (tooltipImageAction != null) {
            tooltipImageAction.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeListener.onClose(TooltipView.this);
                }
            });
        }
    }

    private void setupTootipTextDescription() {
        tooltipTextDescription = (TextView) view.findViewById(R.id.text_tooltip_description);
        if (tooltipTextDescription != null) {
            tooltipTextDescription.setText(this.description);
            tooltipTextDescription.setContentDescription(acessibilityMessage);
            if (customColor != 0) {
                Drawable background = tooltipTextDescription.getBackground();
                ((GradientDrawable) background).setColor(customColor);
            }
            if (this.descriptionAcessibility != null) {
                tooltipTextDescription.setContentDescription(this.descriptionAcessibility);
            }
        }
    }

    private void calculatePositions(List<Gravity> gravities) {
        if (!isAttached()) {
            return;
        }

        if (gravities.isEmpty()) {
            doGravityEmpty();
            return;
        }

        Gravity gravityLocal = gravities.remove(0);

        Rect screenRect = new Rect();
        doActWindow(screenRect);

        int statusbarHeight = screenRect.top;

        doSetTooltipArrowIndicator();
        doViewRect(statusbarHeight);

        screenRect.top += topRule;

        int width = view.getWidth();
        int height = view.getMeasuredHeight();

        // get the destination point
        Point point2 = new Point();

        if (! doVerifyGravity(gravities, gravityLocal, screenRect, width, height, point2)) {
            doCalculatePositionFinale(gravityLocal, point2);
        }

    }

    private boolean doVerifyGravity(List<Gravity> gravities, Gravity gravity, Rect screenRect, int width, int height, Point point2) {
        boolean retornar = false;

        if (gravity == Gravity.BOTTOM) {
            retornar = doGravityBottom(gravities, screenRect, width, height, point2);

        } else if (gravity == Gravity.TOP) {
            retornar =  doGravityTop(gravities, screenRect, width, height, point2);

        } else if (gravity == Gravity.RIGHT) {
            retornar = doGravityRight(gravities, screenRect, width, height, point2);

        } else if (gravity == Gravity.LEFT) {
            retornar = doGravityLeft(gravities, screenRect, width, height, point2);

        } else if (this.gravity == Gravity.CENTER) {
            doGravityCenter(screenRect, width, height, point2);

        } else if (gravity == Gravity.TOP_CENTER) {
            doGravityTopCenter(width, height, point2);
        }
        return retornar;
    }

    private void doGravityEmpty() {
        if (null != tooltipListener) {
            tooltipListener.onShowFailed(this);
        }
        setVisibility(View.GONE);
        return;
    }

    private boolean doGravityLeft(List<Gravity> gravities, Rect screenRect, int width, int height, Point point2) {
        drawRect.set(
                viewRect.left - width,
                viewRect.centerY() - height / 2,
                viewRect.left,
                viewRect.centerY() + height / 2);

        point2.x = viewRect.left;
        point2.y = viewRect.centerY();

        if (restrict && !screenRect.contains(drawRect)) {
            if (drawRect.bottom > screenRect.bottom) {
                drawRect.offset(0, screenRect.bottom - drawRect.bottom);
            } else if (drawRect.top < screenRect.top) {
                drawRect.offset(0, screenRect.top - drawRect.top);
            }
            if (drawRect.left < screenRect.left) {
                // this means there's no enough space!
                this.gravity = Gravity.RIGHT;
                calculatePositions(gravities);
                return true;
            } else if (drawRect.right > screenRect.right) {
                drawRect.offset(screenRect.right - drawRect.right, 0);
            }
        }
        return false;
    }

    private boolean doGravityRight(List<Gravity> gravities, Rect screenRect, int width, int height, Point point2) {
        drawRect.set(
                viewRect.right,
                viewRect.centerY() - height / 2,
                viewRect.right + width,
                viewRect.centerY() + height / 2);

        point2.x = viewRect.right;
        point2.y = viewRect.centerY();

        if (restrict && !screenRect.contains(drawRect)) {
            if (drawRect.bottom > screenRect.bottom) {
                drawRect.offset(0, screenRect.bottom - drawRect.bottom);
            } else if (drawRect.top < screenRect.top) {
                drawRect.offset(0, screenRect.top - drawRect.top);
            }
            if (drawRect.right > screenRect.right) {
                // this means there's no enough space!
                calculatePositions(gravities);
                return true;
            } else if (drawRect.left < screenRect.left) {
                drawRect.offset(screenRect.left - drawRect.left, 0);
            }
        }
        return false;
    }

    private boolean doGravityTop(List<Gravity> gravities, Rect screenRect, int width, int height, Point point2) {
        drawRect.set(
                viewRect.left - width,
                viewRect.top - height,
                viewRect.left,
                viewRect.top);

        point2.x = viewRect.centerX();
        point2.y = viewRect.top;

        if (restrict && !screenRect.contains(drawRect)) {
            if (drawRect.right > screenRect.right) {
                drawRect.offset(screenRect.right - drawRect.right, 0);
            } else if (drawRect.left < screenRect.left) {
                drawRect.offset(-drawRect.left, 0);
            }
            if (drawRect.top < screenRect.top) {
                // this means there's no enough space!
                calculatePositions(gravities);
                return true;
            } else if (drawRect.bottom > screenRect.bottom) {
                drawRect.offset(0, screenRect.bottom - drawRect.bottom);
            }
        }
        return false;
    }

    private boolean doGravityBottom(List<Gravity> gravities, Rect screenRect, int width, int height, Point point2) {
        drawRect.set(
                viewRect.centerX() - width / 2,
                viewRect.bottom,
                viewRect.centerX() + width / 2,
                viewRect.bottom + height);

        point2.x = viewRect.centerX();
        point2.y = viewRect.bottom;

        if (restrict && !screenRect.contains(drawRect)) {
            if (drawRect.right > screenRect.right) {
                drawRect.offset(screenRect.right - drawRect.right, 0);
            } else if (drawRect.left < screenRect.left) {
                drawRect.offset(-drawRect.left, 0);
            }
            if (drawRect.bottom > screenRect.bottom) {
                // this means there's no enough space!
                calculatePositions(gravities);
                return true;
            } else if (drawRect.top < screenRect.top) {
                drawRect.offset(0, screenRect.top - drawRect.top);
            }
        }
        return false;
    }

    private void doGravityTopCenter(int width, int height, Point point2) {
        drawRect.set(
                viewRect.centerX() - width / 2,
                viewRect.top - height,
                viewRect.centerX() - width / 2,
                viewRect.top);

        point2.x = viewRect.centerX();
        point2.y = viewRect.centerY();
    }

    private void doCalculatePositionFinale(Gravity gravity, Point point2) {
        // translate the textview
        recalculateViewRectWithMargins();

        view.setTranslationX(drawRect.left);
        view.setTranslationY(drawRect.top);

        if (null != drawable) {
            // get the global rect for the textview
            view.getGlobalVisibleRect(tempRect);

            point2.x -= tempRect.left;
            point2.y -= tempRect.top;

            if (!hideArrow) {
                if (gravity == Gravity.LEFT || gravity == Gravity.RIGHT) {
                    point2.y -= padding / 2;
                } else if (gravity == Gravity.TOP || gravity == Gravity.BOTTOM) {
                    point2.x -= padding / 2;
                }
            }

            drawable.setAnchor(gravity, hideArrow ? 0 : padding / 2);

            if (!this.hideArrow) {
                drawable.setDestinationPoint(point2);
            }
        }
    }

    private void doGravityCenter(Rect screenRect, int width, int height, Point point2) {
        drawRect.set(
                viewRect.centerX() - width / 2,
                viewRect.centerY() - height / 2,
                viewRect.centerX() - width / 2,
                viewRect.centerY() + height / 2);

        point2.x = viewRect.centerX();
        point2.y = viewRect.centerY();

        if (restrict && !screenRect.contains(drawRect)) {
            if (drawRect.bottom > screenRect.bottom) {
                drawRect.offset(0, screenRect.bottom - drawRect.bottom);
            } else if (drawRect.top < screenRect.top) {
                drawRect.offset(0, screenRect.top - drawRect.top);
            }
            if (drawRect.right > screenRect.right) {
                drawRect.offset(screenRect.right - drawRect.right, 0);
            } else if (drawRect.left < screenRect.left) {
                drawRect.offset(screenRect.left - drawRect.left, 0);
            }
        }
    }

    private void doActWindow(Rect screenRect) {
        final Activity act = getActivity(getContext());
        if (act != null) {
            Window window = act.getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(screenRect);
        } else {
            WindowManager wm =
                    (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            android.view.Display display = wm.getDefaultDisplay();
            display.getRectSize(screenRect);
        }
    }

    private void doViewRect(int statusbarHeight) {
        if (viewRect == null) {
            viewRect = new Rect();
            viewRect.set(point.x, point.y + statusbarHeight, point.x, point.y + statusbarHeight);
        }
    }

    private void doSetTooltipArrowIndicator() {
        if (tooltipArrowIndicator != null) {
            if (this.arrowIndicatorPaddingLeftDimenRes > 0) {
                int marginLeft2 = getResources().getDimensionPixelSize(this.arrowIndicatorPaddingLeftDimenRes);
                tooltipArrowIndicator.setTranslationX(marginLeft2);
            } else if (this.arrowIndicatorPosXPercentage > 0) {
                tooltipArrowIndicator.setTranslationX(marginLeft);
            }
        }
    }

    private void recalculateViewRectWithMargins() {
        if (!marginBasedOnArrowIndicator) {
            switch (gravity) {
                case TOP:
                case TOP_CENTER:
                    drawRect.top = drawRect.top + tooltipArrowIndicator.getHeight();
                    break;
                default:
                    break;
            }
        }
        if (marginBottom > 0) {
            drawRect.top = drawRect.top - marginBottom;
        }
        if (marginTop > 0) {
            drawRect.top = drawRect.top + marginTop;
        }
        if (marginRight > 0) {
            drawRect.left = drawRect.left - marginRight;
        }
        if (marginLeft > 0) {
            drawRect.left = drawRect.left + marginLeft;
        }
    }

    @Override
    public void setOffsetX(int x) {
        setTranslationX(x - viewRect.left);
    }

    @Override
    public void setOffsetY(int y) {
        setTranslationY(y - viewRect.top);
    }

    @Override
    public void offsetTo(final int x, final int y) {
        setTranslationX(x - viewRect.left);
        setTranslationY(y - viewRect.top);
    }

    @Override
    public boolean isAttached() {
        return attached;
    }

    @Override
    public boolean onTouchEvent(@NonNull final MotionEvent event) {
        /*
         * The code here was very confusing. I tried to refactor it and make it more readable,
         * although method names may not reflect their real intention (because I don't know what
         * this method was trying to accomplish).
         */
        final int action = event.getActionMasked();
        final boolean containsTouch = drawRect.contains((int) event.getX(), (int) event.getY());

        if (isClosableDown(action, containsTouch)) {
            onClose(true, containsTouch);
        }

        return isActionableButNotActivated() || isActionableDown(action, containsTouch);
    }

    // Helper method for onTouchEvent. Reduces cyclomatic complexity
    private boolean isActionable() {
        return attached && mShowing && isShown() && closePolicy.isTouch();
    }

    // Helper method for onTouchEvent. Reduces cyclomatic complexity
    private boolean isActionableButNotActivated() {
        return isActionable() && !activated;
    }

    // Helper method for onTouchEvent. Reduces cyclomatic complexity
    private boolean isActionableActivatedDown(int action) {
        return isActionable() && activated && action == MotionEvent.ACTION_DOWN;
    }

    // Helper method for onTouchEvent. Reduces cyclomatic complexity
    private boolean isClosableDown(int action, boolean containsTouch) {
        return isActionableActivatedDown(action)
                && (closePolicy.isOutside() || (closePolicy.isInside() && containsTouch));
    }

    // Helper method for onTouchEvent. Reduces cyclomatic complexity
    private boolean isActionableDown(int action, boolean containsTouch) {
        return isActionableActivatedDown(action)
                && (closePolicy.isExclusive() || (closePolicy.isInclusive() && containsTouch));
    }

    private void onClose(boolean fromUser, boolean containsTouch) {

        if (null == getHandler()) {
            return;
        }
        if (!isAttached()) {
            return;
        }

        getHandler().removeCallbacks(hideRunnable);

        if (null != closeListener) {
            closeListener.onClose(this);
        }

        if (null != closeCallback) {
            closeCallback.onClosing(toolTipId, fromUser, containsTouch);
        }
    }

    void setOnCloseListener(OnCloseListener listener) {
        this.closeListener = listener;
    }

    void setOnToolTipListener(OnToolTipListener listener) {
        this.tooltipListener = listener;
    }

    interface OnCloseListener {
        void onClose(TooltipView layout);
    }

    interface OnToolTipListener {
        void onHideCompleted(TooltipView layout);

        void onShowCompleted(TooltipView layout);

        void onShowFailed(TooltipView layout);
    }

    class AnimationFadeOutListener implements AnimatorListener {
        boolean cancelled;
        boolean remove;

        AnimationFadeOutListener(boolean remove) {
            this.remove = remove;
        }

        @Override
        public void onAnimationStart(final Animator animation) {
            cancelled = false;
        }

        @Override
        public void onAnimationEnd(final Animator animation) {
            if (cancelled) {
                return;
            }
            checkRemoval(remove);
            mShowAnimation = null;
        }

        @Override
        public void onAnimationCancel(final Animator animation) {
            cancelled = true;
        }

        @Override
        public void onAnimationRepeat(final Animator animation) {
            //unused
        }
    }

    class AnimationFadeInListener implements AnimatorListener {
        boolean cancelled;

        @Override
        public void onAnimationStart(final Animator animation) {
            setVisibility(View.VISIBLE);
            cancelled = false;
        }

        @Override
        public void onAnimationEnd(final Animator animation) {
            if (null != tooltipListener && !cancelled) {
                tooltipListener.onShowCompleted(TooltipView.this);
                postActivate(activateDelay);
            }
        }

        @Override
        public void onAnimationCancel(final Animator animation) {
            cancelled = true;
        }

        @Override
        public void onAnimationRepeat(final Animator animation) {
            // unused
        }
    }
}