package com.app.miguel.facturacionmobile.persistencia.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductStockModel (@Expose @SerializedName("id") var id: Int,
                         @Expose @SerializedName("nombre") var nombre: String,
                         @Expose @SerializedName("presentacion") var presentacion: String?,
                         @Expose @SerializedName("unidadMedida") var unidadMedida: String,
                         @Expose @SerializedName("iva") var iva: Double,
                         @Expose @SerializedName("codigo") var codigo: String?,
                         @Expose @SerializedName("precioUnidad") var precioUnidad: Double?,
                         @Expose @SerializedName("precioCaja") var precioCaja: Double?,
                         @Expose @SerializedName("stock") var stock: Int?,
                         @Expose @SerializedName("stockInicial") var stockInicial: Int?,
                         @Expose @SerializedName("composicionVenta") var composicionVenta: Int?,
                         @Expose @SerializedName("idPedidoRemision") var id_pedido_remision: Int?,
                         @Expose var checked: Boolean = false)

class ListProductStock(@Expose @SerializedName("stock") var productList: ArrayList<ProductStockModel>)