package com.app.miguel.facturacionmobile.sales.clients


import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.persistencia.SalesManager
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.models.GeoCiudadModel
import com.app.miguel.facturacionmobile.sales.SalesActivity
import com.app.miguel.facturacionmobile.sales.clients.CityFinderRecyclerAdapter.*
import kotlinx.android.synthetic.main.fragment_city_finder.*


/**
 * A simple [Fragment] subclass.
 */
class CityFinderFragment : Fragment() {
    var mainActivity: SalesActivity? = null
    var cityList: ArrayList<GeoCiudadModel>? = null
    var searchList: ArrayList<GeoCiudadModel>? = null
    var recyclerView: RecyclerView? = null
    var recyclerAdapter: CityFinderRecyclerAdapter? = null
    var mListener: OnCitySelectedListener? = null
    var selectedCity: GeoCiudadModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_city_finder, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as SalesActivity
        mListener = context as OnCitySelectedListener
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back_button.setOnClickListener {
            fragmentManager!!.popBackStack()
        }

        btn_continue.setOnClickListener {
            mListener!!.citytSelected(selectedCity!!)
            fragmentManager!!.popBackStackImmediate()
        }

        cityList = DBUtils.getGeoCiudadesAll(context!!)
        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerAdapter = CityFinderRecyclerAdapter(context, cityList, object : CityFinderClickListener {
            override fun onClick(ciudad: GeoCiudadModel) {
                selectedCity = ciudad
                SalesManager.selectedCity = selectedCity
                btn_continue.isEnabled = ciudad != null
            }
        })
        recyclerView!!.adapter = recyclerAdapter

        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                searchCities(s.toString())
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchCities(s.toString())
            }

        })
    }

    fun searchCities(terms: String?) {
        if (terms!!.trim().length > 3) {
            searchList = ArrayList()
            for (index in cityList!!.indices) {
                val item = cityList!!.get(index)
                if (item.ciudadDescripcion?.toLowerCase()!!.contains(terms.toLowerCase()) || item.departamentoDescripcion?.toLowerCase()!!.contains(terms.toLowerCase())) {
                    searchList!!.add(item)
                }
            }
            recyclerAdapter = CityFinderRecyclerAdapter(context, searchList, object : CityFinderClickListener {
                override fun onClick(ciudad: GeoCiudadModel) {
                    selectedCity = ciudad
                    SalesManager.selectedCity = selectedCity
                    btn_continue.isEnabled = ciudad != null
                }
            })
            recyclerView!!.adapter = recyclerAdapter
            recyclerAdapter!!.notifyDataSetChanged()
        } else if (!terms.trim().isNullOrEmpty() && terms!!.trim().length < 3) {
            recyclerAdapter = CityFinderRecyclerAdapter(context, cityList, object : CityFinderClickListener {
                override fun onClick(ciudad: GeoCiudadModel) {
                    selectedCity = ciudad
                    SalesManager.selectedCity = selectedCity
                    btn_continue.isEnabled = ciudad != null
                }
            })
            recyclerView!!.adapter = recyclerAdapter
            recyclerAdapter!!.notifyDataSetChanged()
        }
    }

    interface OnCitySelectedListener {
        fun citytSelected(ciudad: GeoCiudadModel)
    }
}
