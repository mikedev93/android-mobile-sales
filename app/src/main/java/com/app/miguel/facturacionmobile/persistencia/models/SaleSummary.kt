package com.app.miguel.facturacionmobile.persistencia.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class SaleSummaryMobile(@Expose @SerializedName("venta_cab") var ventaCabeceraModel: VentaCabeceraModel,
                        @Expose @SerializedName("venta_det") var ventaDetalles: ArrayList<VentaDetalleModel>) : Parcelable