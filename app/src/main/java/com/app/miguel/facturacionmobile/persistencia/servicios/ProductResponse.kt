package com.app.miguel.facturacionmobile.persistencia.servicios

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductResponse(var productos: ArrayList<ProductItem>)

class ProductItem(@Expose @SerializedName("id") var id: Int,
                  @Expose @SerializedName("nombre") var nombre: String,
                  @Expose @SerializedName("presentacion") var presentacion: String?,
                  @Expose @SerializedName("unidad_medida") var unidadMedida: String?,
                  @Expose @SerializedName("iva") var iva: Double,
                  @Expose @SerializedName("codigo") var codigo: String?,
                  @Expose @SerializedName("stock") var stock: Int,
                  @Expose @SerializedName("idpedidoremision") var idPedidoRemision: Int,
                  @Expose @SerializedName("precioFabrica")var precioFabrica: Double)

