package com.app.miguel.facturacionmobile.sales.products;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.miguel.facturacionmobile.R;
import com.app.miguel.facturacionmobile.persistencia.models.ProductModel;

import java.util.ArrayList;

public class ProductFinderRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<ProductModel> productList;
    private int rowIndex = -1;
    private ProductFinderClickListener finderClickListener;

    public ProductFinderRecyclerAdapter(Context context, ArrayList<ProductModel> productList, ProductFinderClickListener finderClickListener) {
        this.context = context;
        this.productList = productList;
        this.finderClickListener = finderClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.view_product_finder_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final ProductModel product = productList.get(position);
        final ViewHolder holder = (ViewHolder) viewHolder;

        holder.vTvCodigo.setText(product.getCodigo());
        holder.vTvNombre.setText(product.getNombre());
        holder.vTvPresentacion.setText(product.getPresentacion());

        holder.vLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rowIndex = position;
                notifyDataSetChanged();
            }
        });

        if (rowIndex == position) {
            if (product.getChecked()) {
                holder.viewSeparador.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                holder.vLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.vTvCodigo.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
                holder.vTvNombre.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
                holder.vTvPresentacion.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));

                product.setChecked(false);
                unselectProduct(null);

                finderClickListener.onClick(null);
            } else {
                holder.viewSeparador.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.vLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                holder.vTvCodigo.setTextColor(context.getResources().getColor(R.color.white));
                holder.vTvNombre.setTextColor(context.getResources().getColor(R.color.white));
                holder.vTvPresentacion.setTextColor(context.getResources().getColor(R.color.white));

                product.setChecked(true);
                unselectProduct(product);

                finderClickListener.onClick(product);
            }
        } else {
            holder.viewSeparador.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.vLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.vTvCodigo.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
            holder.vTvNombre.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
            holder.vTvPresentacion.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    private void unselectProduct(ProductModel product) {
        if (product != null) {
            for (int index = 0; index < productList.size(); index++) {
                if (!productList.get(index).equals(product)) {
                    productList.get(index).setChecked(false);
                }
            }
        } else {
            for (int index = 0; index < productList.size(); index++) {
                productList.get(index).setChecked(false);
            }
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayout vLinearLayout;
        private final TextView vTvCodigo;
        private final TextView vTvNombre;
        private final TextView vTvPresentacion;
        private final View viewSeparador;

        public ViewHolder(View itemView) {
            super(itemView);
            vLinearLayout = itemView.findViewById(R.id.ll_content);
            vTvCodigo = itemView.findViewById(R.id.tv_codigo);
            vTvNombre = itemView.findViewById(R.id.tv_descripcion);
            vTvPresentacion = itemView.findViewById(R.id.tv_presentacion);
            viewSeparador = itemView.findViewById(R.id.separador);
        }
    }

    public interface ProductFinderClickListener {
        void onClick(ProductModel product);
    }
}
