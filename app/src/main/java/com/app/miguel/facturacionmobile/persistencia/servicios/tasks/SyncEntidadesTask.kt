package com.app.miguel.facturacionmobile.persistencia.servicios.tasks

import android.content.Context
import android.os.AsyncTask
import com.app.miguel.facturacionmobile.HomeActivity
import com.app.miguel.facturacionmobile.SyncActivity
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.database.contracts.EntidadesContract
import com.app.miguel.facturacionmobile.persistencia.database.contracts.EntidadesSucursalesContract
import com.app.miguel.facturacionmobile.persistencia.models.EntidadSucursalResponse
import com.app.miguel.facturacionmobile.persistencia.models.SucursalResponse
import com.app.miguel.facturacionmobile.persistencia.servicios.SyncAPI
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SyncEntidadesTask(var context: Context, var mainActivity: SyncActivity) : AsyncTask<Void, Void, String>() {

    private var syncroApi: SyncAPI? = null
    private var mRestAdapter: Retrofit? = null

    override fun doInBackground(vararg params: Void?): String {
        val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(40, TimeUnit.MINUTES)
                .writeTimeout(40, TimeUnit.MINUTES)
                .build()

        mRestAdapter = Retrofit.Builder()
                .baseUrl(Utils.getBaseURL(context)!!)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        syncroApi = mRestAdapter!!.create(SyncAPI::class.java)
        var entidadResponse = syncroApi!!.getEntidades()
        entidadResponse.enqueue(object : retrofit2.Callback<List<SucursalResponse>> {
            override fun onFailure(call: retrofit2.Call<List<SucursalResponse>>?, t: Throwable?) {
                mainActivity.showLoading(false)
                mainActivity.showSnackbarError(t!!.message!!)
            }

            override fun onResponse(call: retrofit2.Call<List<SucursalResponse>>?, response: Response<List<SucursalResponse>>?) {
                var servResponse = response!!.body()
                if (servResponse != null) {
                    DBUtils.vaciarTabla(context, EntidadesSucursalesContract.EntidadSucursalesEntry.TABLE_NAME)
                }
                DBUtils.saveEntidadResponse(context, servResponse)
                Thread.sleep(1000)
                mainActivity.showLoading(false)
                mainActivity.showSnackbarGoHome("Sincronización exitosa")
//                mainActivity.goHome()
            }
        })

        return ""
    }
}