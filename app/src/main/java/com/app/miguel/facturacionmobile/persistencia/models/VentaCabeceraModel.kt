package com.app.miguel.facturacionmobile.persistencia.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class VentaCabeceraModel(@Expose @SerializedName("id_venta") var id_venta: Int?,
                         @Expose @SerializedName("id_pedido_remision") var id_pedido_remision: Int?,
                         @Expose @SerializedName("id_entidad") var id_entidad: Int?,
                         @Expose @SerializedName("id_entidad_sucursal") var id_entidad_sucursal: Int?,
                         @Expose @SerializedName("condicion_venta") var condicion_venta: Int?,
                         @Expose @SerializedName("fecha_emision") var fecha_emision: String?,
                         @Expose @SerializedName("nro_documento") var nro_documento: String?,
                         @Expose @SerializedName("fecha_vencimiento") var fecha_vencimiento: String?,
                         @Expose @SerializedName("fecha_alta") var fecha_alta: String?,
                         @Expose @SerializedName("usuario_alta") var usuario_alta: Int?,
                         @Expose @SerializedName("fecha_anulado") var fecha_anulado: String?,
                         @Expose @SerializedName("usuario_anulado") var usuario_anulado: Int?,
                         @Expose @SerializedName("bruto_exento") var bruto_exento: Long?,
                         @Expose @SerializedName("bruto_gravado_5") var bruto_gravado_5: Long?,
                         @Expose @SerializedName("bruto_iva_5") var bruto_iva_5: Long?,
                         @Expose @SerializedName("bruto_gravado_10") var bruto_gravado_10: Long?,
                         @Expose @SerializedName("bruto_iva_10") var bruto_iva_10: Long?,
                         @Expose @SerializedName("neto_exento") var neto_exento: Long?,
                         @Expose @SerializedName("neto_gravado_5") var neto_gravado_5: Long?,
                         @Expose @SerializedName("neto_iva_5") var neto_iva_5: Long?,
                         @Expose @SerializedName("neto_gravado_10") var neto_gravado_10: Long?,
                         @Expose @SerializedName("neto_iva_10") var neto_iva_10: Long?,
                         @Expose @SerializedName("total_pagado") var total_pagado: Long?,
                         @Expose @SerializedName("tipo_pago") var tipo_pago: Int?,
                         @Expose @SerializedName("comentario") var comentario: String?) : Parcelable

class VentaCabeceraSync(@Expose @SerializedName("cab") var cabecera: VentaCabeceraModel?,
                        @Expose @SerializedName("id_venta") var sync_id: Int?,
                        @Expose @SerializedName("id_venta") var sync_date: String?)

@Parcelize
class ListVentaCabecera(@Expose @SerializedName("ventas_cabecera")var ventasCabecera: ArrayList<VentaCabeceraModel>) : Parcelable