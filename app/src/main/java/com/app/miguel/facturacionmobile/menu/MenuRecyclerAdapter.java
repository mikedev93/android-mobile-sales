package com.app.miguel.facturacionmobile.menu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.miguel.facturacionmobile.HomeActivity;
import com.app.miguel.facturacionmobile.LoginActivity;
import com.app.miguel.facturacionmobile.R;
import com.app.miguel.facturacionmobile.Utils;
import com.app.miguel.facturacionmobile.cancelInvoice.CancelInvoiceActivity;
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils;
import com.app.miguel.facturacionmobile.print.PrintBTUtils;
import com.app.miguel.facturacionmobile.sales.SalesActivity;
import com.app.miguel.facturacionmobile.stock.StockActivity;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MenuRecyclerAdapter extends RecyclerView.Adapter<MenuItemHolder> {

    private Context context;
    private ArrayList<MenuItem> menuItems;
    private HomeActivity homeActivity;

    public MenuRecyclerAdapter(Context context, ArrayList<MenuItem> menuItems) {
        this.context = context;
        this.menuItems = menuItems;
        this.homeActivity = (HomeActivity) context;
    }

    @NonNull
    @Override
    public MenuItemHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.menu_card, viewGroup, false);
        MenuItemHolder holder = new MenuItemHolder(view, new MenuItemHolder.MyClickListener() {
            @Override
            public void onClickAction(int position) {
                MenuItem menuItem = menuItems.get(position);
                Intent intent;
                Bundle bundle = new Bundle();
                switch (menuItem.getTitle()) {
                    case "Ventas":
                        if (DBUtils.Companion.getSaleParameters(context) != null) {
                            if (PrintBTUtils.Companion.isBluetoothEnabled()) {
                                if (PrintBTUtils.Companion.isDevicePaired()) {
                                    intent = new Intent(context, SalesActivity.class);
                                    context.startActivity(intent);
                                } else {
                                    Utils.Companion.showSnackbar(homeActivity, homeActivity.findViewById(R.id.rl_main), PrintBTUtils.Companion.getDeviceNotPairedMessage(), Utils.TYPE_ERROR);
                                }
                            } else {
                                Utils.Companion.showSnackbar(homeActivity, homeActivity.findViewById(R.id.rl_main), PrintBTUtils.Companion.getEnableBluetoothMessage(), Utils.TYPE_ERROR);
                            }
                        } else {
                            Utils.Companion.showSnackbar(context, homeActivity.findViewById(R.id.rl_main), "¡Debe sincronizar los datos antes de continuar!", Utils.TYPE_ERROR);
                        }
                        break;
                    case "Stock":
                        intent = new Intent(context, StockActivity.class);
                        context.startActivity(intent);
                        break;
                    case "Resumen":
                        intent = new Intent(context, CancelInvoiceActivity.class);
                        context.startActivity(intent);
                        break;
                    case "Salir":
                        intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);
                        homeActivity.finish();
                        break;
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MenuItemHolder menuItemHolder, int position) {
        MenuItem menuItem = menuItems.get(position);
        menuItemHolder.setMenuDetails(menuItem);
    }

    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    private boolean checkNecesaryData(String opcion) {
//        String query;
//        JSONArray jsonArray, jsonArray1;
//        switch (opcion){
//            case "Carga":
//                query = "select * from " + EquipoContract.EquipoEntry.TABLE_NAME;
//                jsonArray = Utils.getRegistroTablas(homeActivity, query, null);
//                if (jsonArray == null) {
//                    return false;
//                }
//                break;
//            case "Recepción":
//                query = "select * from " + EquipoContract.EquipoEntry.TABLE_NAME;
//                jsonArray = Utils.getRegistroTablas(homeActivity, query, null);
//                query = "select * from " + ItemObraContract.ItemObraEntry.TABLE_NAME;
//                jsonArray1 = Utils.getRegistroTablas(homeActivity, query, null);
//                if (jsonArray == null || jsonArray1 == null) {
//                    return false;
//                }
//                break;
//            case "Reportes":
//                query = "select * from " + EquipoContract.EquipoEntry.TABLE_NAME;
//                jsonArray = Utils.getRegistroTablas(homeActivity, query, null);
//                query = "select * from " + ItemObraContract.ItemObraEntry.TABLE_NAME;
//                jsonArray1 = Utils.getRegistroTablas(homeActivity, query, null);
//                if (jsonArray == null || jsonArray1 == null) {
//                    return false;
//                }
//                break;
//        }
        return true;
    }

    private void showSnackbar(String message, String type) {
        int backgroundColor, textColor, duration;
        if (type == null) type = "";
        switch (type) {
            case "error":
//                backgroundColor = ContextCompat.getColor(homeActivity, R.color.warning);
                duration = Snackbar.LENGTH_LONG;
                textColor = ContextCompat.getColor(homeActivity, R.color.white);
                break;
            case "warning":
//                backgroundColor = ContextCompat.getColor(homeActivity, R.color.sand);
                duration = Snackbar.LENGTH_LONG;
                textColor = ContextCompat.getColor(homeActivity, R.color.white);
                break;

            default:
                backgroundColor = ContextCompat.getColor(homeActivity, R.color.colorPrimaryDark);
                duration = Snackbar.LENGTH_SHORT;
                textColor = ContextCompat.getColor(homeActivity, R.color.white);
                break;
        }
        Snackbar snackbar = Snackbar.make(homeActivity.findViewById(R.id.rl_main), message, duration);
        View snackView = snackbar.getView();
//        snackView.setBackgroundColor(backgroundColor);
        snackbar.show();
    }
}
