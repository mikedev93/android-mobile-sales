package com.app.miguel.facturacionmobile.sales


import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.app.miguel.facturacionmobile.MessageDialogFragment
import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.SalesManager
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.models.SaleSummaryMobile
import com.app.miguel.facturacionmobile.persistencia.models.VentaDetalleModel
import com.app.miguel.facturacionmobile.print.PrintBTUtils
import com.app.miguel.facturacionmobile.print.PrintTask
import kotlinx.android.synthetic.main.fragment_sales_resume.*
import kotlinx.android.synthetic.main.view_toolbar_sales.*
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt


class SalesResumeFragment : Fragment(), MessageDialogFragment.MessageDialogListener {

    var mainActivity: SalesActivity? = null
    var saleSummary: SaleSummaryMobile? = null

    companion object {
        val saleKey = "key_sales"

        fun newInstance(saleSummary: SaleSummaryMobile): SalesResumeFragment {
            var fragment = SalesResumeFragment()
            var bundle = Bundle()
            bundle.putParcelable(saleKey, saleSummary)
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.saleSummary = arguments!!.getParcelable(saleKey)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sales_resume, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        setupInfo(saleSummary!!)

        btn_save.setOnClickListener {
            if (!PrintBTUtils.isBluetoothEnabled()) {
                Utils.showSnackbar(context!!, rl_main, PrintBTUtils.enableBluetoothMessage, "error")
            } else if (!PrintBTUtils.isDevicePaired()) {
                Utils.showSnackbar(context!!, rl_main, PrintBTUtils.deviceNotPairedMessage, "error")
            } else {
                var tipoPago = -1
                val simpleDateTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val fecha_hora = simpleDateTimeFormat.format(Date())
                if (saleSummary!!.ventaCabeceraModel.condicion_venta == 1) {
                    tipoPago = if (rb_efectivo.isChecked) {
                        1
                    } else if (rb_cheque.isChecked) 2 else {
                        3
                    }
                } else tipoPago = 4
                saleSummary!!.ventaCabeceraModel.tipo_pago = tipoPago
                saleSummary!!.ventaCabeceraModel.fecha_emision = fecha_hora
                if (DBUtils.saveSaleMobile(context!!, saleSummary!!) > 0) {
                    printSaleData(saleSummary!!)
                    SalesManager.selectedProductsMemory = null
                    showMessageDialog("Atención", "¡Venta guardada con éxito! \nImprimiendo...")
                    val handler = Handler()
                    handler.postDelayed({ mainActivity!!.finish() }, 5000)
                } else {
                    Utils.showSnackbar(context!!, rl_main, "¡Ocurrió un error al guardar la venta!", "error")
                }
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as SalesActivity
    }

    fun showMessageDialog(title: String, message: String) {
        val fragment = MessageDialogFragment.newInstance(title, message, this)
        fragment.show(mainActivity!!.supportFragmentManager, title)
    }

    private fun setupToolbar() {
        tv_title.text = "Resumen"
        btn_back.setOnClickListener {
            fragmentManager!!.popBackStack()
        }
        btn_next.visibility = View.GONE
    }

    override fun onDialogPositiveClick(dialog: DialogFragment?) {
//        mainActivity!!.finish()
    }

    private fun setupInfo(saleSummary: SaleSummaryMobile) {

        var formatter = NumberFormat.getNumberInstance(Locale.GERMAN) as DecimalFormat
        var entidadSucursalModel = DBUtils.getEntidadSucursal(context!!, saleSummary.ventaCabeceraModel.id_entidad_sucursal!!)

        tv_nombre_cliente.text = entidadSucursalModel!!.razonSocial
        nro_ruc.text = entidadSucursalModel!!.ruc
        tv_direccion_set.text = entidadSucursalModel!!.direccion

        var products: ArrayList<VentaDetalleModel> = saleSummary.ventaDetalles
        var iva5: Int = 0
        var iva10: Int = 0
        var exenta: Int = 0
        var totalGeneral: Long = 0
        var totalNoGravado: Long

        for (product in products) {
            when (product.iva) {
                0 -> exenta += product.precio
                5 -> iva5 += ((product.precio * product.cantidad) / 21).toDouble().roundToInt()
                10 -> iva10 += ((product.precio * product.cantidad) / 11).toDouble().roundToInt()
            }
            totalGeneral += (product.precio * product.cantidad)
        }

        totalNoGravado = totalGeneral - (iva10 + iva5)

        tv_subtotal.text = formatter.format(totalNoGravado)
        tv_exenta.text = formatter.format(exenta)
        tv_iva5.text = formatter.format(iva5)
        tv_iva10.text = formatter.format(iva10)
        tv_total_gral.text = formatter.format(totalGeneral)
        tv_factura.text = saleSummary.ventaCabeceraModel.nro_documento
        tv_condicion_venta.text = if (saleSummary.ventaCabeceraModel.condicion_venta == 1) "CONTADO" else "CRÉDITO"

        if (saleSummary.ventaCabeceraModel.condicion_venta != 1) {
            radio_group.visibility = View.GONE
        }
    }

    fun printSaleData(saleSummary: SaleSummaryMobile) {

        var printTask = PrintTask(context!!, saleSummary)
        printTask.execute()

//        val fragment = PrintPreviewFragment.newInstance(facturaImprimir)
//        mainActivity!!.supportFragmentManager
//                .beginTransaction()
//                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, 0, R.anim.slide_out_right)
//                .add(R.id.container, fragment, fragment.javaClass.simpleName)
//                .addToBackStack(fragment.javaClass.simpleName)
//                .commit()
    }
}
