package com.app.miguel.facturacionmobile.persistencia.servicios

import com.app.miguel.facturacionmobile.persistencia.models.*
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*

interface SyncAPI {

    @GET("productos")
    fun getProductos(@Query("id_remision") idRemision: Int): Call<List<ProductStockModel>>

    @GET("usuarios")
    fun getUsuarios(): Call<List<UsuarioModel>>

    @GET("param")
    fun getParamVta(@Query("id_remision") idRemision: Int): Call<List<ParametroVentaModel>>

    @GET("entidadsucursales")
    fun getEntidades(): Call<List<SucursalResponse>>

    @GET("geoCiudades")
    fun getCiudades(): Call<List<GeoCiudadModel>>

    @POST("pedidoVenta")
    fun postSales(@Body ventas: VentasRequest): Call<List<VentasResponse>>

    @POST("entidadesAlta")
    fun postNewClients(@Body entidades: EntidadesRequest): Call<List<NewEntidadesResponse>>
}