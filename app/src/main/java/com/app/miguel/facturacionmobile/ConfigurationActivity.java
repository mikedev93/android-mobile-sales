package com.app.miguel.facturacionmobile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.app.miguel.facturacionmobile.persistencia.database.DBHelper;
import com.google.android.material.textfield.TextInputLayout;

public class ConfigurationActivity extends AppCompatActivity {

    private Button buttonSave;
    private Button buttonReset;
    private TextView buttonBack;
    private EditText etHost;
    private EditText etPort;
    private EditText etService;
    private TextInputLayout inputLayoutHost;
    private TextInputLayout inputLayoutPort;
    private TextInputLayout inputLayoutService;
    private RadioButton rbDefault;
    private RadioButton rbOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        initViews();
        getSavedConfig();
    }

    private void initViews() {
        etHost = findViewById(R.id.et_host);
        etPort = findViewById(R.id.et_port);
        etService = findViewById(R.id.et_service);
        inputLayoutHost = findViewById(R.id.input_layout_host);
        inputLayoutPort = findViewById(R.id.input_layout_port);
        inputLayoutService = findViewById(R.id.input_layout_service);
        rbDefault = findViewById(R.id.rb_default);
        rbOther = findViewById(R.id.rb_other);

        buttonBack = findViewById(R.id.back_button);
        buttonSave = findViewById(R.id.btn_save);
        buttonReset = findViewById(R.id.btn_reset);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveConfig();
            }
        });

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetData();
            }
        });

        rbDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etHost.setText(getApplicationContext().getResources().getString(R.string.default_host));
                etHost.setEnabled(false);
                etPort.setText(getApplicationContext().getResources().getString(R.string.default_port));
                etPort.setEnabled(false);
                etService.setText(getApplicationContext().getResources().getString(R.string.default_service));
                etService.setEnabled(false);
            }
        });

        rbOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etHost.setEnabled(true);
                etHost.setText("");
                etPort.setEnabled(true);
                etPort.setText("");
                etService.setEnabled(true);
                etService.setText("");
            }
        });

    }

    private void getSavedConfig() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key_config_data), Context.MODE_PRIVATE);
        Boolean otherConf = sharedPreferences.getBoolean(getApplicationContext().getResources().getString(R.string.other_config_key), false);
        String host = sharedPreferences.getString(getApplicationContext().getResources().getString(R.string.host_key), null);
        String port = sharedPreferences.getString(getApplicationContext().getResources().getString(R.string.port_key), null);
        String service = sharedPreferences.getString(getApplicationContext().getResources().getString(R.string.service_key), null);
        if (otherConf) {
            rbOther.setChecked(true);
            if (host != null) {
                etHost.setText(host);
            }
            if (port != null) {
                etPort.setText(port);
            }
            if (service != null) {
                etService.setText(service);
            }
        } else {
            rbDefault.setChecked(true);
            etHost.setEnabled(false);
            etHost.setText(host);
            etPort.setEnabled(false);
            etPort.setText(port);
            etService.setEnabled(false);
            etService.setText(service);
        }
    }

    private boolean checkFields() {
        if (etHost.getText().length() == 0) {
            inputLayoutHost.setError("¡Host no puede estar vacío!");
            etHost.setFocusable(true);
            return false;
        }
        if (etPort.getText().length() == 0) {
            inputLayoutPort.setError("¡Puerto no puede estar vacío!");
            etPort.setFocusable(true);
            return false;
        }
//        if (etService.getText().length() == 0){
//            inputLayoutService.setError("¡Servicio no puede estar vacío!");
//            etService.setFocusable(true);
//            return false;
//        }
        return true;
    }

    private void saveConfig() {
        if (checkFields()) {
            Boolean otherConf = rbOther.isChecked();
            String host = etHost.getText().toString();
            String port = etPort.getText().toString();
            String service = etService.getText().toString();
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key_config_data), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(getApplicationContext().getResources().getString(R.string.other_config_key), otherConf);
            editor.putString(getApplicationContext().getResources().getString(R.string.host_key), host);
            editor.putString(getApplicationContext().getResources().getString(R.string.port_key), port);
            editor.putString(getApplicationContext().getResources().getString(R.string.service_key), service);
            editor.commit();
            finish();
        }
    }

    private void resetData() {
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(this, R.style.ThemeOverlay_AppCompat_Dialog_Alert).create();
        alertDialogBuilder.setTitle("Restablecer Datos");
        alertDialogBuilder.setMessage("Esta a punto de eliminar TODOS los datos de la aplicación.\n\n¿Esta seguro?");
        alertDialogBuilder.setButton(DialogInterface.BUTTON_POSITIVE,
                "Si, eliminar datos",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DBHelper dbHelper = new DBHelper(getApplicationContext());
                        dbHelper.onUpgrade(dbHelper.getWritableDatabase(), 1, 2);
                        dbHelper.onCreate(dbHelper.getWritableDatabase());
                        dbHelper.close();
                        alertDialogBuilder.dismiss();
                        Toast.makeText(getApplicationContext(), "Eliminando datos", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
        alertDialogBuilder.setButton(DialogInterface.BUTTON_NEGATIVE,
                "Cancelar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialogBuilder.dismiss();
                    }
                });
        alertDialogBuilder.show();
    }
}
