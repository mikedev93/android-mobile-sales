package com.app.miguel.facturacionmobile

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.app.miguel.facturacionmobile.Utils.Companion.getBaseURL
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.models.UsuarioModel
import com.app.miguel.facturacionmobile.persistencia.servicios.tasks.SyncUsersTask
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity() {

    private var mRestAdapter: Retrofit? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_login.setOnClickListener {
            var usuarioModel: UsuarioModel? = null
            if (BuildConfig.DEMO_USER && BuildConfig.DEMO_LOGIN) {
                usuarioModel = DBUtils.validateUser(applicationContext, "DEMO", "DEMO")
            } else {
                usuarioModel = DBUtils.validateUser(applicationContext, et_user.text.toString(), et_password.text.toString())
            }
            if (usuarioModel != null) {
                val intent = Intent(applicationContext, HomeActivity::class.java)
                var bundle = Bundle()
                bundle.putParcelable("usuario", usuarioModel)
                intent.putExtras(bundle)
                startActivity(intent)
                this.finish()
            } else {
                Utils.showSnackbar(applicationContext, findViewById(R.id.rl_main), "Usuario y/o contraseña incorrectos", "error")
            }
        }

        tv_config.setOnClickListener{
            val intent = Intent(applicationContext, ConfigurationActivity::class.java)
            startActivity(intent)
        }

        tv_sync.setOnClickListener {
            var cantidad = DBUtils.getSalesNotSyncd(applicationContext)
            if (cantidad > 0) {
                showSnackbarError("¡No se puede descargar!\nTiene ventas sin enviar")
            } else {
                sync()
            }
        }

        version.text = "Version: ${BuildConfig.VERSION_NAME}"
        fecha.text = "Fecha: ${BuildConfig.RELEASE_DATE}"

        val bundle = intent.extras
        if (bundle != null) {
            var mensaje_error = bundle.getString("mensaje_error")
            Utils.showSnackbar(applicationContext, findViewById(R.id.rl_main), mensaje_error, "error")
        }
    }

    override fun onResume() {
        super.onResume()
        if (getBaseURL(applicationContext) == null) {
            Utils.saveDefaultServerConf(applicationContext)
//            val backMessage = "Para sincronizar debe configurar el host y el puerto."
//            Utils.showSnackbar(applicationContext, findViewById(R.id.rl_main), backMessage, "warning")
        }
    }

    fun showSnackbarError(message: String) {
        Utils.showSnackbar(applicationContext, findViewById(R.id.rl_main), message, Utils.TYPE_ERROR)
    }

    fun showSnackbar(message: String) {
        Utils.showSnackbar(applicationContext, findViewById(R.id.rl_main), message, Utils.TYPE_SYNC)
    }

    fun sync() {
        if (!Utils.getBaseURL(applicationContext).isNullOrEmpty()) {
            showLoading(true)
            var task = SyncUsersTask(applicationContext, this)
            task.execute()
        } else {
            val backMessage = "Para sincronizar debe configurar el host y el puerto."
            Utils.showSnackbar(applicationContext, findViewById(R.id.rl_main), backMessage, "warning")
        }
    }

    fun showLoading(show: Boolean){
        rl_curtain.visibility = if (show) View.VISIBLE else View.GONE
    }
}
