package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class GeoCiudadesContract {

    class GeoCiudadesEntry: BaseColumns {
        companion object {
            const val TABLE_NAME: String = "mgeociudades"

            const val _ID: String = BaseColumns._ID
            const val ID_CIUDAD_M: String = "idciudad_m"
            const val NOMBRE_CIUDAD_DEPTO: String = "nom_ciu_dep"
            const val CIUDAD_ID: String = "ciudadid"
            const val CIUDAD_DESCRIPCION: String = "ciudaddescripcion"
            const val DEPARTAMENTO_ID: String = "departamentoid"
            const val DEPARTAMENTO_DESCRIPCION: String = "departamentodescripcion"
            const val ALTA_FECHA_HORA: String = "altafechahora"
            const val ALTA_USUARIO: String = "altausuario"
        }
    }
}