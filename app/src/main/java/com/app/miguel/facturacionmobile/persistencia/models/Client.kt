package com.app.miguel.facturacionmobile.persistencia.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Client (@Expose @SerializedName("id_producto") var id: Int,
              @Expose @SerializedName("nombre") var nombre: String?,
              @Expose @SerializedName("ruc") var ruc: String?,
              @Expose @SerializedName("direccion") var direccion: String?,
              @Expose @SerializedName("ciudad") var ciudad: String?,
              @Expose @SerializedName("barrio") var barrio: String?,
              @Expose @SerializedName("departamento") var departamento: String?,
              @Expose @SerializedName("telefono") var telefono: String?,
              @Expose var checked: Boolean = false) : Parcelable