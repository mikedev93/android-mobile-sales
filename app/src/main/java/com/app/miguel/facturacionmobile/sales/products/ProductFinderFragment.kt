package com.app.miguel.facturacionmobile.sales.products


import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.models.ListProductModel
import com.app.miguel.facturacionmobile.persistencia.models.ProductModel
import com.app.miguel.facturacionmobile.sales.SalesActivity
import com.app.miguel.facturacionmobile.sales.SalesResumeFragment
import kotlinx.android.synthetic.main.fragment_product_finder.*

class ProductFinderFragment : Fragment() {

    var mainActivity: SalesActivity? = null
    var productsList: ArrayList<ProductModel>? = null
    var searchList: ArrayList<ProductModel>? = null
    var recyclerView: RecyclerView? = null
    var recyclerAdapter: ProductFinderRecyclerAdapter? = null
    var selectedProduct: ProductModel? = null
    var mListener: OnProductSelectedListener? = null
    var selectedProductList: ListProductModel? = null

    companion object {
        val productsKey = "key_products"

        fun newInstance(selectedProductList: ListProductModel): ProductFinderFragment {
            var fragment = ProductFinderFragment()
            var bundle = Bundle()
            bundle.putParcelable(productsKey, selectedProductList)
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedProductList = arguments!!.getParcelable(productsKey) as ListProductModel
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_finder, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as SalesActivity
        mListener = context
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back_button.setOnClickListener {
            fragmentManager!!.popBackStack()
        }

        btn_continue.setOnClickListener {
            mListener!!.productSelected(selectedProduct!!)
            fragmentManager!!.popBackStackImmediate()
        }
        productsList = DBUtils.getAllProductosWithStock(context!!)
        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerAdapter = ProductFinderRecyclerAdapter(context, productsList, object : ProductFinderRecyclerAdapter.ProductFinderClickListener{
            override fun onClick(product: ProductModel?) {
                selectedProduct = product
                var productExists  = false
                if (selectedProductList != null && product != null) {
                    for (item in selectedProductList!!.productList) {
                        if (item.id == product!!.id) productExists = true
                    }
                }
                if (!productExists) {
                    btn_continue.isEnabled = product != null
                } else {
                    Utils.showSnackbar(context!!, rl_main, "Producto ya agregado", Utils.TYPE_ERROR)
                }
            }
        })
        recyclerView!!.adapter = recyclerAdapter

        et_search.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                searchProducts(s.toString())
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchProducts(s.toString())
            }

        })
    }

    fun searchProducts(terms: String?) {
        if (terms!!.trim().length > 1) {
            searchList = ArrayList()
            for (index in productsList!!.indices) {
                val item = productsList!!.get(index)
                if (item.nombre.toLowerCase().contains(terms.toLowerCase()) || item.codigo?.toLowerCase()!!.contains(terms.toLowerCase())) {
                    searchList!!.add(item)
                }
            }
            recyclerAdapter = ProductFinderRecyclerAdapter(context, searchList, object : ProductFinderRecyclerAdapter.ProductFinderClickListener{
                override fun onClick(product: ProductModel?) {
                    selectedProduct = product
                    btn_continue.isEnabled = product != null
                }
            })
            recyclerView!!.adapter = recyclerAdapter
            recyclerAdapter!!.notifyDataSetChanged()
        } else {
            recyclerAdapter = ProductFinderRecyclerAdapter(context, productsList, object : ProductFinderRecyclerAdapter.ProductFinderClickListener{
                override fun onClick(product: ProductModel?) {
                    selectedProduct = product
                    btn_continue.isEnabled = product != null
                }
            })
            recyclerView!!.adapter = recyclerAdapter
            recyclerAdapter!!.notifyDataSetChanged()
        }
    }

    interface OnProductSelectedListener {
        fun productSelected(product: ProductModel)
    }
}
