package com.app.miguel.facturacionmobile.sales;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.app.miguel.facturacionmobile.R;
import com.app.miguel.facturacionmobile.persistencia.models.ProductModel;
import com.app.miguel.facturacionmobile.sales.products.ProductEditFragment;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class SalesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<ProductModel> productList;
    private int rowIndex = -1;
    private SalesActivity mainActivity;

    public SalesRecyclerAdapter(Context context, ArrayList<ProductModel> productList) {
        this.context = context;
        this.productList = productList;
        this.mainActivity = (SalesActivity) context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.view_sale_item_card_cl, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final ProductModel product = productList.get(position);
        final ViewHolder holder = (ViewHolder) viewHolder;

        DecimalFormat formatter = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);

        holder.vTvCantidad.setText(formatter.format(product.getCantidad()));
        holder.vTvDescripcion.setText(product.getNombre());
        holder.vTvTotal.setText(formatter.format(product.getPrecio_venta() * product.getCantidad()));

        holder.vConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductEditFragment fragment = new ProductEditFragment().Companion.newInstance(product, position);
                mainActivity.showCurtain(true);
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.slide_down, 0, 0, R.anim.slide_down_out)
                        .add(R.id.popup_container, fragment, fragment.getClass().getSimpleName())
                        .addToBackStack(fragment.getClass().getSimpleName())
                        .commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

//        private final LinearLayout vLinearLayout;
        private final ConstraintLayout vConstraintLayout;
        private final TextView vTvCantidad;
        private final TextView vTvDescripcion;
        private final TextView vTvTotal;

        public ViewHolder(View itemView) {
            super(itemView);
//            vLinearLayout = itemView.findViewById(R.id.ll_content);
            vConstraintLayout = itemView.findViewById(R.id.cl_content);
            vTvCantidad = itemView.findViewById(R.id.tv_cantidad);
            vTvDescripcion = itemView.findViewById(R.id.tv_descripcion);
            vTvTotal = itemView.findViewById(R.id.tv_total);
        }
    }
}
