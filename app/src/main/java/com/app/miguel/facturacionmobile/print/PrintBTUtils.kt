package com.app.miguel.facturacionmobile.print

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.miguel.facturacionmobile.BuildConfig
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.StandardCharsets
import java.util.*


class PrintBTUtils {

    companion object {
        private const val deviceName = "RG-MDP58B"
        private var bluetoothAdapter: BluetoothAdapter? = null
        private var socket: BluetoothSocket? = null
        private var bluetoothDevice: BluetoothDevice? = null
        private var outputStream: OutputStream? = null
        private var inputStream: InputStream? = null
        private var workerThread: Thread? = null
        private var readBuffer: ByteArray? = null
        private var readBufferPosition: Int = 0
        @Volatile
        private var stopWorker: Boolean = false
        private var value = ""
        private var context: Context? = null
        private var activity: AppCompatActivity? = null

        var enableBluetoothMessage = "¡Por favor encienda el bluetooth del dispositivo!"
        var deviceNotPairedMessage = "¡Por favor vincule la Impresora Bluetooth!"

        fun startPrintJob(context: Context, textValue: String) {
            Companion.context = context
            activity = context as AppCompatActivity
            intentPrint(textValue)
        }

        private fun intentPrint(txtvalue: String) {
            val buffer = txtvalue.toByteArray()
            val printHeader = byteArrayOf(0xAA.toByte(), 0x55, 2, 0)
            printHeader[3] = buffer.size.toByte()
            initPrinter()
            if (printHeader.size > 128) {
                value += "\nValue is more than 128 size\n"
                Toast.makeText(context, value, Toast.LENGTH_LONG).show()
            } else {
                try {
                    outputStream!!.write(txtvalue.toByteArray())
                    outputStream!!.close()
                    socket!!.close()
                } catch (ex: Exception) {
                    value += "$ex\nExcep intentPrint \n"
                    activity!!.runOnUiThread(Runnable {
                        Log.e("intentPrint", value, ex)
                        Toast.makeText(context, "Sin conexión con la impresora", Toast.LENGTH_LONG).show()
                    })
                }

            }
        }

        private fun initPrinter() {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            try {
                if (!bluetoothAdapter!!.isEnabled) {
                    val enableBluetooth = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    activity!!.startActivityForResult(enableBluetooth, 0)
                }

                val pairedDevices = bluetoothAdapter!!.bondedDevices
                if (pairedDevices.size > 0) {
                    for (device in pairedDevices) {
                        if (device.name == deviceName)
                        //Note, you will need to change this to match the name of your device
                        {
                            bluetoothDevice = device
                            break
                        }
                    }

                    val uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB") //Standard SerialPortService ID
                    val method = bluetoothDevice!!.javaClass.getMethod("createRfcommSocket", (Int::class.javaPrimitiveType))
                    socket = method.invoke(bluetoothDevice, 1) as BluetoothSocket
                    bluetoothAdapter!!.cancelDiscovery()
                    socket!!.connect()
                    outputStream = socket!!.getOutputStream()
                    inputStream = socket!!.getInputStream()
                    beginListenForData()
                } else {
                    value += "No Devices found"
                    Toast.makeText(context, value, Toast.LENGTH_LONG).show()
                    return
                }
            } catch (ex: Exception) {
                value += "$ex\n initPrinter \n"
                activity!!.runOnUiThread(Runnable {
                    Log.e("initPrinter", value, ex)
                    Toast.makeText(context, "No se pudo inicializar la impresora", Toast.LENGTH_LONG).show()
                })
            }

        }

        private fun beginListenForData() {
            try {
                val handler = Handler()

                // this is the ASCII code for a newline character
                val delimiter: Byte = 10
                stopWorker = false
                readBufferPosition = 0
                readBuffer = ByteArray(1024)

                workerThread = Thread(Runnable {
                    while (!Thread.currentThread().isInterrupted && !stopWorker) {
                        try {
                            val bytesAvailable = inputStream!!.available()
                            if (bytesAvailable > 0) {
                                val packetBytes = ByteArray(bytesAvailable)
                                inputStream!!.read(packetBytes)
                                for (i in 0 until bytesAvailable) {
                                    val b = packetBytes[i]
                                    if (b == delimiter) {
                                        val encodedBytes = ByteArray(readBufferPosition)
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.size
                                        )

                                        // specify US-ASCII encoding
//                                        val data = String(encodedBytes, Charset.forName("US-ASCII"))
//                                        val data = String(encodedBytes, Charset.forName("UTF-8"))
                                        val data = String(encodedBytes, StandardCharsets.US_ASCII)
                                        readBufferPosition = 0

                                        // tell the user data were sent to bluetooth printer device
                                        handler.post { Log.d("e", data) }
                                    } else {
                                        readBuffer!![readBufferPosition++] = b
                                    }
                                }
                            }
                        } catch (ex: IOException) {
                            stopWorker = true
                        }
                    }
                })
                workerThread!!.start()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        fun isBluetoothEnabled(): Boolean {
            if (BuildConfig.USE_PRINTER) {
                bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                return bluetoothAdapter!!.isEnabled
            } else return true
        }

        fun enableBluetooth() {
            if (!isBluetoothEnabled()) {
                val enableBluetooth = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                activity!!.startActivityForResult(enableBluetooth, 0)
            }
        }

        fun isDevicePaired(): Boolean {
            if (BuildConfig.USE_PRINTER) {
                bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                try {
                    if (!isBluetoothEnabled()) {
                        val enableBluetooth = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                        activity!!.startActivityForResult(enableBluetooth, 0)
                    }

                    val pairedDevices = bluetoothAdapter!!.bondedDevices
                    if (pairedDevices.size > 0) {
                        for (device in pairedDevices) {
                            if (device.name == deviceName) {
                                bluetoothDevice = device
                                return true
                            }
                        }
                        if (bluetoothDevice == null) {
                            return false
                        }
                    } else {
                        return false
                    }
                } catch (ex: Exception) {
                    return false
                }
                return false
            } else return true
        }
    }
}