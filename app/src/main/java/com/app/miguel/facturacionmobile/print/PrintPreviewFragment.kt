package com.app.miguel.facturacionmobile.print


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.app.miguel.facturacionmobile.R
import kotlinx.android.synthetic.main.fragment_print_preview.*
import kotlinx.android.synthetic.main.view_toolbar_sales.*

/**
 * A simple [Fragment] subclass.
 */
class PrintPreviewFragment : Fragment() {

    var printText: String? = null

    companion object {

        fun newInstance(text: String): PrintPreviewFragment {
            var fragment = PrintPreviewFragment()
            var bundle = Bundle()
            bundle.putString("print", text)
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        printText = arguments!!.getString("print")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_print_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        tv_print_preview.text = printText
    }

    private fun setupToolbar() {
        tv_title.text = "Resumen"
        btn_back.setOnClickListener {
            fragmentManager!!.popBackStack()
        }
        btn_next.visibility = View.GONE
    }
}
