package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class EntidadesContract {

    class EntidadesEntry: BaseColumns {
        companion object {
            const val TABLE_NAME = "mentidades"

            const val _ID = BaseColumns._ID
            const val ID_ENTIDAD = "identidad"
            const val RAZON_SOCIAL = "razonsocial"
            const val NOMBRE_COMERCIAL = "nombrecomercial"
            const val RUC = "ruc"
            const val DV = "dv"
            const val ID_PEDIDO_REMISION = "idpedido_remision"
            const val ALTA_FECHA_HORA = "altafechahora"
            const val ALTA_USUARIO = "altausuario"
            const val MODIFICADO_FECHA_HORA = "modificadofechahora"
            const val MODIFICADO_USUARIO = "modificadousuario"
        }
    }
}
