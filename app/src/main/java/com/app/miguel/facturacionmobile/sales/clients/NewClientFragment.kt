package com.app.miguel.facturacionmobile.sales.clients


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.SalesManager
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.models.EntidadSucursalModel
import com.app.miguel.facturacionmobile.persistencia.models.GeoCiudadModel
import com.app.miguel.facturacionmobile.persistencia.models.NewEditClientModel
import com.app.miguel.facturacionmobile.sales.SalesActivity
import com.app.miguel.facturacionmobile.sales.SalesFragment
import kotlinx.android.synthetic.main.fragment_new_client.*
import kotlinx.android.synthetic.main.fragment_new_client.view.*

/**
 * A simple [Fragment] subclass.
 */
class NewClientFragment : Fragment() {

    var mainActivity: SalesActivity? = null
    var mListener: OnClientCreatedListener? = null
    var selectedCity: GeoCiudadModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_client, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as SalesActivity
        mListener = context as OnClientCreatedListener
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onResume() {
        super.onResume()
        if (SalesManager.selectedCity != null){
            selectedCity = SalesManager.selectedCity
            var tvDepartamento = view!!.findViewById(R.id.tv_departamento_frag) as TextView
            tvDepartamento.text = SalesManager.selectedCity!!.nombreCiudadDepto
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back_button.setOnClickListener {
            fragmentManager!!.popBackStack()
        }

        btn_save.setOnClickListener {
            if (checkFields()) {
                var idEntidadSucursal = DBUtils.saveNewEntidadSucursal(context!!, collectClientData())
//                var entidadSucursalModel = DBUtils.getEntidadSucursal(context!!, idEntidadSucursal)
                SalesManager.selectedCity = null

                val fragment = SalesFragment.newInstance(idEntidadSucursal)
                mainActivity!!.supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, 0, R.anim.slide_out_right)
                        .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                        .addToBackStack(fragment.javaClass.simpleName)
                        .commit()

//                mListener!!.clientCreated(entidadSucursalModel!!)
//                fragmentManager!!.popBackStack()
            }
        }

        rl_selector_ciudad.setOnClickListener {
            val fragment = CityFinderFragment()
            mainActivity!!.supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, 0, R.anim.slide_out_right)
                    .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                    .addToBackStack(fragment.javaClass.simpleName)
                    .commit()
        }
    }

    fun checkFields(): Boolean {
        input_layout_nombre.isErrorEnabled = false
        input_layout_ruc.isErrorEnabled = false
        input_layout_direccion.isErrorEnabled = false
        input_layout_telefono.isErrorEnabled = false
        if (et_nombre.text.isNullOrEmpty()) {
            input_layout_nombre.error = "¡El campo Nombre no puede estar vacío!"
            et_nombre.requestFocus()
            return false
        }
        if (et_ruc.text.isNullOrEmpty()) {
            input_layout_ruc.error = "¡El campo RUC no puede estar vacío!"
            et_ruc.requestFocus()
            return false
        }
        if (et_direccion.text.isNullOrEmpty()) {
            input_layout_direccion.error = "¡El campo Dirección no puede estar vacío!"
            et_direccion.requestFocus()
            return false
        }
        if (et_telefono.text.isNullOrEmpty()) {
            input_layout_telefono.error = "¡El campo Teléfono no puede estar vacío!"
            et_telefono.requestFocus()
            return false
        }
        if ("Seleccionar" == tv_departamento_frag.text){
            Utils.showSnackbar(context!!, rl_main, "¡Debe seleccionar una ciudad!", Utils.TYPE_ERROR)
            return false
        }
        if (DBUtils.getRUCExists(context!!, et_ruc.text.toString())){
            Utils.showSnackbar(context!!, rl_main, "¡Ya existe un cliente con ese RUC!", Utils.TYPE_ERROR)
            return false
        }
        return true
    }

    fun setCity(ciudadModel: GeoCiudadModel){
        selectedCity = ciudadModel
//        var tvDepartamento = root!!.findViewById(R.id.tv_departamento_frag) as TextView
//        tv_departamento_frag.text = ciudadModel.nombreCiudadDepto
    }

    fun collectClientData (): NewEditClientModel {
        var client =  NewEditClientModel(null, null, et_ruc.text.toString().trim(), et_nombre.text.toString().toUpperCase().trim(), et_direccion.text.toString().toUpperCase().trim(),
                et_telefono.text.toString().trim(), selectedCity!!.departamentoId, selectedCity!!.ciudadId, DBUtils.getSaleParameters(context!!)!!.idpedido_remision,
                SalesManager.loggedUser!!.id_usuario, null, null, null)

        return client
    }

    interface OnClientCreatedListener {
        fun clientCreated(client: EntidadSucursalModel)
    }
}
