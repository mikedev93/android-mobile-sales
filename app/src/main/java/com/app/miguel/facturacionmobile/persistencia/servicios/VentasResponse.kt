package com.app.miguel.facturacionmobile.persistencia.servicios

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VentasResponse (@Expose @SerializedName("_id") var id: Int?,
                      @Expose @SerializedName("id_venta") var idVenta: Int?,
                      @Expose @SerializedName("id_venta_bd") var idVentaBD: Int?,
                      @Expose @SerializedName("success") var success: Boolean?)