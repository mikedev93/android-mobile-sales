package com.app.miguel.facturacionmobile.sales.products


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.models.ProductModel
import com.app.miguel.facturacionmobile.sales.SalesActivity
import kotlinx.android.synthetic.main.pop_up_edit_product.*
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class ProductEditFragment : Fragment() {

    var mainActivity: SalesActivity? = null
    var mListener: OnProductChangedListener? = null
    var selectedProduct: ProductModel? = null
    var selectedProductPosition: Int? = null

    companion object {
        var productKey = "selected_product"
        var positionKey = "position"
        fun newInstance(product: ProductModel, position: Int): ProductEditFragment {
            var fragment = ProductEditFragment()
            var bundle = Bundle()
            bundle.putParcelable(productKey, product)
            bundle.putInt(positionKey, position)
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedProduct = arguments!!.getParcelable(productKey) as ProductModel
        selectedProductPosition = arguments!!.getInt(positionKey)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_edit, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as SalesActivity
        mListener = context as OnProductChangedListener
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tv_product_name.text = selectedProduct!!.nombre
        et_cantidad.setText(selectedProduct!!.cantidad.toString())

        if (selectedProduct!!.precioCaja!! > 0) {
            input_layout_precio.hint = "Precio Caja"
            var precio = selectedProduct!!.precio_venta
            if (selectedProduct!!.precioCaja!! != precio) {
                et_precio.setText(precio.toInt().toString())
            } else {
                et_precio.setText(selectedProduct!!.precioCaja!!.toInt().toString())
            }
        } else {
            input_layout_precio.hint = "Precio Unitario"
            var precio = selectedProduct!!.precio_venta/selectedProduct!!.composicionVenta
            if (selectedProduct!!.precioUnidad!! != precio) {
                et_precio.setText(precio.toInt().toString())
            } else {
                et_precio.setText(selectedProduct!!.precioUnidad!!.toInt().toString())
            }
        }

        btn_delete.setOnClickListener {
            mListener!!.deleteProduct(selectedProduct!!)
            fragmentManager!!.popBackStackImmediate()
            mainActivity!!.showCurtain(false)
        }

        btn_accept.setOnClickListener{
            if (checkFields()) {
                selectedProduct!!.cantidad = et_cantidad.text.toString().toInt()
                var precio = if (selectedProduct!!.precioCaja!! > 0) et_precio.text.toString().toDouble() else et_precio.text.toString().toDouble() * selectedProduct!!.composicionVenta
                selectedProduct!!.precio_venta = precio
                mListener!!.updateProduct(selectedProduct!!, selectedProductPosition!!)
                fragmentManager!!.popBackStackImmediate()
                mainActivity!!.showCurtain(false)
            }
        }
    }

    fun checkFields(): Boolean {
        input_layout_cantidad.isErrorEnabled = false
        input_layout_precio.isErrorEnabled = false

        var parametroVenta = DBUtils.getSaleParameters(context!!)
        var porcentajeVariacion = parametroVenta!!.porcentajeVariacionPrecio
        var precio: Double = (if (selectedProduct!!.precioCaja!! > 0) selectedProduct!!.precioCaja else selectedProduct!!.precioUnidad)!!
        var variacion: Double = selectedProduct!!.precioBase!! + ((selectedProduct!!.precioBase!! * porcentajeVariacion!!)/100)

        if (et_cantidad.text.isNullOrEmpty() || et_cantidad.text.toString().toInt() < 1) {
            input_layout_cantidad.error = "¡La cantidad no puede ser menor a 1!"
            et_cantidad.requestFocus()
            return false
        }
        if (et_precio.text.isNullOrEmpty() || et_precio.text.toString().toDouble() < selectedProduct!!.precioBase!!) {
            input_layout_precio.error = "¡El precio no puede ser menor al precio base!"
            et_precio.requestFocus()
            return false
        }
        if (et_precio.text.toString().toDouble() > variacion) {
            input_layout_precio.error = "¡El precio no puede superar el $porcentajeVariacion% del precio base!"
            et_precio.requestFocus()
            return false
        }
        return true
    }

    interface OnProductChangedListener {
        fun deleteProduct(product: ProductModel)
        fun updateProduct(product: ProductModel, position: Int)
    }
}
