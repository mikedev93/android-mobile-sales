package com.app.miguel.facturacionmobile.cancelInvoice


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.SalesManager
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.database.SaleData
import com.app.miguel.facturacionmobile.print.PrintBTUtils
import com.app.miguel.facturacionmobile.print.PrintTask
import kotlinx.android.synthetic.main.fragment_cancel_invoice_popup.*
import kotlinx.android.synthetic.main.pop_up_cancel_invoice.*
import kotlinx.android.synthetic.main.pop_up_edit_product.btn_accept
import kotlinx.android.synthetic.main.pop_up_edit_product.btn_delete

/**
 * A simple [Fragment] subclass.
 */
class CancelInvoicePopupFragment : Fragment() {

    var mainActivity: CancelInvoiceActivity? = null
    var selectedInvoice: SaleData? = null
    var selectedInvoicePosition: Int = -1
    var mListener: OnSaleItemCancelledListener? = null

    companion object {
        var invoiceKey = "invoice"
        var positionKey = "position"
        fun newInstance(saleData: SaleData, position: Int): CancelInvoicePopupFragment {
            var fragment = CancelInvoicePopupFragment()
            var bundle = Bundle()
            bundle.putParcelable(invoiceKey, saleData)
            bundle.putInt(positionKey, position)
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedInvoice = arguments!!.getParcelable(invoiceKey) as SaleData
        selectedInvoicePosition = arguments!!.getInt(positionKey)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as CancelInvoiceActivity
        mListener = context as OnSaleItemCancelledListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cancel_invoice_popup, container, false)
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var estado = DBUtils.getInvoiceState(context!!, selectedInvoice!!.id_venta)
        var sincronizado = DBUtils.getInvoiceSyncd(context!!, selectedInvoice!!.id_venta)

        tv_factura.text = selectedInvoice!!.factura
        tv_cliente.text = selectedInvoice!!.client!!.nombrecomercial
        tv_ruc.text = selectedInvoice!!.client!!.ruc
        tv_estado.text = if ("X" == estado) "ANULADO" else "ACTIVO"

        if ("X" == estado || sincronizado) {
            btn_delete.isEnabled = false
            btn_delete.background = context!!.getDrawable(R.drawable.button_rounded_disabled)
            btn_print.visibility = View.GONE
        } else {
            btn_delete.isEnabled = true
            btn_delete.background = context!!.getDrawable(R.drawable.button_rounded_red)
            btn_print.visibility = View.VISIBLE
        }

        btn_delete.setOnClickListener {
            DBUtils.cancelInvoice(context!!, selectedInvoice!!.id_venta, SalesManager.loggedUser!!.id_usuario!!)
            mListener!!.cancelInvoice(selectedInvoice!!, selectedInvoicePosition)
            fragmentManager!!.popBackStackImmediate()
            mainActivity!!.showCurtain(false)
        }

        btn_accept.setOnClickListener {
            fragmentManager!!.popBackStackImmediate()
            mainActivity!!.showCurtain(false)
        }

        btn_print.setOnClickListener {
            if (!PrintBTUtils.isBluetoothEnabled()){
                Utils.showSnackbar(context!!, rl_main, PrintBTUtils.enableBluetoothMessage, "error")
            } else if (!PrintBTUtils.isDevicePaired()){
                Utils.showSnackbar(context!!, rl_main, PrintBTUtils.deviceNotPairedMessage, "error")
            } else {
                DBUtils.updateSaleReimpresion(context!!, SalesManager.loggedUser!!.id_usuario!!, selectedInvoice!!.id_venta)
                var saleSummaryMobile = DBUtils.getSaleFull(context!!, selectedInvoice!!.id_venta)
                Toast.makeText(context, "Reimprimiendo factura...", Toast.LENGTH_LONG).show()
                var task = PrintTask(context!!, saleSummaryMobile!!)
                task.execute()
                fragmentManager!!.popBackStackImmediate()
                mainActivity!!.showCurtain(false)
            }
        }
    }

    interface OnSaleItemCancelledListener {
        fun cancelInvoice(saleData: SaleData, position: Int)
    }
}
