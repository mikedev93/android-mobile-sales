package com.app.miguel.facturacionmobile.cancelInvoice

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.database.SaleData
import kotlinx.android.synthetic.main.fragment_cancel_invoice.*
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class CancelInvoiceFragment : Fragment() {

    var mainActivity: CancelInvoiceActivity? = null
    var salesList: ArrayList<SaleData>? = null
    var searchList: ArrayList<SaleData>? = null
    var recyclerView: RecyclerView? = null
    var recyclerAdapter: CancelInvoiceRecyclerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cancel_invoice, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back_button.setOnClickListener {
            Utils.hideSoftKeyboard(activity!!)
            mainActivity!!.finish()
        }

        salesList = DBUtils.getSales(context!!)
        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerAdapter = CancelInvoiceRecyclerAdapter(context!!, salesList)
        recyclerView!!.adapter = recyclerAdapter

        setTotalSales(salesList!!)

        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                searchProducts(s.toString())
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchProducts(s.toString())
            }

        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mainActivity = context as CancelInvoiceActivity

    }

    fun searchProducts(terms: String?) {
        if (terms!!.trim().length > 1) {
            searchList = ArrayList()
            for (index in salesList!!.indices) {
                val item = salesList!!.get(index)
                if (Utils.extraerNroFactura(item.factura).contains(terms)) {
                    searchList!!.add(item)
                }
            }
            recyclerAdapter = CancelInvoiceRecyclerAdapter(context!!, searchList)
            recyclerView!!.adapter = recyclerAdapter
            recyclerAdapter!!.notifyDataSetChanged()
        } else {
            recyclerAdapter = CancelInvoiceRecyclerAdapter(context, salesList)
            recyclerView!!.adapter = recyclerAdapter
            recyclerAdapter!!.notifyDataSetChanged()
        }
    }

    fun cancelInvoice(saleData: SaleData, position: Int) {
        if (salesList!!.contains(saleData)){
            this.salesList!![position] = saleData
            recyclerAdapter = CancelInvoiceRecyclerAdapter(context!!, salesList)
            recyclerView!!.adapter = recyclerAdapter
            this.recyclerAdapter!!.notifyDataSetChanged()
        }
        if (searchList!= null) {
            if (searchList!!.contains(saleData)) {
                this.searchList!![position] = saleData
                recyclerAdapter = CancelInvoiceRecyclerAdapter(context!!, searchList)
                recyclerView!!.adapter = recyclerAdapter
                this.recyclerAdapter!!.notifyDataSetChanged()
            }
        }
        Utils.showSnackbar(context!!, rl_main, "¡Factura anulada con éxito!", Utils.TYPE_INFO)
        setTotalSales(salesList!!)
    }

    private fun setTotalSales(saleList: ArrayList<SaleData>){
        var formatterGS = NumberFormat.getNumberInstance(Locale.GERMAN) as DecimalFormat
        var amount: Long = 0
        for (item in saleList) {
            if (DBUtils.getInvoiceState(context!!, item.id_venta) != "X"){
                amount += DBUtils.getSaleFull(context!!, item.id_venta)!!.ventaCabeceraModel.total_pagado!!
            }
        }
        tv_importe.text = formatterGS.format(amount)
        tv_cant_facturas.text = formatterGS.format(saleList.size)
    }
}
