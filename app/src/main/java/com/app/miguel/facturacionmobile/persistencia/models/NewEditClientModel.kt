package com.app.miguel.facturacionmobile.persistencia.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NewEditClientModel (@Expose @SerializedName("identidad") var identidad: Int?,
                          @Expose @SerializedName("identidadsucursal") var identidadsucursal: Int?,
                          @Expose @SerializedName("ruc") var ruc: String?,
                          @Expose @SerializedName("razonsocial") var razonsocial: String?,
                          @Expose @SerializedName("direccion") var direccion: String?,
                          @Expose @SerializedName("telefono") var telefono: String?,
                          @Expose @SerializedName("departamento") var departamento: Int?,
                          @Expose @SerializedName("ciudad") var ciudad: Int?,
                          @Expose @SerializedName("idpedido_remision") var idpedido_remision: Int?,
                          @Expose @SerializedName("altausuario") var altausuario: Int?,
                          @Expose @SerializedName("altafechahora") var altafechahora: String?,
                          @Expose @SerializedName("modificadousuario") var modificadousuario: Int?,
                          @Expose @SerializedName("modificadofechahora") var modificadofechahora: String?)