package com.app.miguel.facturacionmobile.persistencia.servicios

import com.app.miguel.facturacionmobile.persistencia.models.EntidadSucursalModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EntidadesRequest (@Expose @SerializedName("entidadesAlta") var entidades: ArrayList<EntidadSucursalModel>?)