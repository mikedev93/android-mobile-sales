package com.app.miguel.facturacionmobile.persistencia

import com.app.miguel.facturacionmobile.persistencia.models.GeoCiudadModel
import com.app.miguel.facturacionmobile.persistencia.models.ProductModel
import com.app.miguel.facturacionmobile.persistencia.models.UsuarioModel

class SalesManager {

    companion object {
        var loggedUser: UsuarioModel? = null
        var selectedCity: GeoCiudadModel? = null
        var selectedProductsMemory: ArrayList<ProductModel>? = null
    }
}