package com.app.miguel.facturacionmobile.persistencia.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.math.BigInteger

class ParametroVentaModel (@Expose @SerializedName("id") var idparam_vta_mobile: Int?,
                           @Expose @SerializedName("idPedidoRemision") var idpedido_remision: Int?,
                           @Expose @SerializedName("nroIniFcauto") var nroini_fcauto: Long?,
                           @Expose @SerializedName("nroDesdeFcmanual") var nrodesde_fcmanual: Long?,
                           @Expose @SerializedName("nroHastaFcmanual") var nrohasta_fcmanual: Long?,
                           @Expose @SerializedName("vendedor") var vendedor: Int?,
                           @Expose @SerializedName("direccion") var direccion: String?,
                           @Expose @SerializedName("pais") var pais: String?,
                           @Expose @SerializedName("departamento") var departamento: String?,
                           @Expose @SerializedName("ciudad") var ciudad: String?,
                           @Expose @SerializedName("timbrado") var timbrado: Long?,
                           @Expose @SerializedName("validoDesde") var validoDesde: String?,
                           @Expose @SerializedName("validoHasta") var validoHasta: String?,
                           @Expose @SerializedName("porcentajeVariacionPrecio") var porcentajeVariacionPrecio: Double?,
                           @Expose @SerializedName("idEmpresa") var idEmpresa: Int?,
                           @Expose @SerializedName("idSucursal") var idSucursal: String?,
                           @Expose @SerializedName("idCaja") var idCaja: String?,
                           @Expose @SerializedName("ruc") var ruc: String?,
                           @Expose @SerializedName("razonSocial") var razonSocial: String?,
                           @Expose @SerializedName("tipoPedidoVenta") var tipoPedidoVenta: Int?,
                           @Expose @SerializedName("telefono") var telefono: String?)

class ListParametroVenta(@Expose @SerializedName("parametros") var parametros: ArrayList<ParametroVentaModel>)