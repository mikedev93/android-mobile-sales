package com.app.miguel.facturacionmobile.tooltip;

public interface Tooltip {
	void show();

	void hide(boolean remove);

	void setOffsetX(int x);

	void setOffsetY(int y);

	void offsetTo(int x, int y);

	boolean isAttached();

	boolean isShown();
}
