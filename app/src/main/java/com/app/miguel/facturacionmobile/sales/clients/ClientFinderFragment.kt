package com.app.miguel.facturacionmobile.sales.clients


import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.models.Client
import com.app.miguel.facturacionmobile.persistencia.models.EntidadSucursalModel
import com.app.miguel.facturacionmobile.sales.SalesActivity
import com.app.miguel.facturacionmobile.sales.clients.ClientFinderRecyclerAdapter.ClientFinderClickListener
import kotlinx.android.synthetic.main.fragment_client_finder.*

/**
 * A simple [Fragment] subclass.
 */
class ClientFinderFragment : Fragment() {

    var mainActivity: SalesActivity? = null
    var clientList: ArrayList<EntidadSucursalModel>? = null
    var searchList: ArrayList<EntidadSucursalModel>? = null
    var recyclerView: RecyclerView? = null
    var recyclerAdapter: ClientFinderRecyclerAdapter? = null
    var mListener: OnClientSelectedListener? = null
    var selectedClient: EntidadSucursalModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_client_finder, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as SalesActivity
        mListener = context as OnClientSelectedListener
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back_button.setOnClickListener {
            fragmentManager!!.popBackStack()
        }

        btn_continue.setOnClickListener {
            mListener!!.clientSelected(selectedClient!!)
            fragmentManager!!.popBackStackImmediate()
        }

        btn_new_client.setOnClickListener {
            val fragment = NewClientFragment()
            mainActivity!!.supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, 0, R.anim.slide_out_right)
                    .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                    .addToBackStack(fragment.javaClass.simpleName)
                    .commit()
        }

        clientList = DBUtils.getListEntidadSucursalesAll(context!!)
        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerAdapter = ClientFinderRecyclerAdapter(context, clientList, object : ClientFinderClickListener {
            override fun onClick(client: EntidadSucursalModel?) {
                selectedClient = client
                btn_continue.isEnabled = client != null
            }
        })
        recyclerView!!.adapter = recyclerAdapter

        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                searchProducts(s.toString())
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchProducts(s.toString())
            }

        })
    }

    fun searchProducts(terms: String?) {
        if (terms!!.trim().length > 3) {
            searchList = ArrayList()
            for (index in clientList!!.indices) {
                val item = clientList!!.get(index)
                if (item.nombrecomercial?.toLowerCase()!!.contains(terms.toLowerCase()) || item.ruc?.toLowerCase()!!.contains(terms.toLowerCase())) {
                    searchList!!.add(item)
                }
            }
            recyclerAdapter = ClientFinderRecyclerAdapter(context, searchList, object : ClientFinderClickListener {
                override fun onClick(client: EntidadSucursalModel?) {
                    selectedClient = client
                    btn_continue.isEnabled = client != null
                }
            })
            recyclerView!!.adapter = recyclerAdapter
            recyclerAdapter!!.notifyDataSetChanged()
        } else if (!terms.trim().isNullOrEmpty() && terms!!.trim().length < 3) {
            recyclerAdapter = ClientFinderRecyclerAdapter(context, clientList, object : ClientFinderClickListener {
                override fun onClick(client: EntidadSucursalModel?) {
                    selectedClient = client
                    btn_continue.isEnabled = client != null
                }
            })
            recyclerView!!.adapter = recyclerAdapter
            recyclerAdapter!!.notifyDataSetChanged()
        }
    }

    fun addClient(client: EntidadSucursalModel) {
        clientList!!.add(0, client)
        recyclerAdapter!!.notifyDataSetChanged()
        recyclerView!!.scrollToPosition(clientList!!.size)
    }

    interface OnClientSelectedListener {
        fun clientSelected(client: EntidadSucursalModel)
    }
}
