package com.app.miguel.facturacionmobile.print

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.app.miguel.facturacionmobile.BuildConfig
import com.app.miguel.facturacionmobile.NumeroALetras
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.models.SaleSummaryMobile
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class PrintTask(var context: Context, var saleSummary: SaleSummaryMobile) : AsyncTask<Void, Void, String>() {


    override fun doInBackground(vararg params: Void?): String {
        if (BuildConfig.USE_PRINTER) {
            Log.d("PrintTask", "STARTED")

            val saltoLinea = "\n"
            val saltoLineaDoble = "\n\n"
            val corteLinea = "-".repeat(30)

            var params = DBUtils.getSaleParameters(context)

            var formatterGS = NumberFormat.getNumberInstance(Locale.GERMAN) as DecimalFormat

            var empresa = Utils.formatLineCenter(params!!.razonSocial!!)
            var ruc = Utils.formatLineCenter("RUC: ${params!!.ruc!!}")
            var direccion = Utils.formatLineCenter("Direccion: ${params.direccion!!}")
            var ciudadDpto = Utils.formatLineCenter("${params.ciudad!!}, ${params.departamento!!}")
            var pais = Utils.formatLineCenter(params.pais!!)
            var telefono = Utils.formatLineCenter("Tel: ${params.telefono!!}")
            var timbrado = Utils.formatLineCenter("Timbrado: ${params.timbrado!!}")
            var tipoFactura = if (saleSummary.ventaCabeceraModel.condicion_venta == 1) "CONTADO" else "CREDITO"
            var facturaTitle = Utils.formatLineCenter("*** FACTURA $tipoFactura ***")
            var factura = facturaTitle + saltoLinea + Utils.formatLineCenter("Nro. " + saleSummary.ventaCabeceraModel.nro_documento!!)
            var validoDesde = Utils.formatLineCenter("VALIDO DESDE: ${params.validoDesde!!}")
            var validoHasta = Utils.formatLineCenter("VALIDO HASTA: ${params.validoHasta!!}")
            var ivaIncluido = Utils.formatLineCenter("*** IVA INCLUIDO ***")

            var entidad = DBUtils.getEntidadSucursal(context!!, saleSummary.ventaCabeceraModel.id_entidad_sucursal!!)

            var clienteRUC = "CI/RUC: " + entidad!!.ruc
            var cliente = "CLIENTE: " + entidad.razonSocial
            var clienteInfo = corteLinea + saltoLineaDoble + cliente + saltoLinea + clienteRUC + saltoLinea + corteLinea

            var caja = "CAJA: ${params.idCaja}"
            var fechaFormateada = formatDatePrint(saleSummary.ventaCabeceraModel.fecha_emision!!.trim())
            var splitFechahora = fechaFormateada!!.trim().split(" ")
            var fechaEmision = "FECHA EMISION: ${splitFechahora[0]}"
            var horaEmision = "HORA: ${splitFechahora[1]}"
            var infoCaja = saltoLinea + caja + saltoLinea + fechaEmision + saltoLinea + horaEmision + saltoLinea + corteLinea

            var header = saltoLineaDoble + empresa + saltoLinea + ruc + saltoLineaDoble + direccion + saltoLinea + ciudadDpto + saltoLinea + pais + saltoLineaDoble + telefono + saltoLineaDoble + timbrado +
                    saltoLinea + validoDesde + saltoLinea + validoHasta + saltoLineaDoble + factura + saltoLinea +
                    ivaIncluido + saltoLineaDoble + clienteInfo + saltoLineaDoble + infoCaja + saltoLineaDoble

            var detailsHeader1 = "Articulo"
            var cantidadTitle = Utils.formatLineLeftRight("", "Cant.", 5)
            var cantidadPrecioTitle = Utils.formatLineLeftRight(cantidadTitle, "Precio", 16)
            var cantidadPrecioTotalTitle = Utils.formatLineLeftRight(cantidadPrecioTitle, "Total", 30)

            var details = detailsHeader1 + saltoLinea + cantidadPrecioTotalTitle + saltoLinea + corteLinea
            for (item in saleSummary.ventaDetalles) {
                var nombreArticulo = saltoLineaDoble + Utils.fitTextInChars(DBUtils.getProductWithID(context!!, item.id_producto).nombre, 30)
                var cantidad = saltoLinea + Utils.formatLineLeftRight("", formatterGS.format(item.cantidad), 5)
                var cantidadPrecio = Utils.formatLineLeftRight(cantidad, formatterGS.format(item.precio_venta), 16)
                var cantidadPrecioTotal = Utils.formatLineLeftRight(cantidadPrecio, formatterGS.format(item.precio_venta * item.cantidad), 30)
                details = details + nombreArticulo + cantidadPrecioTotal
            }
            details += saltoLineaDoble + corteLinea
            var totalPagar = saltoLineaDoble + "TOTAL A PAGAR: " + formatterGS.format(saleSummary.ventaCabeceraModel.total_pagado) + " Gs."
            var totalLetras = saltoLineaDoble + NumeroALetras().convertir(saleSummary.ventaCabeceraModel.total_pagado.toString(), true) + saltoLinea + corteLinea


            var detalleExentos = saltoLineaDoble + Utils.formatLineLeftRight("EXENTOS", formatterGS.format(saleSummary.ventaCabeceraModel.bruto_exento), 30)
            var detalleGravados5 = saltoLinea + Utils.formatLineLeftRight("GRAVADOS 5%", formatterGS.format(saleSummary.ventaCabeceraModel.bruto_gravado_5), 30)
            var detalleGravados10 = saltoLinea + Utils.formatLineLeftRight("GRAVADOS 10%", formatterGS.format(saleSummary.ventaCabeceraModel.bruto_gravado_10), 30)
            var descripcionDetalle = saltoLinea + "DETALLES POR CONCEPTOS" + detalleExentos + detalleGravados5 + detalleGravados10 + saltoLinea + corteLinea


            var exenta = saltoLineaDoble + Utils.formatLineLeftRight("EXENTA", formatterGS.format(saleSummary.ventaCabeceraModel.neto_exento), 30)
            var iva5 = saltoLinea + Utils.formatLineLeftRight("IVA 5%", formatterGS.format(saleSummary.ventaCabeceraModel.neto_iva_5), 30)
            var iva10 = saltoLinea + Utils.formatLineLeftRight("IVA 10%", formatterGS.format(saleSummary.ventaCabeceraModel.neto_iva_10), 30)
            var descripcionIva = saltoLinea + "LIQUIDACION DEL IVA" + saltoLinea + exenta + iva5 + iva10 + saltoLinea + corteLinea

            var originalDuplicado = saltoLinea + "ORIGINAL: CLIENTE" + saltoLinea + "DUPLICADO: ARCHIVO TRIBUTARIO"

            var mensajeGracias = saltoLineaDoble + Utils.formatLineCenter("*** GRACIAS POR SU COMPRA ***")

            var facturaImprimir = header + details + totalPagar + totalLetras + descripcionDetalle + descripcionIva + originalDuplicado + mensajeGracias + saltoLineaDoble.repeat(3)

            var printJob1 = header
            var printJob2 = details
            var printJob3 = totalPagar + totalLetras + descripcionDetalle + descripcionIva + mensajeGracias + saltoLineaDoble.repeat(8)

            PrintBTUtils.startPrintJob(context, printJob1)
            Thread.sleep(5000)
            PrintBTUtils.startPrintJob(context, printJob2)
            Thread.sleep(5000)
            PrintBTUtils.startPrintJob(context, printJob3)
        }

        return ""
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        Log.d("PrintTask", "FINISHED")
    }


    fun formatDatePrint(date: String): String? {
        val originDateTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        var returnDate: String? = null

        try {
            var parsedDate = originDateTimeFormat.parse(date)
            returnDate = simpleDateFormat.format(parsedDate)
        } catch (e: ParseException){
            returnDate = date
        }

        return returnDate
    }
}