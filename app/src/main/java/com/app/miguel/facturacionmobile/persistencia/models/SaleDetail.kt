package com.app.miguel.facturacionmobile.persistencia.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SaleDetail (@Expose @SerializedName("id_producto") var id: Int,
                  @Expose @SerializedName("cantidad") var cantidad: Int,
                  @Expose @SerializedName("decripcion") var descripcion: Int?)