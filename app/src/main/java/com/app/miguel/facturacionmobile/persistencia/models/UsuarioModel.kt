package com.app.miguel.facturacionmobile.persistencia.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class UsuarioModel(@Expose @SerializedName("id") var id_usuario: Int?,
                   @Expose @SerializedName("nombreUsuario") var nombre_usuario: String?,
                   @Expose @SerializedName("apellidoNombres") var apellido_nombres: String?,
                   @Expose @SerializedName("contrasenia") var contrasenia: String?,
                   @Expose @SerializedName("fechaExpira") var fecha_expira: String?,
                   @Expose @SerializedName("entidadSucursal") var idEntidadSucursal: Int?,
                   @Expose @SerializedName("estado") var estado: String?) : Parcelable
