package com.app.miguel.facturacionmobile

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.util.TypedValue
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.app.miguel.facturacionmobile.menu.MenuItem
import com.app.miguel.facturacionmobile.menu.MenuRecyclerAdapter
import com.app.miguel.facturacionmobile.persistencia.SalesManager
import com.app.miguel.facturacionmobile.persistencia.models.UsuarioModel
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*

class HomeActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var menuArrayList: ArrayList<MenuItem>? = null
    private var menuRecyclerAdapter: MenuRecyclerAdapter? = null
    private var usuarioActivo: UsuarioModel? = null
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

        initViews()
    }

    private fun initViews() {
        recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        recyclerView!!.addItemDecoration(GridSpacingItemDecoration(2, dpToPx(8), true))
        menuArrayList = ArrayList()
        menuRecyclerAdapter = MenuRecyclerAdapter(this, menuArrayList)
        recyclerView!!.adapter = menuRecyclerAdapter

        setupMenu()
        tv_sync.setOnClickListener {
            var intent = Intent(this, SyncActivity::class.java)
            startActivity(intent)
        }

        val bundle = intent.extras
        if(bundle != null) {
            usuarioActivo = bundle.getParcelable("usuario") as UsuarioModel
            SalesManager.loggedUser = usuarioActivo
            tv_logged_user.text = "Usuario: ${usuarioActivo!!.nombre_usuario}"
        }
    }


    private fun setupMenu() {
        menuArrayList!!.clear()

        menuArrayList!!.add(MenuItem("Ventas", getDrawable(R.drawable.ic_venta)))

        menuArrayList!!.add(MenuItem("Stock", getDrawable(R.drawable.ic_warehouse)))

        menuArrayList!!.add(MenuItem("Resumen", getDrawable(R.drawable.ic_analysis)))

        menuArrayList!!.add(MenuItem("Salir", getDrawable(R.drawable.ic_exit)))

        menuRecyclerAdapter!!.notifyDataSetChanged()
    }

    private fun dpToPx(dp: Int): Int {
        val r = resources
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics))
    }

    inner class GridSpacingItemDecoration(private val spanCount: Int, private val spacing: Int, private val includeEdge: Boolean) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce){
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        val backMessage = "Presiona nuevamente hacia atras si quiere salir de la aplicación"

        Utils.showSnackbar(this, rl_main, backMessage, Utils.TYPE_INFO)
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
}
