package com.app.miguel.facturacionmobile.tooltip;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DimenRes;

import com.app.miguel.facturacionmobile.R;

import org.jetbrains.annotations.Nullable;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

public class TooltipManager {

    private static final String TAG = "TooltipManager";

    private static volatile TooltipManager instance;

    private final List<OnTooltipAttachedStateChange> onTooltipAttachedStateChanges = new
            ArrayList<>();

    private final WeakHashMap<Integer, WeakReference<TooltipView>> tooltips = new WeakHashMap<>();

    private final Object lock = new Object();

    private TooltipView.OnCloseListener onCloseListener = new TooltipView.OnCloseListener() {
        @Override
        public void onClose(final TooltipView layout) {

            hide(layout.getTooltipId());
        }
    };

    private TooltipView.OnToolTipListener onToolTipListener = new MyOnToolTipListener();

    private TooltipManager() {
    }

    public static synchronized TooltipManager getInstance() {
        if (instance == null) {
            instance = new TooltipManager();
        }

        return instance;
    }

    @Nullable
    static Activity getActivity(@Nullable Context cont) {
        if (cont == null) {
            return null;
        } else if (cont instanceof Activity) {
            return (Activity) cont;
        } else if (cont instanceof ContextWrapper) {
            return getActivity(((ContextWrapper) cont).getBaseContext());
        }
        return null;
    }

    private void fireOnTooltipDetached(int id) {
        if (onTooltipAttachedStateChanges.isEmpty()) {
            for (OnTooltipAttachedStateChange listener : onTooltipAttachedStateChanges) {
                listener.onTooltipDetached(id);
            }
        }
    }

    private void fireOnTooltipAttached(int id, Context context, View layout, String description) {
        if (onTooltipAttachedStateChanges.isEmpty()) {
            for (OnTooltipAttachedStateChange listener : onTooltipAttachedStateChanges) {
                listener.onTooltipAttached(id);
            }
        }
//        if (AccessibilityUtils.accessibilityIsActive(context)) {
//            AccessibilityUtils.announceForAccessibilityCompat(context, layout, description);
//        }
    }

    public Builder create(final Context context, int id) {
        return new Builder(this, context, id);
    }

    private boolean show(Builder builder, boolean immediate) {

        synchronized (lock) {
            if (tooltips.containsKey(builder.id)) {
                Log.w(TAG, "A Tooltip with the same id was walready specified");
                return false;
            }

            TooltipView layout = new TooltipView(builder.context, builder);
            layout.setOnCloseListener(onCloseListener);
            layout.setOnToolTipListener(onToolTipListener);

            tooltips.put(builder.id, new WeakReference<>(layout));

            final Activity act = getActivity(builder.context);

            if (null == act || act.getWindow() == null || act.getWindow().getDecorView() == null) {
                return false;
            }

            String description = builder.descriptionAcessibility == null ? builder.description :
                    builder.descriptionAcessibility;

            showInternal(act.getWindow().getDecorView(), layout, immediate, builder.context,
                    description);
        }
        return true;
    }

    public void hide(int id) {
        final WeakReference<TooltipView> layout;
        synchronized (lock) {
            layout = tooltips.remove(id);
        }
        if (null != layout) {
            TooltipView tooltipView = layout.get();
            if (tooltipView != null) {
                tooltipView.setOnCloseListener(null);
                tooltipView.hide(true);
            }
        }
    }

    @Nullable
    public TooltipView get(int id) {
        synchronized (lock) {
            WeakReference<TooltipView> weakReference = tooltips.get(id);

            if (weakReference != null) {
                return weakReference.get();
            }
        }
        return null;
    }

    public void update(int id) {
        final TooltipView layout;
        synchronized (lock) {
            layout = get(id);
        }
        if (null != layout) {
            layout.layout(layout.getLeft(), layout.getTop(), layout.getRight(), layout.getBottom());
            layout.requestLayout();
        }
    }

    public synchronized void remove(int id) {
        final WeakReference<TooltipView> layout;
        synchronized (lock) {
            layout = tooltips.remove(id);
        }
        if (null != layout) {
            TooltipView tooltipView = layout.get();
            tooltipView.setOnCloseListener(null);
            tooltipView.setOnToolTipListener(null);
            tooltipView.removeFromParent();
            fireOnTooltipDetached(id);
        }
    }

    private void showInternal(View rootView, TooltipView layout, boolean immediate, Context
            context, String description) {
        if (null != rootView && rootView instanceof ViewGroup) {
            if (layout.getParent() == null) {
                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams
                        .MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                ((ViewGroup) rootView).addView(layout, params);
            }

            if (immediate) {
                layout.show();
            }

            fireOnTooltipAttached(layout.getTooltipId(), context, layout, description);
        }
    }

    public enum ClosePolicy {
        /**
         * tooltip will hide when touching it, or after the specified delay.
         * If delay is '0' the tooltip will never hide until clicked
         */
        TouchInside, /**
         * tooltip will hide when touching it, or after the specified delay.
         * If delay is '0' the tooltip will never hide until clicked.
         * In exclusive mode all touches will be consumed by the tooltip itself
         */
        TouchInsideExclusive, /**
         * tooltip will hide when user touches the screen, or after the specified delay.
         * If delay is '0' the tooltip will never hide until clicked
         */
        TouchOutside, /**
         * tooltip will hide when user touches the screen, or after the specified delay.
         * If delay is '0' the tooltip will never hide until clicked.
         * Touch will be consumed in any case.
         */
        TouchOutsideExclusive, /**
         * tooltip is hidden only after the specified delay
         */
        None;

        /**
         * Returns true if this is different of None.
         *
         * @return true if this is different of None, false otherwise.
         */
        public boolean isTouch() {
            return this != None;
        }

        /**
         * Returns true if this is TouchInsideExclusive or TouchOutsideExclusive
         *
         * @return true if this is TouchInsideExclusive or TouchOutsideExclusive, false otherwise
         */
        public boolean isExclusive() {
            return this == TouchInsideExclusive || this == TouchOutsideExclusive;
        }

        /**
         * Returns true if this is TouchInside or TouchOutside
         *
         * @return true if this is TouchInside or TouchOutside, false otherwise
         */
        public boolean isInclusive() {
            return this == TouchInside || this == TouchOutside;
        }

        /**
         * Returns true if this is TouchInside or TouchInsideExclusive
         *
         * @return true if this is TouchInside or TouchInsideExclusive, false otherwise
         */
        public boolean isInside() {
            return this == TouchInside || this == TouchInsideExclusive;
        }

        /**
         * Returns true if this is TouchOutside or TouchOutsideExclusive
         *
         * @return true if this is TouchOutside or TouchOutsideExclusive, false otherwise
         */
        public boolean isOutside() {
            return this == TouchOutside || this == TouchOutsideExclusive;
        }

    }

    /**
     * Gravity
     */
    public enum Gravity {
        LEFT, RIGHT, TOP, BOTTOM, CENTER, TOP_CENTER
    }

    /**
     *
     */
    interface OnTooltipAttachedStateChange {

        /**
         * onTooltipAttached
         * @param id
         */
        void onTooltipAttached(int id);

        /**
         * onTooltipDetached
         * @param id
         */
        void onTooltipDetached(int id);
    }

    /**
     * OnTooltipClosingCallback
     */
    interface OnTooltipClosingCallback {

        /**
         * tooltip is being closed
         *
         * @param id            int
         * @param fromUser      true if the close operation started from a user click
         * @param containsTouch true if the original touch came from inside the tooltip
         */
        void onClosing(int id, boolean fromUser, boolean containsTouch);
    }

    public static final class Builder {

        final Context context;

        int id;

        String title;

        String description;

        String descriptionAcessibility;

        View view;

        Gravity gravity;

        int actionbarSize = 0;

        int textResId = R.layout.tooltip_textview;

        int customColor = 0;

        ClosePolicy closePolicy;

        long showDuration;

        Point point;

        WeakReference<TooltipManager> manager;

        long showDelay = 0;

        boolean hideArrow;

        int defStyleRes = R.style.ToolTipLayoutDefaultStyle;

        int defStyleAttr = R.attr.ttlm_defaultStyle;

        int maxWidth = -1;

        long activateDelay = 0;

        boolean isCustomView;

        boolean restrictToScreenEdges = true;

        long fadeDuration = 200;

        OnTooltipClosingCallback closeCallback;

        int arrowIndicatorPaddingLeftDimenRes;

        int arrowIndicatorPosXPercentage;

        String acessibilityMessage = null;

        boolean marginBasedOnArrowIndicator = true;

        int marginBottom = 0;

        int marginTop = 0;

        int marginRight = 0;

        int marginLeft = 0;

        Builder(final TooltipManager manager, final Context context, int id) {
            this.manager = new WeakReference<>(manager);
            this.id = id;
            this.context = context;
        }

        public Builder withCustomView(int resId, boolean replaceBackground) {
            this.textResId = resId;
            this.isCustomView = replaceBackground;

            return this;
        }

        /**
         * Use a custom View for the tooltip. Note that the custom view
         * must include a TextView which id is `@android:id/text1`.<br />
         * Moreover, when using a custom view, the anchor arrow will not be shown
         *
         * @param resId             the custom layout view.
         * @param replaceBackground if true the custom view's background won't be replaced
         * @param customColor       if you want to change the background of the view
         * @return the builder for chaining.
         */
        public Builder withCustomView(int resId, boolean replaceBackground, int customColor) {
            this.customColor = customColor;
            return withCustomView(resId, replaceBackground);
        }

        /**
         * fitToScreen
         * @param value
         * @return
         */
        public Builder fitToScreen(boolean value) {
            restrictToScreenEdges = value;
            return this;
        }

        /**
         * withMarginBottom
         * @param marginBottom
         * @return
         */
        public Builder withMarginBottom(int marginBottom) {
            this.marginBottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    marginBottom, context.getResources().getDisplayMetrics());
            return this;
        }

        /**
         * withMarginTop
         * @param marginTop
         * @return
         */
        public Builder withMarginTop(int marginTop) {
            this.marginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    marginTop, context.getResources().getDisplayMetrics());
            return this;
        }

        /**
         * withMarginRight
         * @param marginRight
         * @return
         */
        public Builder withMarginRight(int marginRight) {
            this.marginRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    marginRight, context.getResources().getDisplayMetrics());
            return this;
        }

        /**
         * withMarginLeft
         * @param marginLeft
         * @return
         */
        public Builder withMarginLeft(int marginLeft) {
            this.marginLeft = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    marginLeft, context.getResources().getDisplayMetrics());
            return this;
        }

        /**
         * withMarginBasedOnArrowIndicator
         * @param marginBasedOnArrowIndicator
         * @return
         */
        public Builder withMarginBasedOnArrowIndicator(boolean marginBasedOnArrowIndicator) {
            this.marginBasedOnArrowIndicator = marginBasedOnArrowIndicator;
            return this;
        }

        /**
         * fadeDuration
         * @param ms
         * @return
         */
        public Builder fadeDuration(long ms) {
            fadeDuration = ms;
            return this;
        }

        /**
         * withCallback
         * @param callback
         * @return
         */
        public Builder withCallback(OnTooltipClosingCallback callback) {
            this.closeCallback = callback;
            return this;
        }

        /**
         * title
         * @param res
         * @param resid
         * @return
         */
        public Builder title(Resources res, int resid) {
            return title(res.getString(resid));
        }

        /**
         * title
         * @param resid
         * @return
         */
        public Builder title(int resid) {
            if (null != view) {
                return title(view.getResources().getString(resid));
            }
            return this;
        }

        /**
         * title
         * @param title
         * @return
         */
        public Builder title(String title) {
            this.title = title;
            return this;
        }

        /**
         * description
         * @param description
         * @return
         */
        public Builder description(String description) {
            this.description = description;
            return this;
        }

        /**
         * descriptionAcessibility
         * @param descriptionAcessibility
         * @return
         */
        public Builder descriptionAcessibility(String descriptionAcessibility) {
            this.descriptionAcessibility = descriptionAcessibility;
            return this;
        }

        /**
         * maxWidth
         * @param maxWidth
         * @return
         */
        public Builder maxWidth(int maxWidth) {
            this.maxWidth = maxWidth;
            return this;
        }

        /**
         *
         * anchor
         * @param view
         * @param gravity
         * @return
         */
        public Builder anchor(View view, Gravity gravity) {
            this.point = null;
            this.view = view;
            this.gravity = gravity;
            return this;
        }

        /**
         * actionBarSize
         * @param actionBarSize
         * @return
         */
        public Builder actionBarSize(final int actionBarSize) {
            this.actionbarSize = actionBarSize;
            return this;
        }

        public Builder actionBarSize(Resources resources, int resId) {
            return actionBarSize(resources.getDimensionPixelSize(resId));
        }

        public Builder setArrowIndicatorPaddingLeft(@DimenRes final int id) {
            this.arrowIndicatorPaddingLeftDimenRes = id;
            return this;
        }

        /**
         * setArrowIndicatorPosXPercentage
         * @param xPercentage
         * @return
         */
        public Builder setArrowIndicatorPosXPercentage(final int xPercentage) {
            this.arrowIndicatorPosXPercentage = xPercentage;
            return this;
        }

        /**
         * closePolicy
         * @param policy
         * @param milliseconds
         * @return
         */
        public Builder closePolicy(ClosePolicy policy, long milliseconds) {
            this.closePolicy = policy;
            this.showDuration = milliseconds;
            return this;
        }

        /**
         * show
         * @return
         */
        public boolean show() {
            // verificação
            if (null == closePolicy) {
                throw new IllegalStateException("ClosePolicy cannot be null");
            }
            if (null == point && null == view) {
                throw new IllegalStateException("Target point or target view must be specified");
            }
            if (gravity == Gravity.CENTER) {
                hideArrow = true;
            }

            TooltipManager tmanager = this.manager.get();
            return null != tmanager && tmanager.show(this, true);
        }

        /**
         * withAcessibilityText
         * @param acessibilityMessage
         * @return
         */
        public Builder withAcessibilityText(String acessibilityMessage) {
            this.acessibilityMessage = acessibilityMessage;
            return this;
        }
    }

    private class MyOnToolTipListener implements TooltipView.OnToolTipListener {

        @Override
        public void onHideCompleted(final TooltipView layout) {

            int id = layout.getTooltipId();
            layout.removeFromParent();
            fireOnTooltipDetached(id);
        }

        @Override
        public void onShowCompleted(TooltipView layout) {
            //Unused
        }

        @Override
        public void onShowFailed(final TooltipView layout) {
            remove(layout.getTooltipId());
        }
    }
}