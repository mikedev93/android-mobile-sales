package com.app.miguel.facturacionmobile.cancelInvoice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.miguel.facturacionmobile.R;
import com.app.miguel.facturacionmobile.Utils;
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils;
import com.app.miguel.facturacionmobile.persistencia.database.SaleData;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class CancelInvoiceRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<SaleData> saleDataList;
    private CancelInvoiceActivity mainActivity;

    public CancelInvoiceRecyclerAdapter(Context context, ArrayList<SaleData> saleDataList) {
        this.context = context;
        this.saleDataList = saleDataList;
        this.mainActivity = (CancelInvoiceActivity) context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.view_cancel_invoice_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final SaleData saleData = saleDataList.get(position);
        final ViewHolder holder = (ViewHolder) viewHolder;
        Boolean syncronizado = DBUtils.Companion.getInvoiceSyncd(context, saleData.getId_venta());
        String syncText;
        if(syncronizado) syncText = "SI"; else syncText = "NO";
        String anulado = DBUtils.Companion.getInvoiceState(context, saleData.getId_venta());
        String anuladoText;
        if("X".equals(anulado)) anuladoText = "SI"; else anuladoText = "NO";

        DecimalFormat formatGS = (DecimalFormat) NumberFormat.getInstance(Locale.GERMAN);
        String condicionVenta;
        if (saleData.getCondicionVenta() == 1) condicionVenta = "CONTADO"; else condicionVenta = "CREDITO";

        holder.vTvFactura.setText(Utils.Companion.getNroFacturaFormatted(saleData.getFactura()));
        holder.vTvTipo.setText(condicionVenta);
        holder.vTvCliente.setText(saleData.getClient().getNombrecomercial());
        holder.vTvRuc.setText(saleData.getClient().getRuc());
        holder.vTvAnulado.setText(anuladoText);
        holder.vTvSync.setText(syncText);
        holder.vTvImporte.setText(formatGS.format(DBUtils.Companion.getSaleFull(context, saleData.getId_venta()).getVentaCabeceraModel().getTotal_pagado()));

        holder.vLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CancelInvoicePopupFragment fragment = new CancelInvoicePopupFragment().Companion.newInstance(saleData, position);
                mainActivity.showCurtain(true);
                mainActivity.getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.slide_down, 0, 0, R.anim.slide_down_out)
                        .add(R.id.popup_container, fragment, fragment.getClass().getSimpleName())
                        .addToBackStack(fragment.getClass().getSimpleName())
                        .commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return saleDataList.size();
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayout vLinearLayout;
        private final TextView vTvFactura;
        private final TextView vTvTipo;
        private final TextView vTvCliente;
        private final TextView vTvRuc;
        private final TextView vTvSync;
        private final TextView vTvAnulado;
        private final TextView vTvImporte;
        private final View vVSeparador;

        public ViewHolder(View itemView) {
            super(itemView);
            vLinearLayout = itemView.findViewById(R.id.ll_content);
            vTvFactura = itemView.findViewById(R.id.tv_factura);
            vTvTipo = itemView.findViewById(R.id.tv_condicion_venta);
            vTvCliente = itemView.findViewById(R.id.tv_cliente);
            vTvRuc = itemView.findViewById(R.id.tv_ruc);
            vTvSync = itemView.findViewById(R.id.tv_sync);
            vTvAnulado = itemView.findViewById(R.id.tv_anulado);
            vTvImporte = itemView.findViewById(R.id.tv_importe);
            vVSeparador = itemView.findViewById(R.id.separador);
        }
    }
}
