package com.app.miguel.facturacionmobile.persistencia.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EntidadModel(@Expose @SerializedName("identidad") var identidad: Int?,
        @Expose @SerializedName("razonsocial") var razonsocial: String?,
        @Expose @SerializedName("nombrecomercial") var nombrecomercial: String?,
        @Expose @SerializedName("ruc") var ruc: String?,
        @Expose @SerializedName("dv") var dv: Int?,
        @Expose @SerializedName("idpedido_remision") var idpedido_remision: Int?,
        @Expose @SerializedName("altafechahora") var altafechahora: String?,
        @Expose @SerializedName("altausuario") var altausuario: Int?,
        @Expose @SerializedName("modificadofechahora") var modificadofechahora: String?,
        @Expose @SerializedName("modificadousuario") var modificadousuario: Int?)

class ListEntidad(@Expose @SerializedName("entidades") var entidades: ArrayList<EntidadModel>)