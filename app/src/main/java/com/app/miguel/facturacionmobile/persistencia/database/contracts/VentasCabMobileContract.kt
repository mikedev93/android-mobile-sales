package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class VentasCabMobileContract {

    class VentasCabMobileEntry: BaseColumns {
        companion object {
            const val TABLE_NAME: String = "mobile_ventas_cab"

            const val _ID = BaseColumns._ID

            const val ID_VENTA: String = "id_venta"
            const val ID_PEDIDO_REMISION: String = "id_pedido_remision"
            const val ID_ENTIDAD: String = "id_entidad"
            const val ID_ENTIDAD_SUCURSAL: String = "id_entidad_sucursal"
            const val CONDICION_VENTA: String = "condicion_venta"
            const val FECHA_EMISION: String = "fecha_emision"
            const val NRO_DOCUMENTO: String = "nro_documento"
            const val FECHA_VENCIMIENTO: String = "fecha_vencimiento"
            const val FECHA_ALTA: String = "fecha_alta"
            const val USUARIO_ALTA: String = "usuario_alta"
            const val ESTADO: String = "estado"
            const val FECHA_ANULADO: String = "fecha_anulado"
            const val USUARIO_ANULADO: String = "usuario_anulado"
            const val BRUTO_EXENTO: String = "bruto_exento"
            const val BRUTO_GRAVADO_5: String = "bruto_gravado_5"
            const val BRUTO_IVA_5: String = "bruto_iva_5"
            const val BRUTO_GRAVADO_10: String = "bruto_gravado_10"
            const val BRUTO_IVA_10: String = "bruto_iva_10"
            const val NETO_EXENTO: String = "neto_exento"
            const val NETO_GRAVADO_5: String = "neto_gravado_5"
            const val NETO_IVA_5: String = "neto_iva_5"
            const val NETO_GRAVADO_10: String = "neto_gravado_10"
            const val NETO_IVA_10: String = "neto_iva_10"
            const val TOTAL_PAGADO: String = "total_pagado"
            const val TIPO_PAGO: String = "tipo_pago" 	//Efectivo, Cheque, Otro
            const val SYNC_DB_ID: String = "sync_db_id"
            const val SYNC_DB_FECHA: String = "sync_db_fecha"
            const val TIPO_VENTA: String = "tipo_venta"
            const val REIMPRESION_FECHA_HORA: String = "reimpresion_fecha_hora"
            const val REIMPRESION_USUARIO: String = "reimpresion_usuario"
            const val REIMPRESION_CANTIDAD: String = "reimpresion_cantidad"
            const val VENDEDOR: String = "vendedor"
            const val COMENTARIO: String = "comentario"
        }
    }
}