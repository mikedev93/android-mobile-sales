package com.app.miguel.facturacionmobile.persistencia.servicios.tasks

import android.content.Context
import android.os.AsyncTask
import com.app.miguel.facturacionmobile.SyncActivity
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.database.contracts.GeoCiudadesContract
import com.app.miguel.facturacionmobile.persistencia.models.GeoCiudadModel
import com.app.miguel.facturacionmobile.persistencia.servicios.SyncAPI
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SyncGeoCiudadesTask(var context: Context, var mainActivity: SyncActivity): AsyncTask<Void, Void, String>(){

    private var syncroApi: SyncAPI? = null
    private var mRestAdapter: Retrofit? = null

    override fun doInBackground(vararg params: Void?): String {
        val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(40, TimeUnit.MINUTES)
                .writeTimeout(40, TimeUnit.MINUTES)
                .build()

        mRestAdapter = Retrofit.Builder()
                .baseUrl(Utils.getBaseURL(context)!!)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        syncroApi = mRestAdapter!!.create(SyncAPI::class.java)
        var ciudadesResponse = syncroApi!!.getCiudades()
        ciudadesResponse.enqueue(object : retrofit2.Callback<List<GeoCiudadModel>> {
            override fun onFailure(call: retrofit2.Call<List<GeoCiudadModel>>?, t: Throwable?) {
                mainActivity.showLoading(false)
                mainActivity.showSnackbarError(t!!.message!!)
            }

            override fun onResponse(call: retrofit2.Call<List<GeoCiudadModel>>?, response: Response<List<GeoCiudadModel>>?) {
                var servResponse = response!!.body()
                if (servResponse != null) {
                    DBUtils.vaciarTabla(context, GeoCiudadesContract.GeoCiudadesEntry.TABLE_NAME)
                }
                DBUtils.saveGeoCiudades(context, servResponse)
                Thread.sleep(1000)
                mainActivity.syncEntidades()
            }
        })

        return ""
    }

}