package com.app.miguel.facturacionmobile.persistencia.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GeoCiudadModel (@Expose @SerializedName("id") var id: Int?,
                      @Expose @SerializedName("nomCiuDep") var nombreCiudadDepto: String?,
                      @Expose @SerializedName("ciudadId") var ciudadId: Int?,
                      @Expose @SerializedName("ciudadDescripcion") var ciudadDescripcion: String?,
                      @Expose @SerializedName("departamentoId") var departamentoId: Int?,
                      @Expose @SerializedName("departamentoDescripcion") var departamentoDescripcion: String?,
                      @Expose @SerializedName("altaFechaHora") var altaFechaHora: String?,
                      @Expose @SerializedName("altaUsuario") var altaUsuario: Int?,
                      @Expose var checked: Boolean = false)