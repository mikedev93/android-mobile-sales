package com.app.miguel.facturacionmobile.sales


import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.SalesManager
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.models.*
import com.app.miguel.facturacionmobile.print.PrintBTUtils
import com.app.miguel.facturacionmobile.sales.clients.ClientFinderFragment
import com.app.miguel.facturacionmobile.sales.products.ProductFinderFragment
import kotlinx.android.synthetic.main.fragment_sales.*
import kotlinx.android.synthetic.main.view_toolbar_sales.*
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt


/**
 * A simple [Fragment] subclass.
 */
class SalesFragment : Fragment(), NumberPicker.OnValueChangeListener {

    var mainActivity: SalesActivity? = null
    var recyclerView: RecyclerView? = null
    var recyclerAdapter: SalesRecyclerAdapter? = null
    var selectedProductsList: ArrayList<ProductModel>? = null
    var selectedClient: EntidadSucursalModel? = null
    var parametroVenta: ParametroVentaModel? = null
    private val CERO: String = "0"
    private val BARRA: String = "-"
    val c = Calendar.getInstance()
    //Variables para obtener la fecha
    internal val mes = c.get(Calendar.MONTH)
    internal val dia = c.get(Calendar.DAY_OF_MONTH)
    internal val anio = c.get(Calendar.YEAR)

    companion object {
        val client_key = "key_client"

        fun newInstance(idCliente: Int): SalesFragment{
            var fragment = SalesFragment()
            var bundle = Bundle()
            bundle.putInt(client_key, idCliente)
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null){
            selectedClient = DBUtils.getEntidadSucursal(context!!, arguments!!.getInt(client_key))
            selectedProductsList = SalesManager.selectedProductsMemory
        } else {
            selectedProductsList = ArrayList()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sales, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as SalesActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()

        et_nro_factura.text = Editable.Factory().newEditable(DBUtils.getNextNroFactura(context!!))
        et_nro_factura.isEnabled = false
        parametroVenta = DBUtils.getSaleParameters(context!!)

        fab_add_product.setOnClickListener {
            var listProductModel: ListProductModel = ListProductModel(selectedProductsList!!)
            val fragment = ProductFinderFragment.newInstance(listProductModel)
            mainActivity!!.supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, 0, R.anim.slide_out_right)
                    .add(R.id.container, fragment, fragment.javaClass.simpleName)
                    .addToBackStack(fragment.javaClass.simpleName)
                    .commit()
        }

        btn_buscar_cliente.setOnClickListener {
            SalesManager.selectedProductsMemory = selectedProductsList
            val fragment = ClientFinderFragment()
            mainActivity!!.supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, 0, R.anim.slide_out_right)
                    .add(R.id.container, fragment, fragment.javaClass.simpleName)
                    .addToBackStack(fragment.javaClass.simpleName)
                    .commit()
        }

        configureRecyclerView()

        if (selectedClient != null){
            updateClient(selectedClient!!)
        }
    }

    private fun configureRecyclerView() {
//        selectedProductsList = ArrayList()
        recyclerView = view!!.findViewById(R.id.recyclerView_ventas)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerAdapter = SalesRecyclerAdapter(context, selectedProductsList)
        recyclerView!!.adapter = recyclerAdapter
        updateTotal(getTotalSum(selectedProductsList))
    }

    private fun setupToolbar() {
        tv_title.text = "Ventas"
        btn_back.setOnClickListener {
            mainActivity!!.finish()
        }

        btn_next.setOnClickListener {
            if (selectedClient != null && recyclerAdapter!!.itemCount > 0) {

                var check = checkCantidad(selectedProductsList!!)
                if (check == null) {
                    if (!PrintBTUtils.isBluetoothEnabled()){
                        Utils.showSnackbar(context!!, rl_main, PrintBTUtils.enableBluetoothMessage, "error")
                    } else if (!PrintBTUtils.isDevicePaired()){
                        Utils.showSnackbar(context!!, rl_main, PrintBTUtils.deviceNotPairedMessage, "error")
                    } else {

                        var saleSummaryMobile = collectSaleData()

                        val fragment = SalesResumeFragment.newInstance(saleSummaryMobile)
                        mainActivity!!.supportFragmentManager
                                .beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, 0, R.anim.slide_out_right)
                                .add(R.id.container, fragment, fragment.javaClass.simpleName)
                                .addToBackStack(fragment.javaClass.simpleName)
                                .commit()
                    }
                } else {
                    Utils.showSnackbar(context!!, rl_main, check, "warning")
                }
            } else {
                Utils.showSnackbar(context!!, rl_main, "Selecciona un cliente y uno o más productos para realizar la venta!", "warning")
            }
        }

        rb_tipo_credito.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                obtenerDiasVto()
                tv_title_vencimiento.visibility = View.VISIBLE
                tv_vencimiento.visibility = View.VISIBLE
            } else {
                tv_title_vencimiento.visibility = View.GONE
                tv_vencimiento.visibility = View.GONE
                tv_vencimiento.text = ""
            }
        }

        tv_vencimiento.setOnClickListener {
            obtenerDiasVto()
        }

        tv_title_vencimiento.setOnClickListener {
            obtenerDiasVto()
        }

    }

    fun addProduct(product: ProductModel) {
        this.selectedProductsList!!.add(product)
        this.recyclerAdapter!!.notifyDataSetChanged()
        updateTotal(getTotalSum(this.selectedProductsList))
    }

    fun removeProduct(product: ProductModel) {
        this.selectedProductsList!!.remove(product)
        this.recyclerAdapter!!.notifyDataSetChanged()
        updateTotal(getTotalSum(this.selectedProductsList))
    }

    fun updateProduct(product: ProductModel, position: Int) {
        this.selectedProductsList!![position] = product
        this.recyclerAdapter!!.notifyDataSetChanged()
        updateTotal(getTotalSum(this.selectedProductsList))

    }

    fun getTotalSum(productList: ArrayList<ProductModel>?): Double {
        var sum = 0.0

        for (product in productList!!) {
            sum += (product.cantidad * product.precio_venta)
        }
        return sum
    }

    fun updateTotal(total: Double) {
        var formatter = NumberFormat.getNumberInstance(Locale.GERMAN) as DecimalFormat
        tv_sum_total.text = formatter.format(total)
    }

    fun updateClient(client: EntidadSucursalModel) {
        selectedClient = client
        tv_nombre_cliente.text = client.nombrecomercial
        nro_ruc.text = client.ruc
        tv_direccion_set.text = client.direccion
    }

    private fun obtenerFecha(textView: TextView) {
        val recogerFecha = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
            val mesActual = month + 1
            //Formateo el día obtenido: antepone el 0 si son menores de 10
            val diaFormateado = if (dayOfMonth < 10) CERO + dayOfMonth.toString() else dayOfMonth.toString()
            //Formateo el mes obtenido: antepone el 0 si son menores de 10
            val mesFormateado = if (mesActual < 10) CERO + mesActual.toString() else mesActual.toString()
            //Muestro la fecha con el formato deseado
//            textView.setText(year.toString() + BARRA + mesFormateado + BARRA + diaFormateado)
            textView.setText(diaFormateado + BARRA + mesFormateado + BARRA + year.toString())
        },
                //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
                /**
                 * También puede cargar los valores que usted desee
                 */
                anio, mes, dia)
        //Muestro el widget
        recogerFecha.show()

    }

    override fun onValueChange(picker: NumberPicker?, oldVal: Int, newVal: Int) {
        tv_vencimiento.text = "$newVal"
    }

    private fun obtenerDiasVto() {
        val newFragment = NumberPickerDialog()
        newFragment.setValueChangeListener(this)
        newFragment.show(fragmentManager, NumberPickerDialog::class.java.simpleName)
    }

    fun collectSaleData(): SaleSummaryMobile {
        var totalBrutoExento = obtenerTotalBrutoExento(selectedProductsList!!)
        var totalBruto: Long = obtenerTotalBruto(selectedProductsList!!)
        var totalBrutoGravado5: Long = obtenerTotalProductosIVA_5(selectedProductsList!!)
        var totalBrutoGravado10: Long = obtenerTotalProductosIVA_10(selectedProductsList!!)
        var totalBrutoIVA5: Long = obtenerTotalIVA_5(selectedProductsList!!)
        var totalBrutoIVA10: Long = obtenerTotalIVA_10(selectedProductsList!!)
        var totalNetoExento: Long = obtenerTotalBrutoExento(selectedProductsList!!)
        var totalNetoGravado5: Long = obtenerTotalProductosIVA_5(selectedProductsList!!)
        var totalNetoIVA5: Long = obtenerTotalIVA_5(selectedProductsList!!)
        var totalNetoGravado10: Long = obtenerTotalProductosIVA_10(selectedProductsList!!)
        var totalNetoIVA10: Long = obtenerTotalIVA_10(selectedProductsList!!)
        var totalNeto: Long = obtenerTotalNeto(selectedProductsList!!)
        var vencimiento = (if (!tv_vencimiento.text.isNullOrEmpty()) tv_vencimiento.text else "").toString()
        var condicionVenta = if (rb_tipo_contado.isChecked) 1 else 2
        var nroFactura = et_nro_factura.text.toString()

        var ventaCabeceraModel = VentaCabeceraModel(null, parametroVenta!!.idpedido_remision, selectedClient!!.entidad, selectedClient!!.identidadsucursal,
                condicionVenta, null, nroFactura, vencimiento, null,
                SalesManager.loggedUser!!.id_usuario, null, null, totalBrutoExento, totalBrutoGravado5,
                totalBrutoIVA5, totalBrutoGravado10, totalBrutoIVA10, totalNetoExento, totalNetoGravado5,
                totalNetoIVA5, totalNetoGravado10, totalNetoIVA10, totalNeto, null,
                null)

        var ventasDet: ArrayList<VentaDetalleModel> = ArrayList()
        for (product in selectedProductsList!!) {

            var iva5 = 0.0
            var gravado5 = 0.0
            var iva10 = 0.0
            var gravado10 = 0.0
            var exenta = 0.0
//            var precioCaja: Double = (if (product.precioCaja!! > 0) product.precioCaja else product.precioUnidad!! * product.composicionVenta)!!
            var precioCaja: Double = product.precio_venta
            var total_detalle = (precioCaja * product.cantidad).toDouble().roundToInt().toDouble()
            when (product.iva) {
                0 -> exenta = (precioCaja * product.cantidad).roundToInt().toDouble()
                5 -> {
                    iva5 = ((precioCaja * product.cantidad) / 21).roundToInt().toDouble()
                    gravado5 = (precioCaja * product.cantidad) - iva5
                }
                10 -> {
                    iva10 = ((precioCaja * product.cantidad) / 11).roundToInt().toDouble()
                    gravado10 = (precioCaja * product.cantidad) - iva10
                }
            }

            ventasDet.add(VentaDetalleModel(null, null, product.id, product.iva, product.cantidad,
                    precioCaja.toInt(), precioCaja.toInt(), exenta, gravado5, iva5, gravado10, iva10, total_detalle, null, 1, product.codigo))
        }

        var saleSummaryMobile = SaleSummaryMobile(ventaCabeceraModel, ventasDet)
        return saleSummaryMobile
    }

    //Total sin IVA
    fun obtenerTotalBruto(productsList: ArrayList<ProductModel>): Long {
        var iva5: Long = 0
        var iva10: Long = 0
        var exenta: Long = 0
        var totalGeneral = 0
        var totalBruto: Long

        for (product in productsList) {
            when (product.iva) {
                0 -> exenta += product.precio_venta.toLong()
                5 -> iva5 += ((product.precio_venta * product.cantidad) / 21).toDouble().roundToInt()
                10 -> iva10 += ((product.precio_venta * product.cantidad) / 11).toDouble().roundToInt()
            }
            totalGeneral += (product.precio_venta * product.cantidad).toInt()
        }

        totalBruto = totalGeneral - (iva10 + iva5)
        return totalBruto
    }

    //Total de productos exentas
    fun obtenerTotalBrutoExento(productsList: ArrayList<ProductModel>): Long {
        var totalBrutoExento: Long = 0
        for (product in productsList) {
            if (product.iva == 0) totalBrutoExento += (product.precio_venta * product.cantidad).toLong()
        }
        return totalBrutoExento
    }

    //Total con IVA
    fun obtenerTotalNeto(productsList: ArrayList<ProductModel>): Long {
        var totalNeto: Long = 0
        for (product in productsList) {
            totalNeto += (product.precio_venta * product.cantidad).toLong()
        }
        return totalNeto
    }

    //Total de IVA 5%
    fun obtenerTotalIVA_5(productsList: ArrayList<ProductModel>): Long {
        var totalIVA5: Long = 0

        for (product in productsList) {
            if (product.iva == 5) totalIVA5 += ((product.precio_venta * product.cantidad) / 21).toLong()
        }
        return totalIVA5
    }

    //Total de IVA 10%
    fun obtenerTotalIVA_10(productsList: ArrayList<ProductModel>): Long {
        var totalIVA10: Long = 0

        for (product in productsList) {
            if (product.iva == 10) totalIVA10 += ((product.precio_venta * product.cantidad) / 11).toLong()
        }
        return totalIVA10
    }

    //Total de IVA 5%
    fun obtenerTotalProductosIVA_5(productsList: ArrayList<ProductModel>): Long {
        var totalProductosIVA5: Long = 0

        for (product in productsList) {
            if (product.iva == 5) totalProductosIVA5 += (product.precio_venta * product.cantidad).toLong()
        }
        return totalProductosIVA5
    }

    //Total de IVA 10%
    fun obtenerTotalProductosIVA_10(productsList: ArrayList<ProductModel>): Long {
        var totalProductosIVA10: Long = 0

        for (product in productsList) {
            if (product.iva == 10) totalProductosIVA10 += (product.precio_venta * product.cantidad).toLong()
        }
        return totalProductosIVA10
    }

    fun checkCantidad(productsList: ArrayList<ProductModel>): String? {
        var message: String? = null
        for (item in productsList) {
            var cantidadDisponible = DBUtils.getProductStock(context!!, item.id)
            if (cantidadDisponible < item.cantidad) {
                message = "¡El producto ${item.nombre} solo tiene una cantidad disponible de $cantidadDisponible!"
                return message
            }
        }
        return message
    }
}
