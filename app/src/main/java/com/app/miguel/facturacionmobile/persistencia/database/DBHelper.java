package com.app.miguel.facturacionmobile.persistencia.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.app.miguel.facturacionmobile.persistencia.database.contracts.DocumentosOficialesContract.DocumentosOficialesEntry;
import com.app.miguel.facturacionmobile.persistencia.database.contracts.EntidadesContract.EntidadesEntry;
import com.app.miguel.facturacionmobile.persistencia.database.contracts.EntidadesSucursalesContract.EntidadSucursalesEntry;
import com.app.miguel.facturacionmobile.persistencia.database.contracts.GeoCiudadesContract.GeoCiudadesEntry;
import com.app.miguel.facturacionmobile.persistencia.database.contracts.ParametroVentaContract.ParametroVentasEntry;
import com.app.miguel.facturacionmobile.persistencia.database.contracts.ProductosContract.ProductosEntry;
import com.app.miguel.facturacionmobile.persistencia.database.contracts.UsuariosContract.UsuariosEntry;
import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasCabMobileContract.VentasCabMobileEntry;
import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasDetMobileContract.VentasDetMobileEntry;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "mobile_sales.db";
    public static final int DB_VERSION = 1;

    private static final String TEXT_TYPE = " TEXT";
    private static final String TEXT_TYPE_NN = " TEXT NOT NULL";
    private static final String INT_TYPE = " INTEGER";
    private static final String INT_TYPE_NN = " INTEGER NOT NULL";
    private static final String REAL_TYPE = " REAL";
    private static final String REAL_TYPE_NN = " REAL NOT NULL";
    private static final String COMMA_SEP = ",";
    private static final String createTableStatement = "CREATE TABLE IF NOT EXISTS ";
    private static final String dropTableStatement = "DROP TABLE IF EXISTS ";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //Tabla Usuarios;
    private final String DELETE_TABLE_PARAMETRO_VENTA =
            dropTableStatement + ParametroVentasEntry.TABLE_NAME;

    private final String CREATE_TABLE_PARAMETRO_VENTA =
            createTableStatement + ParametroVentasEntry.TABLE_NAME + " (" +
                    ParametroVentasEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    ParametroVentasEntry.ID_PARAM_VTA_MOBILE + INT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.ID_PEDIDO_REMISION + INT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.NRO_INI_FACT_AUTO + REAL_TYPE + COMMA_SEP +
                    ParametroVentasEntry.NRO_DESDE_FACT_MANUAL + REAL_TYPE + COMMA_SEP +
                    ParametroVentasEntry.NRO_HASTA_FACT_MANUAL + REAL_TYPE + COMMA_SEP +
                    ParametroVentasEntry.ID_SUCURSAL + INT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.CAJA + INT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.ID_EMPRESA + INT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.TIMBRADO + REAL_TYPE + COMMA_SEP +
                    ParametroVentasEntry.VALIDO_DESDE + TEXT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.VALIDO_HASTA + TEXT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.PORCENTAJE_VARIACION_PRECIO + REAL_TYPE + COMMA_SEP +
                    ParametroVentasEntry.DIRECCION + TEXT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.PAIS + TEXT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.DEPARTAMENTO + TEXT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.CIUDAD + TEXT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.RUC + TEXT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.RAZON_SOCIAL + TEXT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.TELEFONO + TEXT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.TIPO_PEDIDO_VENTA + INT_TYPE + COMMA_SEP +
                    ParametroVentasEntry.ULTIMO_NRO_FACTURA + REAL_TYPE + COMMA_SEP +
                    ParametroVentasEntry.VENDEDOR + INT_TYPE + ");";

    //Tabla Usuarios;
    private final String DELETE_TABLE_USUARIOS =
            dropTableStatement + UsuariosEntry.TABLE_NAME;

    private final String CREATE_TABLE_USUARIOS =
            createTableStatement + UsuariosEntry.TABLE_NAME + " (" +
                    UsuariosEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    UsuariosEntry.ID_USUARIO + INT_TYPE + COMMA_SEP +
                    UsuariosEntry.NOMBRE_USUARIO + TEXT_TYPE + COMMA_SEP +
                    UsuariosEntry.APELLIDO_NOMBRES + TEXT_TYPE + COMMA_SEP +
                    UsuariosEntry.CONTRASENIA + TEXT_TYPE + COMMA_SEP +
                    UsuariosEntry.FECHA_EXPIRA + TEXT_TYPE + COMMA_SEP +
                    UsuariosEntry.ID_ENTIDAD_SUCURSAL + INT_TYPE + COMMA_SEP +
                    UsuariosEntry.ESTADO + TEXT_TYPE + ");";

    //Tabla Productos
    private final String DELETE_TABLE_PRODUCTOS =
            dropTableStatement + ProductosEntry.TABLE_NAME;

    private final String CREATE_TABLE_PRODUCTOS =
            createTableStatement + ProductosEntry.TABLE_NAME + " (" +
                    ProductosEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    ProductosEntry.ID_PRODUCTO + INT_TYPE + COMMA_SEP +
                    ProductosEntry.CODIGO + TEXT_TYPE + COMMA_SEP +
                    ProductosEntry.NOMBRE + TEXT_TYPE + COMMA_SEP +
                    ProductosEntry.PRESENTACION + TEXT_TYPE + COMMA_SEP +
                    ProductosEntry.UNIDAD_MEDIDA + TEXT_TYPE + COMMA_SEP +
                    ProductosEntry.IVA + REAL_TYPE + COMMA_SEP +
                    ProductosEntry.STOCK + INT_TYPE_NN + COMMA_SEP +
                    ProductosEntry.STOCK_INICIAL + INT_TYPE_NN + COMMA_SEP +
                    ProductosEntry.COMPOSICION_VENTA + INT_TYPE + COMMA_SEP +
                    ProductosEntry.ID_PEDIDO_REMISION + INT_TYPE + COMMA_SEP +
                    ProductosEntry.PRECIO_UNIDAD + REAL_TYPE + COMMA_SEP +
                    ProductosEntry.PRECIO_CAJA + REAL_TYPE + ");";

    //Tabla Entidades Sucursales
    private final String DELETE_TABLE_ENTIDADES_SUCURSALES =
            dropTableStatement + EntidadSucursalesEntry.TABLE_NAME;

    private final String CREATE_TABLE_ENTIDADES_SUCURSALES =
            createTableStatement + EntidadSucursalesEntry.TABLE_NAME + " (" +
                    EntidadSucursalesEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL_M + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.ENTIDAD + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.RAZON_SOCIAL + TEXT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.RUC + TEXT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.NOMBRE_COMERCIAL + TEXT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.TIPO_CLIENTE + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.VENDEDOR + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.ESTADO + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.DEPARTAMENTO + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.CIUDAD + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.BARRIO + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.DIRECCION + TEXT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.TELEFONO + TEXT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.ID_PEDIDO_REMISION + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.ALTA_FECHA_HORA + TEXT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.ALTA_USUARIO + INT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.MODIFICADO_FECHA_HORA + TEXT_TYPE + COMMA_SEP +
                    EntidadSucursalesEntry.MODIFICADO_USUARIO + INT_TYPE + ");";

    //Tabla Documentos Oficiales
    private final String DELETE_TABLE_DOCUMENTOS_OFICIALES =
            dropTableStatement + DocumentosOficialesEntry.TABLE_NAME;

    private final String CREATE_TABLE_DOCUMENTOS_OFICIALES =
            createTableStatement + DocumentosOficialesEntry.TABLE_NAME + " (" +
                    DocumentosOficialesEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    DocumentosOficialesEntry.ID_DOCUMENTO_OFICIAL + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.ID_PEDIDO_REMISION + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.EMPRESA + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.SUCURSAL + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.CAJA + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.TIMBRADO + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.VIGENTE_HASTA + TEXT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.OPERACION_NUMERO + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.DOCUMENTO_TIPO + TEXT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.FECHA_DOCUMENTO + TEXT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.ID_DOCUMENTO_NUMERO + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.ESTADO_FISICO + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.IMPRESION_FECHA_HORA + TEXT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.IMPRESION_USUARIO + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.REIMPRESION_FECHA_HORA + TEXT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.REIMPRESION_USUARIO + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.ANULACION_FECHA_HORA + TEXT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.ANULACION_USUARIO + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.ALTA_FECHA_HORA + TEXT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.ALTA_USUARIO + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.DIAS_VENCIMIENTO + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.ENTIDAD_SUCURSAL + INT_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.BRUTO_EXENTO + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.BRUTO_GRAVADO_5 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.BRUTO_IVA_5 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.BRUTO_GRAVADO_10 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.BRUTO_IVA_10 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.DESC_EXENTO + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.DESC_GRAVADO_5 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.DESC_IVA_5 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.DESC_GRAVADO_10 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.DESC_IVA_10 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.NETO_EXENTO + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.NETO_GRAVADO_5 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.NETO_IVA_5 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.NETO_GRAVADO_10 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.NETO_IVA_10 + REAL_TYPE + COMMA_SEP +
                    DocumentosOficialesEntry.TOTAL_PAGADO + REAL_TYPE + ");";

    //Tabla Entidades
    private final String DELETE_TABLE_ENTIDADES =
            dropTableStatement + EntidadesEntry.TABLE_NAME;

    private final String CREATE_TABLE_ENTIDADES =
            createTableStatement + EntidadesEntry.TABLE_NAME + " (" +
                    EntidadesEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    EntidadesEntry.ID_ENTIDAD + INT_TYPE_NN + COMMA_SEP +
                    EntidadesEntry.RAZON_SOCIAL + TEXT_TYPE_NN + COMMA_SEP +
                    EntidadesEntry.NOMBRE_COMERCIAL + TEXT_TYPE + COMMA_SEP +
                    EntidadesEntry.RUC + TEXT_TYPE + COMMA_SEP +
                    EntidadesEntry.DV + INT_TYPE + COMMA_SEP +
                    EntidadesEntry.ID_PEDIDO_REMISION + INT_TYPE + COMMA_SEP +
                    EntidadesEntry.ALTA_FECHA_HORA + TEXT_TYPE + COMMA_SEP +
                    EntidadesEntry.ALTA_USUARIO + INT_TYPE + COMMA_SEP +
                    EntidadesEntry.MODIFICADO_FECHA_HORA + TEXT_TYPE + COMMA_SEP +
                    EntidadesEntry.MODIFICADO_USUARIO + TEXT_TYPE + ");";

    //Tabla Ventas Cab
    private final String DELETE_TABLE_VENTAS_CAB_MOBILE =
            dropTableStatement + VentasCabMobileEntry.TABLE_NAME;

    private final String CREATE_TABLE_VENTAS_CAB_MOBILE =
            createTableStatement + VentasCabMobileEntry.TABLE_NAME + " (" +
                    VentasCabMobileEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    VentasCabMobileEntry.ID_VENTA + INT_TYPE_NN + COMMA_SEP +
                    VentasCabMobileEntry.ID_PEDIDO_REMISION + INT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.ID_ENTIDAD + INT_TYPE_NN + COMMA_SEP +
                    VentasCabMobileEntry.ID_ENTIDAD_SUCURSAL + INT_TYPE_NN + COMMA_SEP +
                    VentasCabMobileEntry.CONDICION_VENTA + INT_TYPE_NN + COMMA_SEP +
                    VentasCabMobileEntry.FECHA_EMISION + TEXT_TYPE_NN + COMMA_SEP +
                    VentasCabMobileEntry.NRO_DOCUMENTO + TEXT_TYPE_NN + COMMA_SEP +
                    VentasCabMobileEntry.FECHA_VENCIMIENTO + TEXT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.FECHA_ALTA + TEXT_TYPE_NN + COMMA_SEP +
                    VentasCabMobileEntry.USUARIO_ALTA + INT_TYPE_NN + COMMA_SEP +
                    VentasCabMobileEntry.ESTADO+ TEXT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.FECHA_ANULADO+ TEXT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.USUARIO_ANULADO + INT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.BRUTO_EXENTO + REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.BRUTO_GRAVADO_5 + REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.BRUTO_IVA_5 + REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.BRUTO_GRAVADO_10 + REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.BRUTO_IVA_10 + REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.NETO_EXENTO + REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.NETO_GRAVADO_5 + REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.NETO_IVA_5 + REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.NETO_GRAVADO_10 + REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.NETO_IVA_10 + REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.TOTAL_PAGADO+ REAL_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.TIPO_PAGO + INT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.SYNC_DB_ID + INT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.SYNC_DB_FECHA + TEXT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.REIMPRESION_FECHA_HORA + TEXT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.REIMPRESION_USUARIO + INT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.REIMPRESION_CANTIDAD + INT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.VENDEDOR + INT_TYPE + COMMA_SEP +
                    VentasCabMobileEntry.COMENTARIO + TEXT_TYPE + ");";


    //Tabla Ventas Det
    private final String DELETE_TABLE_VENTAS_DET_MOBILE =
            dropTableStatement + VentasDetMobileEntry.TABLE_NAME;

    private final String CREATE_TABLE_VENTAS_DET_MOBILE =
            createTableStatement + VentasDetMobileEntry.TABLE_NAME + " (" +
                    VentasDetMobileEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    VentasDetMobileEntry.ID_VENTA_DET + INT_TYPE_NN + COMMA_SEP +
                    VentasDetMobileEntry.ID_VENTA + INT_TYPE_NN + COMMA_SEP +
                    VentasDetMobileEntry.ID_PRODUCTO + INT_TYPE_NN + COMMA_SEP +
                    VentasDetMobileEntry.IVA + INT_TYPE_NN + COMMA_SEP +
                    VentasDetMobileEntry.CANTIDAD + INT_TYPE_NN + COMMA_SEP +
                    VentasDetMobileEntry.PRECIO + INT_TYPE + COMMA_SEP +
                    VentasDetMobileEntry.PRECIO_VENTA + REAL_TYPE_NN + COMMA_SEP +
                    VentasDetMobileEntry.TOTAL_DET + REAL_TYPE + COMMA_SEP +
                    VentasDetMobileEntry.EXENTO + REAL_TYPE + COMMA_SEP +
                    VentasDetMobileEntry.GRAVADO_5 + REAL_TYPE + COMMA_SEP +
                    VentasDetMobileEntry.IVA_5 + REAL_TYPE + COMMA_SEP +
                    VentasDetMobileEntry.GRAVADO_10 + REAL_TYPE + COMMA_SEP +
                    VentasDetMobileEntry.IVA_10 + REAL_TYPE + COMMA_SEP +
                    VentasDetMobileEntry.FECHA_ALTA + TEXT_TYPE + COMMA_SEP +
                    VentasDetMobileEntry.USUARIO_ALTA + INT_TYPE + COMMA_SEP +
                    VentasDetMobileEntry.PRODUCTO_CODIGO + TEXT_TYPE + ");";

    //Tabla Geo Ciudades
    private final String DELETE_TABLE_GEO_CIUDADES =
            dropTableStatement + GeoCiudadesEntry.TABLE_NAME;

    private final String CREATE_TABLE_GEO_CIUDADES =
            createTableStatement + GeoCiudadesEntry.TABLE_NAME + " (" +
                    GeoCiudadesEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    GeoCiudadesEntry.ID_CIUDAD_M+ INT_TYPE + COMMA_SEP +
                    GeoCiudadesEntry.NOMBRE_CIUDAD_DEPTO + TEXT_TYPE + COMMA_SEP +
                    GeoCiudadesEntry.CIUDAD_ID + INT_TYPE + COMMA_SEP +
                    GeoCiudadesEntry.CIUDAD_DESCRIPCION + TEXT_TYPE + COMMA_SEP +
                    GeoCiudadesEntry.DEPARTAMENTO_ID + INT_TYPE + COMMA_SEP +
                    GeoCiudadesEntry.DEPARTAMENTO_DESCRIPCION + TEXT_TYPE + COMMA_SEP +
                    GeoCiudadesEntry.ALTA_FECHA_HORA + TEXT_TYPE + COMMA_SEP +
                    GeoCiudadesEntry.ALTA_USUARIO + INT_TYPE + ");";

    @Override
    public void onCreate(SQLiteDatabase db) {
        String TAG = "onCreate DB";
        db.execSQL(CREATE_TABLE_PARAMETRO_VENTA);
        db.execSQL(CREATE_TABLE_USUARIOS);
        db.execSQL(CREATE_TABLE_PRODUCTOS);
        db.execSQL(CREATE_TABLE_ENTIDADES);
        db.execSQL(CREATE_TABLE_ENTIDADES_SUCURSALES);
        db.execSQL(CREATE_TABLE_DOCUMENTOS_OFICIALES);
        db.execSQL(CREATE_TABLE_VENTAS_CAB_MOBILE);
        db.execSQL(CREATE_TABLE_VENTAS_DET_MOBILE);
        db.execSQL(CREATE_TABLE_GEO_CIUDADES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String TAG = "onUpgrade DB";
        db.execSQL(DELETE_TABLE_PARAMETRO_VENTA);
        db.execSQL(DELETE_TABLE_USUARIOS);
        db.execSQL(DELETE_TABLE_PRODUCTOS);
        db.execSQL(DELETE_TABLE_ENTIDADES);
        db.execSQL(DELETE_TABLE_ENTIDADES_SUCURSALES);
        db.execSQL(DELETE_TABLE_DOCUMENTOS_OFICIALES);
        db.execSQL(DELETE_TABLE_VENTAS_CAB_MOBILE);
        db.execSQL(DELETE_TABLE_VENTAS_DET_MOBILE);
        db.execSQL(DELETE_TABLE_GEO_CIUDADES);
    }
}
