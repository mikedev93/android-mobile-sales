package com.app.miguel.facturacionmobile.persistencia.servicios.tasks

import android.content.Context
import android.os.AsyncTask
import com.app.miguel.facturacionmobile.HomeActivity
import com.app.miguel.facturacionmobile.SyncActivity
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.database.contracts.ProductosContract
import com.app.miguel.facturacionmobile.persistencia.models.ListProductStock
import com.app.miguel.facturacionmobile.persistencia.models.ProductStockModel
import com.app.miguel.facturacionmobile.persistencia.servicios.SyncAPI
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SyncProductsTask (var context: Context, var mainActivity: SyncActivity, var idRemision: Int): AsyncTask<Void, Void, String>(){

    private var syncroApi: SyncAPI? = null
    private var mRestAdapter: Retrofit? = null

    override fun doInBackground(vararg params: Void?): String {
        val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(40, TimeUnit.MINUTES)
                .writeTimeout(40, TimeUnit.MINUTES)
                .build()

        mRestAdapter = Retrofit.Builder()
                .baseUrl(Utils.getBaseURL(context)!!)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        syncroApi = mRestAdapter!!.create(SyncAPI::class.java)
        var productResponse = syncroApi!!.getProductos(idRemision)
        productResponse.enqueue(object : retrofit2.Callback<List<ProductStockModel>> {
            override fun onFailure(call: retrofit2.Call<List<ProductStockModel>>?, t: Throwable?) {
                mainActivity.showLoading(false)
                mainActivity.showSnackbarError(t!!.message!!)
            }

            override fun onResponse(call: retrofit2.Call<List<ProductStockModel>>?, response: Response<List<ProductStockModel>>?) {
                var productResponse = response!!.body()
                if (productResponse != null && productResponse.isNotEmpty()) {
                    DBUtils.vaciarTabla(context, ProductosContract.ProductosEntry.TABLE_NAME)
                    var list: ListProductStock = ListProductStock(ArrayList())
                    for (item in productResponse) {
                        list.productList.add(item)
                    }
                    DBUtils.saveProductos(context, list)
                    Thread.sleep(1000)
                    mainActivity.syncCiudades()
                } else {
                    Thread.sleep(1000)
                    mainActivity.showLoading(false)
                    mainActivity.showSnackbarError(Utils.NO_DATA_REMISION_ERROR)
                }
            }
        })
        return ""
    }
}