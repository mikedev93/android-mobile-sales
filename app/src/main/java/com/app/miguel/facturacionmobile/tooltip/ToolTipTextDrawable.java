package com.app.miguel.facturacionmobile.tooltip;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import com.app.miguel.facturacionmobile.R;


/**
 * ToolTipTextDrawable
 */
class ToolTipTextDrawable extends Drawable {

    static final String TAG = "ToolTipTextDrawable";

    private final RectF rectF;

    private final Path path;

    private final Paint bgPaint;

    private final Paint stPaint;

    private final float arrowRatio;

    private final float ellipseSize;

    private final int strokeWidth;

    private final int strokeColor;

    private final int backgroundColor;

    private Point point;

    private int padding = 0;

    private int arrowWeight = 0;

    private TooltipManager.Gravity gravity;

    ToolTipTextDrawable(final Context context, final TooltipManager.Builder builder) {

        TypedArray theme = context.getTheme().obtainStyledAttributes(null, R.styleable.TooltipLayout, builder.defStyleAttr, builder.defStyleRes);
        this.ellipseSize = theme.getDimensionPixelSize(R.styleable.TooltipLayout_ttlm_cornerRadius, 4);
        this.strokeWidth = theme.getDimensionPixelSize(R.styleable.TooltipLayout_ttlm_strokeWeight, 30);
        this.backgroundColor = theme.getColor(R.styleable.TooltipLayout_ttlm_backgroundColor, 0);
        this.strokeColor = theme.getColor(R.styleable.TooltipLayout_ttlm_strokeColor, 0);
        this.arrowRatio = theme.getFloat(R.styleable.TooltipLayout_ttlm_arrowRatio, 1.4f);
        theme.recycle();

        this.rectF = new RectF();

        if (backgroundColor != 0) {
            bgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            bgPaint.setColor(this.backgroundColor);
            bgPaint.setStyle(Paint.Style.FILL);
        } else {
            bgPaint = null;
        }

        if (strokeColor != 0) {
            stPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            stPaint.setColor(strokeColor);
            stPaint.setStyle(Paint.Style.STROKE);
            stPaint.setStrokeWidth(strokeWidth);
        } else {
            stPaint = null;
        }

        path = new Path();
    }

    private void calculatePath(Rect outBounds) {

        int left = outBounds.left + padding;
        int top = outBounds.top + padding;
        int right = outBounds.right - padding;
        int bottom = outBounds.bottom - padding;

        final float maxY = bottom - ellipseSize;
        final float maxX = right - ellipseSize;
        final float minY = top + ellipseSize;
        final float minX = left + ellipseSize;

        if (null != point && null != gravity) {

            boolean drawPoint = isDrawPoint(new int[]{left, top, right, bottom}, new float[]{maxY, maxX}, new float[] {minY, minX});

            path.reset();

            // clamp the point..
            clampPoint(left, top, right, bottom);

            // top/left
            drawPathTopLeft(outBounds, left, top, drawPoint);

            // top/right
            drawPathTopRight(outBounds, top, right, drawPoint);

            // bottom/right
            drawPathBottomRight(outBounds, left, right, bottom, drawPoint);

            // bottom/left
            drawPathBottomLeft(outBounds, left, top, bottom, drawPoint);

            // top/left
            path.lineTo(left, top + ellipseSize);
            path.quadTo(left, top, left + ellipseSize, top);
        } else {
            rectF.set(left, top, right, bottom);
            path.addRoundRect(rectF, ellipseSize, ellipseSize, Path.Direction.CW);
        }
    }

    private boolean isDrawPoint(int[] arr, float[] max, float[] min) {
        boolean drawPoint = false;
        int left = arr[0];
        int top = arr[1];
        int right = arr[2];
        int bottom = arr[3];
        float maxY = max[0];
        float maxX = max[1];
        float minY = min[0];
        float minX = min[1];
        if (gravity == TooltipManager.Gravity.RIGHT || gravity == TooltipManager.Gravity.LEFT) {
            if (point.y >= top && point.y <= bottom) {
                setYPoint(top, maxY, minY);
                drawPoint = true;
            }
        } else {
            if (point.x >= left && point.x <= right) {
                setXPoint(left, maxX, minX);
                drawPoint = true;
            }
        }
        return drawPoint;
    }

    private void drawPathTopLeft(Rect outBounds, int left, int top, boolean drawPoint) {
        path.moveTo(left + ellipseSize, top);

        if (drawPoint && gravity == TooltipManager.Gravity.BOTTOM) {
            path.lineTo(left + point.x - arrowWeight, top);
            path.lineTo(left + point.x, outBounds.top);
            path.lineTo(left + point.x + arrowWeight, top);
        }
    }

    private void drawPathBottomLeft(Rect outBounds, int left, int top, int bottom, boolean drawPoint) {
        path.lineTo(left + ellipseSize, bottom);
        path.quadTo(left, bottom, left, bottom - ellipseSize);

        if (drawPoint && gravity == TooltipManager.Gravity.RIGHT) {
            path.lineTo(left, top + point.y + arrowWeight);
            path.lineTo(outBounds.left, top + point.y);
            path.lineTo(left, top + point.y - arrowWeight);
        }
    }

    private void drawPathBottomRight(Rect outBounds, int left, int right, int bottom, boolean drawPoint) {
        path.lineTo(right, bottom - ellipseSize);
        path.quadTo(right, bottom, right - ellipseSize, bottom);

        if (drawPoint && gravity == TooltipManager.Gravity.TOP) {
            path.lineTo(left + point.x + arrowWeight, bottom);
            path.lineTo(left + point.x, outBounds.bottom);
            path.lineTo(left + point.x - arrowWeight, bottom);
        }
    }

    private void drawPathTopRight(Rect outBounds, int top, int right, boolean drawPoint) {
        path.lineTo(right - ellipseSize, top);
        path.quadTo(right, top, right, top + ellipseSize);

        if (drawPoint && gravity == TooltipManager.Gravity.LEFT) {
            path.lineTo(right, top + point.y - arrowWeight);
            path.lineTo(outBounds.right, top + point.y);
            path.lineTo(right, top + point.y + arrowWeight);
        }
    }

    private void clampPoint(int left, int top, int right, int bottom) {
        if (point.y < top) {
            point.y = top;
        }
        if (point.y > bottom) {
            point.y = bottom;
        }
        if (point.x < left) {
            point.x = left;
        }
        if (point.x > right) {
            point.x = right;
        }
    }

    private void setXPoint(int left, float maxX, float minX) {
        if (left + point.x + arrowWeight > maxX) {
            point.x = (int) (maxX - arrowWeight - left);
        } else if (left + point.x - arrowWeight < minX) {
            point.x = (int) (minX + arrowWeight - left);
        }
    }

    private void setYPoint(int top, float maxY, float minY) {
        if (top + point.y + arrowWeight > maxY) {
            point.y = (int) (maxY - arrowWeight - top);
        } else if (top + point.y - arrowWeight < minY) {
            point.y = (int) (minY + arrowWeight - top);
        }
    }

    @Override
    public void draw(final Canvas canvas) {

        if (null != bgPaint) {
            canvas.drawPath(path, bgPaint);
        }

        if (null != stPaint) {
            canvas.drawPath(path, stPaint);
        }
    }

    @Override
    public void setAlpha(final int alpha) {
        // unused
    }

    @Override
    public void setColorFilter(final ColorFilter cf) {
        // unused
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }

    @Override
    protected void onBoundsChange(final Rect bounds) {
        super.onBoundsChange(bounds);
        calculatePath(bounds);
    }

    public void setDestinationPoint(final Point point) {
        this.point = new Point(point);
    }

    public void setAnchor(final TooltipManager.Gravity gravity, int padding) {
        this.gravity = gravity;
        this.padding = padding;
        this.arrowWeight = (int) ((float) padding / arrowRatio);
    }
}