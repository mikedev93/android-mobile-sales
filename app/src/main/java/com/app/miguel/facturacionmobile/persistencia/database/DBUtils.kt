package com.app.miguel.facturacionmobile.persistencia.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.SalesManager
import com.app.miguel.facturacionmobile.persistencia.database.contracts.EntidadesContract.EntidadesEntry
import com.app.miguel.facturacionmobile.persistencia.database.contracts.EntidadesSucursalesContract.EntidadSucursalesEntry
import com.app.miguel.facturacionmobile.persistencia.database.contracts.GeoCiudadesContract.GeoCiudadesEntry
import com.app.miguel.facturacionmobile.persistencia.database.contracts.ParametroVentaContract.ParametroVentasEntry
import com.app.miguel.facturacionmobile.persistencia.database.contracts.ProductosContract.ProductosEntry
import com.app.miguel.facturacionmobile.persistencia.database.contracts.UsuariosContract.UsuariosEntry
import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasCabMobileContract.VentasCabMobileEntry
import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasDetMobileContract.VentasDetMobileEntry
import com.app.miguel.facturacionmobile.persistencia.models.*
import com.app.miguel.facturacionmobile.persistencia.servicios.*
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DBUtils {

    companion object {

        fun getNroFacturaDisplay(context: Context): String {
            var parametroVenta = getSaleParameters(context)
            var numeroFact = Utils.extraerNroFactura(parametroVenta!!.nroini_fcauto.toString())
            var puntoVenta = Utils.extraerPuntoVenta(parametroVenta!!.nroini_fcauto.toString())
            var sucursal = Utils.extraerSucursal(parametroVenta!!.nroini_fcauto.toString())
            var nroFactuarDisplay = "$sucursal-$puntoVenta-$numeroFact"
            return nroFactuarDisplay
        }

        fun getNextNroFactura(context: Context): String {
            var numeroFactAuto: Long = getLastInvoiceNumber(context)
            var numeroFact = Utils.extraerNroFactura(numeroFactAuto.toString())
            var puntoVenta = Utils.extraerPuntoVenta(numeroFactAuto.toString())
            var sucursal = Utils.extraerSucursal(numeroFactAuto.toString())
            var nroFactuarDisplay = "$sucursal-$puntoVenta-$numeroFact"
            if (checkCurrentInvoiceNumberExists(context, nroFactuarDisplay)) {
                numeroFact = Utils.extraerNroFactura((numeroFactAuto + 1).toString())
                nroFactuarDisplay = "$sucursal-$puntoVenta-$numeroFact"
            }
            return nroFactuarDisplay
        }

        fun saveUsuario(context: Context, usuarioModel: UsuarioModel) {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            var values = ContentValues()

            values.put(UsuariosEntry.ID_USUARIO, usuarioModel.id_usuario)
            values.put(UsuariosEntry.NOMBRE_USUARIO, usuarioModel.nombre_usuario)
            values.put(UsuariosEntry.APELLIDO_NOMBRES, usuarioModel.apellido_nombres)
            values.put(UsuariosEntry.CONTRASENIA, usuarioModel.contrasenia)
            values.put(UsuariosEntry.FECHA_EXPIRA, usuarioModel.fecha_expira)
            values.put(UsuariosEntry.ESTADO, usuarioModel.estado)
            values.put(UsuariosEntry.ID_ENTIDAD_SUCURSAL, usuarioModel.idEntidadSucursal)

            database.insert(UsuariosEntry.TABLE_NAME, null, values)
            database.close()
        }

        fun validateUser(context: Context, username: String, password: String): UsuarioModel? {
            val dbHelper: DBHelper = DBHelper(context)
            val database: SQLiteDatabase = dbHelper.readableDatabase
            val query = "select * from ${UsuariosEntry.TABLE_NAME} where ${UsuariosEntry.NOMBRE_USUARIO} = '$username' and ${UsuariosEntry.CONTRASENIA} = '$password'"
            var cursor = database.rawQuery(query, null)
            var returnUser: UsuarioModel? = null
            if (cursor.count > 0) {
                cursor.moveToFirst()
                returnUser = UsuarioModel(cursor.getInt(cursor.getColumnIndex(UsuariosEntry.ID_USUARIO)),
                        cursor.getString(cursor.getColumnIndex(UsuariosEntry.NOMBRE_USUARIO)),
                        cursor.getString(cursor.getColumnIndex(UsuariosEntry.APELLIDO_NOMBRES)),
                        cursor.getString(cursor.getColumnIndex(UsuariosEntry.CONTRASENIA)),
                        cursor.getString(cursor.getColumnIndex(UsuariosEntry.FECHA_EXPIRA)),
                        cursor.getInt(cursor.getColumnIndex(UsuariosEntry.ID_ENTIDAD_SUCURSAL)),
                        cursor.getString(cursor.getColumnIndex(UsuariosEntry.ESTADO)))
            }
            cursor.close()
            database.close()
            return returnUser

        }

        fun saveSaleParameters(context: Context, parametro: ParametroVentaModel) {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            var values = ContentValues()

            values.put(ParametroVentasEntry.ID_PARAM_VTA_MOBILE, parametro.idparam_vta_mobile)
            values.put(ParametroVentasEntry.ID_PEDIDO_REMISION, parametro.idparam_vta_mobile)
            values.put(ParametroVentasEntry.NRO_INI_FACT_AUTO, parametro.nroini_fcauto)
            values.put(ParametroVentasEntry.NRO_DESDE_FACT_MANUAL, parametro.nrodesde_fcmanual)
            values.put(ParametroVentasEntry.NRO_HASTA_FACT_MANUAL, parametro.nrohasta_fcmanual)
            values.put(ParametroVentasEntry.VENDEDOR, parametro.vendedor)
            values.put(ParametroVentasEntry.ID_SUCURSAL, parametro.idSucursal)
            values.put(ParametroVentasEntry.CAJA, parametro.idCaja)
            values.put(ParametroVentasEntry.ID_EMPRESA, parametro.idEmpresa)
            values.put(ParametroVentasEntry.TIMBRADO, parametro.timbrado)
            values.put(ParametroVentasEntry.VALIDO_DESDE, parametro.validoDesde)
            values.put(ParametroVentasEntry.VALIDO_HASTA, parametro.validoHasta)
            values.put(ParametroVentasEntry.PORCENTAJE_VARIACION_PRECIO, parametro.porcentajeVariacionPrecio)
            values.put(ParametroVentasEntry.DIRECCION, parametro.direccion)
            values.put(ParametroVentasEntry.PAIS, parametro.pais)
            values.put(ParametroVentasEntry.DEPARTAMENTO, parametro.departamento)
            values.put(ParametroVentasEntry.CIUDAD, parametro.ciudad)
            values.put(ParametroVentasEntry.RUC, parametro.ruc)
            values.put(ParametroVentasEntry.RAZON_SOCIAL, parametro.razonSocial)
            values.put(ParametroVentasEntry.TIPO_PEDIDO_VENTA, parametro.tipoPedidoVenta)
            values.put(ParametroVentasEntry.TELEFONO, parametro.telefono)
            database.insert(ParametroVentasEntry.TABLE_NAME, null, values)
            database.close()
        }

        fun getSaleParameters(context: Context): ParametroVentaModel? {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${ParametroVentasEntry.TABLE_NAME}"
            var cursor = database.rawQuery(query, null)
            var parametroVenta: ParametroVentaModel? = null
            if (cursor.count > 0) {
                cursor.moveToFirst()
                parametroVenta = ParametroVentaModel(cursor.getInt(cursor.getColumnIndex(ParametroVentasEntry.ID_PARAM_VTA_MOBILE)),
                        cursor.getInt(cursor.getColumnIndex(ParametroVentasEntry.ID_PEDIDO_REMISION)),
                        cursor.getLong(cursor.getColumnIndex(ParametroVentasEntry.NRO_INI_FACT_AUTO)),
                        cursor.getLong(cursor.getColumnIndex(ParametroVentasEntry.NRO_DESDE_FACT_MANUAL)),
                        cursor.getLong(cursor.getColumnIndex(ParametroVentasEntry.NRO_HASTA_FACT_MANUAL)),
                        cursor.getInt(cursor.getColumnIndex(ParametroVentasEntry.VENDEDOR)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.DIRECCION)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.PAIS)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.DEPARTAMENTO)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.CIUDAD)),
                        cursor.getLong(cursor.getColumnIndex(ParametroVentasEntry.TIMBRADO)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.VALIDO_DESDE)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.VALIDO_HASTA)),
                        cursor.getDouble(cursor.getColumnIndex(ParametroVentasEntry.PORCENTAJE_VARIACION_PRECIO)),
                        cursor.getInt(cursor.getColumnIndex(ParametroVentasEntry.ID_EMPRESA)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.ID_SUCURSAL)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.CAJA)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.RUC)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.RAZON_SOCIAL)),
                        cursor.getInt(cursor.getColumnIndex(ParametroVentasEntry.TIPO_PEDIDO_VENTA)),
                        cursor.getString(cursor.getColumnIndex(ParametroVentasEntry.TELEFONO)))
            }
            cursor.close()
            database.close()
            return parametroVenta
        }

        fun saveGeoCiudades(context: Context, ciudades: List<GeoCiudadModel>){
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            for (item in ciudades) {
                var values = ContentValues()
                values.put(GeoCiudadesEntry.ID_CIUDAD_M, item.id)
                values.put(GeoCiudadesEntry.NOMBRE_CIUDAD_DEPTO, item.nombreCiudadDepto)
                values.put(GeoCiudadesEntry.CIUDAD_ID, item.ciudadId)
                values.put(GeoCiudadesEntry.CIUDAD_DESCRIPCION, item.ciudadDescripcion)
                values.put(GeoCiudadesEntry.DEPARTAMENTO_ID, item.departamentoId)
                values.put(GeoCiudadesEntry.DEPARTAMENTO_DESCRIPCION, item.departamentoDescripcion)
                values.put(GeoCiudadesEntry.ALTA_FECHA_HORA, item.altaFechaHora)
                values.put(GeoCiudadesEntry.ALTA_USUARIO, item.altaUsuario)

                var success = database.insert(GeoCiudadesEntry.TABLE_NAME, null, values)
                Log.d("Insert", "Success: ${success}")
            }
            database.close()
        }

        fun getGeoCiudadesAll(context: Context): ArrayList<GeoCiudadModel> {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${GeoCiudadesEntry.TABLE_NAME}"
            var cursor = database.rawQuery(query, null)
            var returnList: ArrayList<GeoCiudadModel> = ArrayList()

            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    returnList.add(GeoCiudadModel(cursor.getInt(cursor.getColumnIndex(GeoCiudadesEntry.ID_CIUDAD_M)),
                            cursor.getString(cursor.getColumnIndex(GeoCiudadesEntry.NOMBRE_CIUDAD_DEPTO)),
                            cursor.getInt(cursor.getColumnIndex(GeoCiudadesEntry.CIUDAD_ID)),
                            cursor.getString(cursor.getColumnIndex(GeoCiudadesEntry.CIUDAD_DESCRIPCION)),
                            cursor.getInt(cursor.getColumnIndex(GeoCiudadesEntry.DEPARTAMENTO_ID)),
                            cursor.getString(cursor.getColumnIndex(GeoCiudadesEntry.DEPARTAMENTO_DESCRIPCION)),
                            cursor.getString(cursor.getColumnIndex(GeoCiudadesEntry.ALTA_FECHA_HORA)),
                            cursor.getInt(cursor.getColumnIndex(GeoCiudadesEntry.ALTA_USUARIO))))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()

            return returnList
        }

        fun getGeoCiudadByID(context: Context, idCiudad: Int): GeoCiudadModel? {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${GeoCiudadesEntry.TABLE_NAME} where ${GeoCiudadesEntry.CIUDAD_ID} = $idCiudad"
            var cursor = database.rawQuery(query, null)
            var returnCiudad: GeoCiudadModel? = null

            if (cursor.count > 0) {
                cursor.moveToLast()
                returnCiudad = GeoCiudadModel(cursor.getInt(cursor.getColumnIndex(GeoCiudadesEntry.ID_CIUDAD_M)),
                        cursor.getString(cursor.getColumnIndex(GeoCiudadesEntry.NOMBRE_CIUDAD_DEPTO)),
                        cursor.getInt(cursor.getColumnIndex(GeoCiudadesEntry.CIUDAD_ID)),
                        cursor.getString(cursor.getColumnIndex(GeoCiudadesEntry.CIUDAD_DESCRIPCION)),
                        cursor.getInt(cursor.getColumnIndex(GeoCiudadesEntry.DEPARTAMENTO_ID)),
                        cursor.getString(cursor.getColumnIndex(GeoCiudadesEntry.DEPARTAMENTO_DESCRIPCION)),
                        cursor.getString(cursor.getColumnIndex(GeoCiudadesEntry.ALTA_FECHA_HORA)),
                        cursor.getInt(cursor.getColumnIndex(GeoCiudadesEntry.ALTA_USUARIO)))
            }
            cursor.close()
            database.close()

            return returnCiudad
        }

        private fun getLastInvoiceNumber(context: Context): Long {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select ${ParametroVentasEntry.ULTIMO_NRO_FACTURA}, ${ParametroVentasEntry.NRO_INI_FACT_AUTO} from ${ParametroVentasEntry.TABLE_NAME}"
            var cursor = database.rawQuery(query, null)
            var ultimoNumero: Long = 0
            if (cursor.count > 0) {
                cursor.moveToFirst()
                ultimoNumero = if (cursor.getLong(1) > cursor.getLong(0)) {
                    cursor.getLong(1)
                } else {
                    cursor.getLong(0) + 1
                }
            }
            cursor.close()
            database.close()

            return ultimoNumero
        }

        fun saveProductos(context: Context, listProductModel: ListProductStock) {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase

            for (item in listProductModel.productList) {
                var values = ContentValues()
                values.put(ProductosEntry.ID_PRODUCTO, item.id)
                values.put(ProductosEntry.CODIGO, item.codigo)
                values.put(ProductosEntry.NOMBRE, item.nombre)
                values.put(ProductosEntry.PRESENTACION, item.presentacion)
                values.put(ProductosEntry.UNIDAD_MEDIDA, item.unidadMedida)
                values.put(ProductosEntry.IVA, item.iva)
                values.put(ProductosEntry.STOCK, item.stock)
                values.put(ProductosEntry.STOCK_INICIAL, item.stock)
                values.put(ProductosEntry.COMPOSICION_VENTA, item.composicionVenta)
                values.put(ProductosEntry.ID_PEDIDO_REMISION, item.id_pedido_remision)
                values.put(ProductosEntry.PRECIO_UNIDAD, item.precioUnidad)
                values.put(ProductosEntry.PRECIO_CAJA, item.precioCaja)

                var succeed = database.insert(ProductosEntry.TABLE_NAME, null, values)
            }

            database.close()
        }

        fun getAllProductos(context: Context): ArrayList<ProductStockModel> {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${ProductosEntry.TABLE_NAME} order by ${ProductosEntry.NOMBRE} asc"
            var cursor = database.rawQuery(query, null)
            var productList: ArrayList<ProductStockModel> = ArrayList()
            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    productList.add(ProductStockModel(cursor.getInt(cursor.getColumnIndex(ProductosEntry.ID_PRODUCTO)),
                            cursor.getString(cursor.getColumnIndex(ProductosEntry.NOMBRE)),
                            cursor.getString(cursor.getColumnIndex(ProductosEntry.PRESENTACION)),
                            cursor.getString(cursor.getColumnIndex(ProductosEntry.UNIDAD_MEDIDA)),
                            cursor.getDouble(cursor.getColumnIndex(ProductosEntry.IVA)),
                            cursor.getString(cursor.getColumnIndex(ProductosEntry.CODIGO)),
                            cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_UNIDAD)),
                            cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA)),
                            cursor.getInt(cursor.getColumnIndex(ProductosEntry.STOCK)),
                            cursor.getInt(cursor.getColumnIndex(ProductosEntry.STOCK_INICIAL)),
                            cursor.getInt(cursor.getColumnIndex(ProductosEntry.ID_PEDIDO_REMISION)),
                            cursor.getInt(cursor.getColumnIndex(ProductosEntry.COMPOSICION_VENTA))))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()
            return productList
        }

        fun getAllProductosWithStock(context: Context): ArrayList<ProductModel> {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
//            var query = "select * from ${ProductosEntry.TABLE_NAME} where ${ProductosEntry.STOCK} > 0"
            var query = "select * from ${ProductosEntry.TABLE_NAME} where ${ProductosEntry.STOCK} > 0 order by ${ProductosEntry.NOMBRE} asc"
            var cursor = database.rawQuery(query, null)
            var productList: ArrayList<ProductModel> = ArrayList()
            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    var precioBase = if (cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA)) > 0) {
                        cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA))
                    } else {
                        cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_UNIDAD))
                    }
                    var precioVenta = if (cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA)) > 0) {
                        cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA))
                    } else {
                        cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_UNIDAD)) * cursor.getInt(cursor.getColumnIndex(ProductosEntry.COMPOSICION_VENTA))
                    }

                    productList.add(ProductModel(cursor.getInt(cursor.getColumnIndex(ProductosEntry.ID_PRODUCTO)),
                            cursor.getString(cursor.getColumnIndex(ProductosEntry.NOMBRE)),
                            cursor.getString(cursor.getColumnIndex(ProductosEntry.PRESENTACION)),
                            cursor.getInt(cursor.getColumnIndex(ProductosEntry.UNIDAD_MEDIDA)),
                            cursor.getInt(cursor.getColumnIndex(ProductosEntry.IVA)),
                            cursor.getInt(cursor.getColumnIndex(ProductosEntry.COMPOSICION_VENTA)),
                            cursor.getString(cursor.getColumnIndex(ProductosEntry.CODIGO)),
                            precioVenta,
                            cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_UNIDAD)),
                            cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA)),
                            precioBase))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()
            return productList
        }

        fun updateStock(context: Context, idProducto: Int, cantidad: Int, operacion: String): Int {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            var values = ContentValues()
            var cantidadDisponible = getProductStock(context, idProducto)
            var cantidadUpdate = -1
            when (operacion) {
                "-" -> {
                    //Restar de stock
                    cantidadUpdate = cantidadDisponible - cantidad
                }
                "+" -> {
                    //Agregar a stock
                    cantidadUpdate = cantidadDisponible + cantidad
                }
            }
            values.put(ProductosEntry.STOCK, cantidadUpdate)
            var whereClause = "${ProductosEntry.ID_PRODUCTO} = $idProducto"
            var success = database.update(ProductosEntry.TABLE_NAME, values, whereClause, null)
            database.close()

            return success
        }

        fun getProductStock(context: Context, idProducto: Int): Int {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select ${ProductosEntry.STOCK} from ${ProductosEntry.TABLE_NAME} where ${ProductosEntry.ID_PRODUCTO} = $idProducto"
            var cursor = database.rawQuery(query, null)
            var cantidad: Int = -1
            if (cursor.count > 0) {
                if (cursor.moveToLast()) cantidad = cursor.getInt(0)
            }
            cursor.close()
            database.close()
            return cantidad
        }

        fun getProductWithID(context: Context, idProducto: Int): ProductModel {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${ProductosEntry.TABLE_NAME} where ${ProductosEntry.ID_PRODUCTO} = $idProducto"
            var cursor = database.rawQuery(query, null)
            var returnProducto: ProductModel? = null
            if (cursor.count > 0) {
                if (cursor.moveToFirst()) {
                    var precioBase = if (cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA)) > 0) {
                        cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA))
                    } else {
                        cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_UNIDAD))
                    }
                    var precioVenta = if (cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA)) > 0) {
                        cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA))
                    } else {
                        cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_UNIDAD)) * cursor.getInt(cursor.getColumnIndex(ProductosEntry.COMPOSICION_VENTA))
                    }

                    returnProducto = ProductModel(cursor.getInt(cursor.getColumnIndex(ProductosEntry.ID_PRODUCTO)),
                            cursor.getString(cursor.getColumnIndex(ProductosEntry.NOMBRE)),
                            cursor.getString(cursor.getColumnIndex(ProductosEntry.PRESENTACION)),
                            cursor.getInt(cursor.getColumnIndex(ProductosEntry.UNIDAD_MEDIDA)),
                            cursor.getInt(cursor.getColumnIndex(ProductosEntry.IVA)),
                            cursor.getInt(cursor.getColumnIndex(ProductosEntry.COMPOSICION_VENTA)),
                            cursor.getString(cursor.getColumnIndex(ProductosEntry.CODIGO)),
                            precioVenta,
                            cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_UNIDAD)),
                            cursor.getDouble(cursor.getColumnIndex(ProductosEntry.PRECIO_CAJA)),
                            precioBase)
                }
            }
            cursor.close()
            database.close()
            return returnProducto!!
        }

        fun saveSaleMobile(context: Context, saleSummaryMobile: SaleSummaryMobile): Int {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            var valuesCab = ContentValues()
            var saleID: Int = getLastSaleID(context)

            valuesCab.put(VentasCabMobileEntry.ID_VENTA, saleID)
            valuesCab.put(VentasCabMobileEntry.ID_ENTIDAD, saleSummaryMobile.ventaCabeceraModel.id_entidad)
            valuesCab.put(VentasCabMobileEntry.ID_ENTIDAD_SUCURSAL, saleSummaryMobile.ventaCabeceraModel.id_entidad_sucursal)
            valuesCab.put(VentasCabMobileEntry.ID_PEDIDO_REMISION, saleSummaryMobile.ventaCabeceraModel.id_pedido_remision)
            valuesCab.put(VentasCabMobileEntry.CONDICION_VENTA, saleSummaryMobile.ventaCabeceraModel.condicion_venta)
            valuesCab.put(VentasCabMobileEntry.FECHA_EMISION, saleSummaryMobile.ventaCabeceraModel.fecha_emision)
            valuesCab.put(VentasCabMobileEntry.NRO_DOCUMENTO, saleSummaryMobile.ventaCabeceraModel.nro_documento)
            valuesCab.put(VentasCabMobileEntry.FECHA_VENCIMIENTO, saleSummaryMobile.ventaCabeceraModel.fecha_vencimiento)
            valuesCab.put(VentasCabMobileEntry.FECHA_ALTA, saleSummaryMobile.ventaCabeceraModel.fecha_emision)
            valuesCab.put(VentasCabMobileEntry.USUARIO_ALTA, saleSummaryMobile.ventaCabeceraModel.usuario_alta)
            valuesCab.put(VentasCabMobileEntry.ESTADO, "A")
            valuesCab.put(VentasCabMobileEntry.BRUTO_EXENTO, saleSummaryMobile.ventaCabeceraModel.bruto_exento)
            valuesCab.put(VentasCabMobileEntry.BRUTO_GRAVADO_5, saleSummaryMobile.ventaCabeceraModel.bruto_gravado_5)
            valuesCab.put(VentasCabMobileEntry.BRUTO_IVA_5, saleSummaryMobile.ventaCabeceraModel.bruto_iva_5)
            valuesCab.put(VentasCabMobileEntry.BRUTO_GRAVADO_10, saleSummaryMobile.ventaCabeceraModel.bruto_gravado_10)
            valuesCab.put(VentasCabMobileEntry.BRUTO_IVA_10, saleSummaryMobile.ventaCabeceraModel.bruto_iva_10)
            valuesCab.put(VentasCabMobileEntry.NETO_EXENTO, saleSummaryMobile.ventaCabeceraModel.neto_exento)
            valuesCab.put(VentasCabMobileEntry.NETO_GRAVADO_5, saleSummaryMobile.ventaCabeceraModel.neto_gravado_5)
            valuesCab.put(VentasCabMobileEntry.NETO_IVA_5, saleSummaryMobile.ventaCabeceraModel.neto_iva_5)
            valuesCab.put(VentasCabMobileEntry.NETO_GRAVADO_10, saleSummaryMobile.ventaCabeceraModel.neto_gravado_10)
            valuesCab.put(VentasCabMobileEntry.NETO_IVA_10, saleSummaryMobile.ventaCabeceraModel.neto_iva_10)
            valuesCab.put(VentasCabMobileEntry.TOTAL_PAGADO, saleSummaryMobile.ventaCabeceraModel.total_pagado)
            valuesCab.put(VentasCabMobileEntry.TIPO_PAGO, saleSummaryMobile.ventaCabeceraModel.tipo_pago)
            valuesCab.put(VentasCabMobileEntry.VENDEDOR, SalesManager.loggedUser!!.idEntidadSucursal)
            valuesCab.put(VentasCabMobileEntry.COMENTARIO, saleSummaryMobile.ventaCabeceraModel.comentario)

            if (database.insert(VentasCabMobileEntry.TABLE_NAME, null, valuesCab) > -1) {

                for (item in saleSummaryMobile.ventaDetalles) {
                    var valuesDet = ContentValues()
                    valuesDet.put(VentasDetMobileEntry.ID_VENTA_DET, getLastSaleIDDET(context))
                    valuesDet.put(VentasDetMobileEntry.ID_VENTA, saleID)
                    valuesDet.put(VentasDetMobileEntry.ID_PRODUCTO, item.id_producto)
                    valuesDet.put(VentasDetMobileEntry.IVA, item.iva)
                    valuesDet.put(VentasDetMobileEntry.CANTIDAD, item.cantidad)
                    valuesDet.put(VentasDetMobileEntry.PRECIO, item.precio)
                    valuesDet.put(VentasDetMobileEntry.PRECIO_VENTA, item.precio_venta)
                    valuesDet.put(VentasDetMobileEntry.TOTAL_DET, item.total_detalle)
                    valuesDet.put(VentasDetMobileEntry.EXENTO, item.exento)
                    valuesDet.put(VentasDetMobileEntry.GRAVADO_5, item.gravado_5)
                    valuesDet.put(VentasDetMobileEntry.IVA_5, item.iva_5)
                    valuesDet.put(VentasDetMobileEntry.GRAVADO_10, item.gravado_10)
                    valuesDet.put(VentasDetMobileEntry.IVA_10, item.iva_10)
                    valuesDet.put(VentasDetMobileEntry.FECHA_ALTA, item.fecha_alta)
                    valuesDet.put(VentasDetMobileEntry.USUARIO_ALTA, item.usuario_alta)
                    valuesDet.put(VentasDetMobileEntry.PRODUCTO_CODIGO, item.producto_codigo)

                    if (database.insert(VentasDetMobileEntry.TABLE_NAME, null, valuesDet) > -1) {
                        updateStock(context, item.id_producto, item.cantidad, "-")
                        updateLastInvoiceNumber(context, saleSummaryMobile.ventaCabeceraModel)
                    }
                }
                database.close()
                return 1
            } else {
                database.close()
                return -1
            }
        }

        fun updateLastInvoiceNumber(context: Context, ventaCabeceraModel: VentaCabeceraModel) {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            var facturaNoFormat = ventaCabeceraModel.nro_documento!!.replace("-", "").trim()
            var facturaNumber = facturaNoFormat.toLong()
            var values = ContentValues()
            values.put(ParametroVentasEntry.ULTIMO_NRO_FACTURA, facturaNumber)

            database.update(ParametroVentasEntry.TABLE_NAME, values, "${ParametroVentasEntry.ID_PEDIDO_REMISION} = '${ventaCabeceraModel.id_pedido_remision}'", null)
            database.close()
        }

        fun checkCurrentInvoiceNumberExists(context: Context, invoiceNumber: String): Boolean {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select ${VentasCabMobileEntry.NRO_DOCUMENTO} from ${VentasCabMobileEntry.TABLE_NAME} where ${VentasCabMobileEntry.NRO_DOCUMENTO} = $invoiceNumber"
            var cursor = database.rawQuery(query, null)
            return if (cursor.count > 0) {
                cursor.close()
                database.close()
                true
            } else {
                cursor.close()
                database.close()
                false
            }
        }

        fun getLastEntidadID(context: Context): Int {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select MAX(${EntidadesEntry.ID_ENTIDAD}) from ${EntidadesEntry.TABLE_NAME}"
            var cursor = database.rawQuery(query, null)
            var id = 1
            if (cursor.count > 0) {
                if (cursor.moveToLast()) id = cursor.getInt(0) + 1
            }
            cursor.close()
            database.close()
            return id
        }

        fun getLastEntidadSucursalID(context: Context): Int {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select MAX(${EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL}) from ${EntidadSucursalesEntry.TABLE_NAME}"
            var cursor = database.rawQuery(query, null)
            var id = 1
            if (cursor.count > 0) {
                if (cursor.moveToLast()) id = cursor.getInt(0) + 1
            }
            cursor.close()
            database.close()
            return id
        }

        fun getLastSaleID(context: Context): Int {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select MAX(" + VentasCabMobileEntry.ID_VENTA + ") from " + VentasCabMobileEntry.TABLE_NAME
            var cursor = database.rawQuery(query, null)
            var id = 1
            if (cursor.count > 0) {
                if (cursor.moveToLast()) id = cursor.getInt(0) + 1
            }
            cursor.close()
            database.close()
            return id
        }

        fun getLastSaleIDDET(context: Context): Int {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select MAX(" + VentasDetMobileEntry.ID_VENTA_DET + ") from " + VentasDetMobileEntry.TABLE_NAME
            var cursor = database.rawQuery(query, null)
            var id = 1
            if (cursor.count > 0) {
                if (cursor.moveToLast()) id = cursor.getInt(0) + 1
            }
            cursor.close()
            database.close()
            return id
        }

        fun getSales(context: Context): ArrayList<SaleData> {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from " + VentasCabMobileEntry.TABLE_NAME
            var cursor = database.rawQuery(query, null)
            var returnList: ArrayList<SaleData>? = ArrayList()
            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    var entidadSucursalModel = getEntidadSucursal(context, cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_ENTIDAD_SUCURSAL)))
                    returnList!!.add(SaleData(entidadSucursalModel,
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.NRO_DOCUMENTO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_VENCIMIENTO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.ESTADO)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.CONDICION_VENTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_VENTA))))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()
            return returnList!!
        }

        fun getSaleFull(context: Context, idVenta: Int): SaleSummaryMobile? {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${VentasCabMobileEntry.TABLE_NAME} where ${VentasCabMobileEntry.ID_VENTA} = $idVenta"
            var returnSale: SaleSummaryMobile? = null
            var cursor = database.rawQuery(query, null)
            if (cursor.count > 0) {
                if (cursor.moveToLast()) {
                    returnSale = SaleSummaryMobile(VentaCabeceraModel(cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_VENTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_PEDIDO_REMISION)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_ENTIDAD)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_ENTIDAD_SUCURSAL)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.CONDICION_VENTA)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_EMISION)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.NRO_DOCUMENTO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_VENCIMIENTO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_ALTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.USUARIO_ALTA)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_ANULADO)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.USUARIO_ANULADO)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_EXENTO)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_GRAVADO_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_IVA_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_GRAVADO_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_IVA_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_EXENTO)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_GRAVADO_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_IVA_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_GRAVADO_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_IVA_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.TOTAL_PAGADO)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.TIPO_PAGO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.COMENTARIO))),
                            getSaleDetail(context, idVenta))
                }
            }
            cursor.close()
            database.close()
            return returnSale
        }

        fun getSaleDetail(context: Context, idVenta: Int): ArrayList<VentaDetalleModel> {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${VentasDetMobileEntry.TABLE_NAME} where ${VentasDetMobileEntry.ID_VENTA} = $idVenta"
            var cursor = database.rawQuery(query, null)
            var returnList: ArrayList<VentaDetalleModel>? = ArrayList()
            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    returnList!!.add(VentaDetalleModel(cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.ID_VENTA_DET)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.ID_VENTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.ID_PRODUCTO)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.IVA)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.CANTIDAD)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.PRECIO)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.PRECIO_VENTA)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.EXENTO)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.GRAVADO_5)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.IVA_5)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.GRAVADO_10)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.IVA_10)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.TOTAL_DET)),
                            cursor.getString(cursor.getColumnIndex(VentasDetMobileEntry.FECHA_ALTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.USUARIO_ALTA)),
                            cursor.getString(cursor.getColumnIndex(VentasDetMobileEntry.PRODUCTO_CODIGO))))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()
            return returnList!!
        }

        fun getSalesNotSyncd(context: Context): Int {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select count(${VentasCabMobileEntry.ID_VENTA}) from ${VentasCabMobileEntry.TABLE_NAME} where ${VentasCabMobileEntry.SYNC_DB_ID} is NULL"
            var cantidad: Int = 0
            var cursor = database.rawQuery(query, null)
            if (cursor.count > 0) {
                if (cursor.moveToLast()) {
                    cantidad = cursor.getInt(0)
                }
            }

            return cantidad
        }

        fun getSalesSync(context: Context): ArrayList<VentaCabeceraSync>? {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${VentasCabMobileEntry.TABLE_NAME}"
            var returnSale: ArrayList<VentaCabeceraSync>? = ArrayList()
            var cursor = database.rawQuery(query, null)
            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    returnSale!!.add(VentaCabeceraSync(VentaCabeceraModel(cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_VENTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_PEDIDO_REMISION)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_ENTIDAD)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_ENTIDAD_SUCURSAL)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.CONDICION_VENTA)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_EMISION)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.NRO_DOCUMENTO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_VENCIMIENTO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_ALTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.USUARIO_ALTA)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_ANULADO)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.USUARIO_ANULADO)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_EXENTO)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_GRAVADO_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_IVA_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_GRAVADO_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_IVA_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_EXENTO)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_GRAVADO_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_IVA_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_GRAVADO_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_IVA_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.TOTAL_PAGADO)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.TIPO_PAGO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.COMENTARIO))),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.SYNC_DB_ID)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.SYNC_DB_FECHA))))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()
            return returnSale
        }

        fun cancelInvoice(context: Context, idVenta: Int, idUsuario: Int): Int {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            val simpleDateTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val fecha_hora = simpleDateTimeFormat.format(Date())
            var values = ContentValues()
            values.put(VentasCabMobileEntry.ESTADO, "X")
            values.put(VentasCabMobileEntry.USUARIO_ANULADO, idUsuario)
            values.put(VentasCabMobileEntry.FECHA_ANULADO, fecha_hora)
            var success = database.update(VentasCabMobileEntry.TABLE_NAME, values, "${VentasCabMobileEntry.ID_VENTA} = $idVenta", null)
            if (success > -1) {
                var saleDetail = getSaleDetail(context, idVenta)
                for (item in saleDetail) {
                    updateStock(context, item.id_producto, item.cantidad, "+")
                }
            }
            database.close()

            return success
        }

        fun getInvoiceState(context: Context, idVenta: Int): String {
            val dbHelper: DBHelper = DBHelper(context)
            val database: SQLiteDatabase = dbHelper.readableDatabase
            var estado: String? = null
            var query = "select ${VentasCabMobileEntry.ESTADO} from ${VentasCabMobileEntry.TABLE_NAME} where ${VentasCabMobileEntry.ID_VENTA} = $idVenta"
            var cursor = database.rawQuery(query, null)
            var id = 1
            if (cursor.count > 0) {
                if (cursor.moveToLast()) estado = cursor.getString(0)
            }
            cursor.close()
            database.close()

            return estado!!
        }

        fun getInvoiceSyncd(context: Context, idVenta: Int): Boolean {
            val dbHelper: DBHelper = DBHelper(context)
            val database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select ${VentasCabMobileEntry.SYNC_DB_ID} from ${VentasCabMobileEntry.TABLE_NAME} where ${VentasCabMobileEntry.ID_VENTA} = $idVenta and ${VentasCabMobileEntry.SYNC_DB_ID} is NOT NULL"
            var cursor = database.rawQuery(query, null)
            var id = -1
            if (cursor.count > 0) {
                if (cursor.moveToLast()) id = cursor.getInt(0)
            }
            cursor.close()
            database.close()
            return id != -1
        }

        fun getEntidadWithID(context: Context, id: Int): EntidadModel? {
            val dbHelper: DBHelper = DBHelper(context)
            val database: SQLiteDatabase = dbHelper.readableDatabase
            val query = "select * from " + EntidadesEntry.TABLE_NAME + " where " + EntidadesEntry.ID_ENTIDAD + " = " + id.toString()
            var cursor = database.rawQuery(query, null)
            var returnEntidad: EntidadModel? = null
            if (cursor.count > 0) {
                cursor.moveToFirst()
                returnEntidad = EntidadModel(cursor.getInt(cursor.getColumnIndex(EntidadesEntry.ID_ENTIDAD)),
                        cursor.getString(cursor.getColumnIndex(EntidadesEntry.RAZON_SOCIAL)),
                        cursor.getString(cursor.getColumnIndex(EntidadesEntry.NOMBRE_COMERCIAL)),
                        cursor.getString(cursor.getColumnIndex(EntidadesEntry.RUC)),
                        cursor.getInt(cursor.getColumnIndex(EntidadesEntry.DV)),
                        cursor.getInt(cursor.getColumnIndex(EntidadesEntry.ID_PEDIDO_REMISION)),
                        cursor.getString(cursor.getColumnIndex(EntidadesEntry.ALTA_FECHA_HORA)),
                        cursor.getInt(cursor.getColumnIndex(EntidadesEntry.ALTA_USUARIO)),
                        cursor.getString(cursor.getColumnIndex(EntidadesEntry.MODIFICADO_FECHA_HORA)),
                        cursor.getInt(cursor.getColumnIndex(EntidadesEntry.MODIFICADO_USUARIO)))
            }
            cursor.close()
            database.close()
            return returnEntidad
        }

        fun getEntidadRUC(context: Context, id: Int): String? {
            val dbHelper: DBHelper = DBHelper(context)
            val database: SQLiteDatabase = dbHelper.readableDatabase
            val query = "select ${EntidadesEntry.RUC}, ${EntidadesEntry.DV} from ${EntidadesEntry.TABLE_NAME}  where ${EntidadesEntry.ID_ENTIDAD} = '${id.toString()}'"
            var cursor = database.rawQuery(query, null)
            var returnRUC: String? = null
            if (cursor.count > 0) {
                cursor.moveToFirst()
                returnRUC = cursor.getString(0) /*+ "-" + cursor.getInt(1).toString()*/
            }
            cursor.close()
            database.close()
            return returnRUC
        }

        fun getEntidadSucursal(context: Context, identidad: Int): EntidadSucursalModel? {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${EntidadSucursalesEntry.TABLE_NAME} where ${EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL} = " + identidad.toString()
            var cursor = database.rawQuery(query, null)
            var returnClient: EntidadSucursalModel? = null
            if (cursor.count > 0) {
                cursor.moveToFirst()
                returnClient = EntidadSucursalModel(cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL)),
                        cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ENTIDAD)),
                        cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.RAZON_SOCIAL)),
                        cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.RUC)),
                        cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.NOMBRE_COMERCIAL)),
                        cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.TIPO_CLIENTE)),
                        cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.VENDEDOR)),
                        cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ESTADO)),
                        cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.DEPARTAMENTO)),
                        cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.CIUDAD)),
                        cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.BARRIO)),
                        cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.DIRECCION)),
                        cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.TELEFONO)),
                        cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ID_PEDIDO_REMISION)),
                        cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.ALTA_FECHA_HORA)),
                        cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ALTA_USUARIO)),
                        cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.MODIFICADO_FECHA_HORA)),
                        cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.MODIFICADO_USUARIO)))
            }
            cursor.close()
            database.close()
            return returnClient
        }

        fun getListEntidadSucursales(context: Context, identidad: Int): ArrayList<EntidadSucursalModel>? {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from " + EntidadSucursalesEntry.TABLE_NAME + "where " + EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL + " = " + identidad.toString()
            var cursor = database.rawQuery(query, null)
            var returnClient: ArrayList<EntidadSucursalModel> = ArrayList()
            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    returnClient.add(EntidadSucursalModel(cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ENTIDAD)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.RAZON_SOCIAL)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.RUC)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.NOMBRE_COMERCIAL)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.TIPO_CLIENTE)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.VENDEDOR)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ESTADO)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.DEPARTAMENTO)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.CIUDAD)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.BARRIO)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.DIRECCION)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.TELEFONO)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ID_PEDIDO_REMISION)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.ALTA_FECHA_HORA)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ALTA_USUARIO)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.MODIFICADO_FECHA_HORA)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.MODIFICADO_USUARIO))))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()
            return returnClient
        }

        fun getListEntidadSucursalesAll(context: Context): ArrayList<EntidadSucursalModel>? {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from " + EntidadSucursalesEntry.TABLE_NAME
            var cursor = database.rawQuery(query, null)
            var returnClient: ArrayList<EntidadSucursalModel> = ArrayList()
            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    returnClient.add(EntidadSucursalModel(cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ENTIDAD)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.RAZON_SOCIAL)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.RUC)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.NOMBRE_COMERCIAL)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.TIPO_CLIENTE)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.VENDEDOR)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ESTADO)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.DEPARTAMENTO)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.CIUDAD)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.BARRIO)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.DIRECCION)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.TELEFONO)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ID_PEDIDO_REMISION)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.ALTA_FECHA_HORA)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ALTA_USUARIO)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.MODIFICADO_FECHA_HORA)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.MODIFICADO_USUARIO))))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()
            return returnClient
        }

        fun saveEntidad(context: Context, entidad: EntidadModel) {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            var values = ContentValues()

            values.put(EntidadesEntry.ID_ENTIDAD, entidad.identidad)
            values.put(EntidadesEntry.RAZON_SOCIAL, entidad.razonsocial)
            values.put(EntidadesEntry.NOMBRE_COMERCIAL, entidad.nombrecomercial)
            values.put(EntidadesEntry.RUC, entidad.ruc)
            values.put(EntidadesEntry.DV, entidad.dv)
            values.put(EntidadesEntry.ID_PEDIDO_REMISION, entidad.idpedido_remision)
            values.put(EntidadesEntry.ALTA_FECHA_HORA, entidad.altafechahora)
            values.put(EntidadesEntry.ALTA_USUARIO, entidad.altausuario)

            database.insert(EntidadesEntry.TABLE_NAME, null, values)
            database.close()
        }

        fun saveEntidadSucursal(context: Context, entidadSucursal: EntidadSucursalModel) {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            var values = ContentValues()

            values.put(EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL, entidadSucursal.identidadsucursal)
            values.put(EntidadSucursalesEntry.ENTIDAD, entidadSucursal.entidad)
            values.put(EntidadSucursalesEntry.RAZON_SOCIAL, entidadSucursal.razonSocial)
            values.put(EntidadSucursalesEntry.RUC, entidadSucursal.ruc)
            values.put(EntidadSucursalesEntry.NOMBRE_COMERCIAL, entidadSucursal.nombrecomercial)
            values.put(EntidadSucursalesEntry.TIPO_CLIENTE, entidadSucursal.tipocliente)
            values.put(EntidadSucursalesEntry.VENDEDOR, entidadSucursal.vendedor)
            values.put(EntidadSucursalesEntry.ESTADO, entidadSucursal.estado)
            values.put(EntidadSucursalesEntry.DEPARTAMENTO, entidadSucursal.departamento)
            values.put(EntidadSucursalesEntry.CIUDAD, entidadSucursal.ciudad)
            values.put(EntidadSucursalesEntry.BARRIO, entidadSucursal.barrio)
            values.put(EntidadSucursalesEntry.DIRECCION, entidadSucursal.direccion)
            values.put(EntidadSucursalesEntry.TELEFONO, entidadSucursal.telefono)
            values.put(EntidadSucursalesEntry.ID_PEDIDO_REMISION, entidadSucursal.idpedido_remision)
            values.put(EntidadSucursalesEntry.ALTA_FECHA_HORA, entidadSucursal.altafechahora)
            values.put(EntidadSucursalesEntry.ALTA_USUARIO, entidadSucursal.altausuario)
            values.put(EntidadSucursalesEntry.MODIFICADO_FECHA_HORA, entidadSucursal.modificadofechahora)
            values.put(EntidadSucursalesEntry.MODIFICADO_USUARIO, entidadSucursal.modificadousuario)

            database.insert(EntidadSucursalesEntry.TABLE_NAME, null, values)
            database.close()
        }

        fun saveNewEntidadSucursal(context: Context, client: NewEditClientModel): Int {
            var lastEntidadID = getLastEntidadID(context)
            var lastEntidadSucursalID = getLastEntidadSucursalID(context)
            val simpleDateTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val fecha_hora = simpleDateTimeFormat.format(Date())

            var entidadSucursal = EntidadSucursalModel(lastEntidadSucursalID, null, client.razonsocial!!, client.ruc!!, client.razonsocial!!,
                    1, SalesManager.loggedUser!!.idEntidadSucursal, 0, client.departamento!!, client.ciudad!!, 0, client.direccion, client.telefono,
                    client.idpedido_remision!!, fecha_hora, client.altausuario, client.modificadofechahora, client.modificadousuario)
            saveEntidadSucursal(context, entidadSucursal)

            return lastEntidadSucursalID
        }

        fun getNewEntidades(context: Context): ArrayList<EntidadSucursalModel> {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${EntidadSucursalesEntry.TABLE_NAME} where ${EntidadSucursalesEntry.ALTA_FECHA_HORA} is NOT NULL"
            var cursor = database.rawQuery(query, null)
            var returnClient: ArrayList<EntidadSucursalModel> = ArrayList()
            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    returnClient.add(EntidadSucursalModel(cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ENTIDAD)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.RAZON_SOCIAL)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.RUC)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.NOMBRE_COMERCIAL)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.TIPO_CLIENTE)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.VENDEDOR)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ESTADO)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.DEPARTAMENTO)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.CIUDAD)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.BARRIO)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.DIRECCION)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.TELEFONO)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ID_PEDIDO_REMISION)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.ALTA_FECHA_HORA)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.ALTA_USUARIO)),
                            cursor.getString(cursor.getColumnIndex(EntidadSucursalesEntry.MODIFICADO_FECHA_HORA)),
                            cursor.getInt(cursor.getColumnIndex(EntidadSucursalesEntry.MODIFICADO_USUARIO))))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()
            return returnClient
        }

        fun saveEntidadResponse(context: Context, response: List<SucursalResponse>) {

            for (item in response) {
                var entidadSucursal = EntidadSucursalModel(item.identidadsucursal, item.entidad, item.nombrecomercial, item.ruc, item.nombrecomercial, item.tipocliente,
                        item.vendedor, item.estado, item.departamento, item.ciudad, item.barrio, item.direccion, item.telefono, item.idpedido_remision,
                        null, null, item.modificadofechahora, item.modificadousuario)
                saveEntidadSucursal(context, entidadSucursal)
            }
        }

        fun updateEntidadSync(context: Context, response: NewEntidadesResponse){
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            var nullInt: Int? = null
            var nullString: String? = null
            var values = ContentValues()
            values.put(EntidadSucursalesEntry.ALTA_FECHA_HORA, nullString)
            values.put(EntidadSucursalesEntry.ALTA_USUARIO, nullInt)
            var success = database.update(EntidadSucursalesEntry.TABLE_NAME, values, "${EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL} = ${response.idEntidadSucursal}", null)
            database.close()
        }

        fun getRUCExists(context: Context, ruc: String): Boolean{
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select ${EntidadSucursalesEntry.ID_ENTIDAD_SUCURSAL} from ${EntidadSucursalesEntry.TABLE_NAME} where ${EntidadSucursalesEntry.RUC} = '$ruc'"
            var cursor = database.rawQuery(query, null)
            var existe = false
            if (cursor.count > 0) {
                if (cursor.moveToLast()) {
                    if (cursor.getInt(0) > 0) existe = true
                }
            }
            cursor.close()
            database.close()
            return existe
        }

        fun updateSaleSync(context: Context, response: VentasResponse){
            val simpleDateTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val fecha_hora = simpleDateTimeFormat.format(Date())
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            var values = ContentValues()
            values.put(VentasCabMobileEntry.SYNC_DB_FECHA, fecha_hora)
            values.put(VentasCabMobileEntry.SYNC_DB_ID, response.idVentaBD)
            var success = database.update(VentasCabMobileEntry.TABLE_NAME, values, "${VentasCabMobileEntry.ID_VENTA} = ${response.idVenta}", null)
            database.close()
        }

        fun getSaleRequest(context: Context): VentasRequest {
            val dbHelper: DBHelper = DBHelper(context)
            val database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select * from ${VentasCabMobileEntry.TABLE_NAME} where ${VentasCabMobileEntry.SYNC_DB_ID} is NULL"
            var cursor = database.rawQuery(query, null)
            var salesRequest: ArrayList<VentasCabRequest> = ArrayList()
            var parametroVenta = DBUtils.getSaleParameters(context)

            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    salesRequest.add(VentasCabRequest(cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry._ID)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_VENTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_PEDIDO_REMISION)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_ENTIDAD)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_ENTIDAD_SUCURSAL)),
                            parametroVenta!!.idSucursal!!.toInt(),
                            parametroVenta.idEmpresa!!,
                            parametroVenta.tipoPedidoVenta,
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.CONDICION_VENTA)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_EMISION)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.NRO_DOCUMENTO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_VENCIMIENTO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_ALTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.USUARIO_ALTA)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.ESTADO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.FECHA_ANULADO)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.USUARIO_ANULADO)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_EXENTO)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_GRAVADO_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_IVA_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_GRAVADO_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.BRUTO_IVA_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_EXENTO)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_GRAVADO_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_IVA_5)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_GRAVADO_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.NETO_IVA_10)),
                            cursor.getLong(cursor.getColumnIndex(VentasCabMobileEntry.TOTAL_PAGADO)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.TIPO_PAGO)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.REIMPRESION_FECHA_HORA)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.REIMPRESION_USUARIO)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.REIMPRESION_CANTIDAD)),
                            cursor.getString(cursor.getColumnIndex(VentasCabMobileEntry.COMENTARIO)),
                            cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.VENDEDOR)),
                            getSaleRequestDetail(context, cursor.getInt(cursor.getColumnIndex(VentasCabMobileEntry.ID_VENTA)))))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()

            return VentasRequest(salesRequest)
        }

        fun getSaleRequestDetail(context: Context, idVenta: Int): ArrayList<VentasDetalleReq> {
            val dbHelper: DBHelper = DBHelper(context)
            val database: SQLiteDatabase = dbHelper.readableDatabase
            var estado: String? = null
            var query = "select * from ${VentasDetMobileEntry.TABLE_NAME} where ${VentasDetMobileEntry.ID_VENTA} = $idVenta"
            var cursor = database.rawQuery(query, null)
            var detail: ArrayList<VentasDetalleReq> = ArrayList()

            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    detail.add(VentasDetalleReq(cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry._ID)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.ID_VENTA_DET)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.ID_VENTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.ID_PRODUCTO)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.IVA)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.CANTIDAD)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.PRECIO)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.PRECIO_VENTA)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.TOTAL_DET)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.EXENTO)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.GRAVADO_5)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.IVA_5)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.GRAVADO_10)),
                            cursor.getDouble(cursor.getColumnIndex(VentasDetMobileEntry.IVA_10)),
                            cursor.getString(cursor.getColumnIndex(VentasDetMobileEntry.FECHA_ALTA)),
                            cursor.getInt(cursor.getColumnIndex(VentasDetMobileEntry.USUARIO_ALTA)),
                            cursor.getString(cursor.getColumnIndex(VentasDetMobileEntry.PRODUCTO_CODIGO))))
                    cursor.moveToNext()
                }
            }
            cursor.close()
            database.close()

            return detail
        }

        fun updateSaleReimpresion(context: Context, idUsuario: Int, idVenta: Int){
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.writableDatabase
            val simpleDateTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val fechaHora = simpleDateTimeFormat.format(Date())
            var cantidad = getCantidadReimpresion(context, idVenta) + 1
            var values = ContentValues()
            values.put(VentasCabMobileEntry.REIMPRESION_FECHA_HORA, fechaHora)
            values.put(VentasCabMobileEntry.REIMPRESION_USUARIO, idUsuario)
            values.put(VentasCabMobileEntry.REIMPRESION_CANTIDAD, cantidad)
            var success = database.update(VentasCabMobileEntry.TABLE_NAME, values, "${VentasCabMobileEntry.ID_VENTA} = $idVenta", null)
        }

        fun getCantidadReimpresion(context: Context, idVenta: Int): Int {
            var dbHelper: DBHelper = DBHelper(context)
            var database: SQLiteDatabase = dbHelper.readableDatabase
            var query = "select ${VentasCabMobileEntry.REIMPRESION_CANTIDAD} from ${VentasCabMobileEntry.TABLE_NAME} where ${VentasCabMobileEntry.ID_VENTA} = $idVenta"
            var cursor = database.rawQuery(query, null)
            var cantidad = 0
            if (cursor.count > 0) {
                if (cursor.moveToLast()) cantidad = cursor.getInt(0)
            }
            cursor.close()
            database.close()
            return cantidad
        }

        fun getRegistroTablas(context: Context, query: String, args: Array<String>?): JSONArray? {
            val dbHelper = DBHelper(context)
            val database = dbHelper.readableDatabase
            val cursor = database.rawQuery(query, args)
            val cant = cursor.count
            if (cant > 0) {
                val resultSet = JSONArray()
                cursor.moveToFirst()
                val rowId = 0
                while (!cursor.isAfterLast) {
                    val totalColumn = cursor.columnCount
                    val rowObject = JSONObject()
                    for (i in 0 until totalColumn) {
                        if (cursor.getColumnName(i) != null) {
                            try {
                                if (cursor.getType(i) == Cursor.FIELD_TYPE_INTEGER) {
                                    rowObject.put(cursor.getColumnName(i), cursor.getInt(i))
                                } else if (cursor.getType(i) == Cursor.FIELD_TYPE_STRING) {
                                    rowObject.put(cursor.getColumnName(i), cursor.getString(i))
                                } else if (cursor.getType(i) == Cursor.FIELD_TYPE_FLOAT) {
                                    rowObject.put(cursor.getColumnName(i), cursor.getFloat(i).toDouble())
                                } else if (cursor.getType(i) == Cursor.FIELD_TYPE_BLOB) {
                                    rowObject.put(cursor.getColumnName(i), cursor.getBlob(i))
                                } else {
                                    rowObject.put(cursor.getColumnName(i), "")
                                }
                            } catch (e: Exception) {
                                Log.d("TAG_NAME", e.message)
                            }

                        }
                    }
                    resultSet.put(rowObject)
                    cursor.moveToNext()
                }

                cursor.close()
                database.close()
                Log.d("getRegistroTablas", resultSet.toString())
                return resultSet
            } else {
                return null
            }
        }

        fun getRegistroTablasGson(context: Context, query: String, args: Array<String>?): JsonArray? {
            val dbHelper = DBHelper(context)
            val database = dbHelper.readableDatabase
            val cursor = database.rawQuery(query, args)
            val cant = cursor.count
            if (cant > 0) {
                val resultSet = JsonArray()
                cursor.moveToFirst()
                val rowId = 0
                while (!cursor.isAfterLast) {
                    val totalColumn = cursor.columnCount
                    val rowObject = JsonObject()
                    for (i in 0 until totalColumn) {
                        if (cursor.getColumnName(i) != null) {
                            try {
                                if (cursor.getType(i) == Cursor.FIELD_TYPE_INTEGER) {
                                    rowObject.addProperty(cursor.getColumnName(i), cursor.getInt(i))
                                } else if (cursor.getType(i) == Cursor.FIELD_TYPE_STRING) {
                                    rowObject.addProperty(cursor.getColumnName(i), cursor.getString(i))
                                } else if (cursor.getType(i) == Cursor.FIELD_TYPE_FLOAT) {
                                    rowObject.addProperty(cursor.getColumnName(i), cursor.getFloat(i).toDouble())
                                } else {
                                    rowObject.addProperty(cursor.getColumnName(i), "")
                                }
                            } catch (e: Exception) {
                                Log.d("TAG_NAME", e.message)
                            }

                        }
                    }
                    resultSet.add(rowObject)
                    cursor.moveToNext()
                }

                cursor.close()
                database.close()
                Log.d("getRegistroTablas", resultSet.toString())
                return resultSet
            } else {
                return null
            }
        }

        fun vaciarTabla(context: Context, nombreTabla: String): Boolean {
            var resultado: Boolean
            val dbHelper = DBHelper(context)
            val database = dbHelper.writableDatabase
            try {
                database.execSQL("DELETE FROM $nombreTabla")
                resultado = true
            } catch (e: SQLException) {
                resultado = false
            }

            database.close()
            return resultado
        }

    }
}
