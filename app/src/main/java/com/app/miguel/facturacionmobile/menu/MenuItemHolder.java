package com.app.miguel.facturacionmobile.menu;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.miguel.facturacionmobile.R;

public class MenuItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView tvTitle;
    ImageView icon;
    CardView cardView;
    LinearLayout llContent;
    private MyClickListener listener;

    public MenuItemHolder(@NonNull View itemView, MyClickListener myClickListener) {
        super(itemView);
        tvTitle = itemView.findViewById(R.id.menu_title);
        icon = itemView.findViewById(R.id.menu_icon);
        cardView = itemView.findViewById(R.id.card_layout);
        llContent = itemView.findViewById(R.id.ll_content);
        this.listener = myClickListener;
        llContent.setOnClickListener(this);
    }

    public void setMenuDetails(MenuItem menuItem){
        tvTitle.setText(menuItem.getTitle());
        icon.setBackground(menuItem.getIcon());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_content:
                listener.onClickAction(this.getLayoutPosition());
        }
    }

    public interface MyClickListener {
        void onClickAction(int position);
    }
}
