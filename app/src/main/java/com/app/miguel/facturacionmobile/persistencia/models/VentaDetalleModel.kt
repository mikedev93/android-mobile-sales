package com.app.miguel.facturacionmobile.persistencia.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class VentaDetalleModel(@Expose @SerializedName("id_venta_det") var id_venta_det: Int?,
                        @Expose @SerializedName("id_venta") var id_venta: Int?,
                        @Expose @SerializedName("id_producto") var id_producto: Int,
                        @Expose @SerializedName("iva") var iva: Int,
                        @Expose @SerializedName("cantidad") var cantidad: Int,
                        @Expose @SerializedName("precio") var precio: Int,
                        @Expose @SerializedName("precio_venta") var precio_venta: Int,
                        @Expose @SerializedName("exento") var exento: Double,
                        @Expose @SerializedName("gravado_5") var gravado_5: Double,
                        @Expose @SerializedName("iva_5") var iva_5: Double,
                        @Expose @SerializedName("gravado_10") var gravado_10: Double,
                        @Expose @SerializedName("iva_10") var iva_10: Double,
                        @Expose @SerializedName("total_detalle") var total_detalle: Double,
                        @Expose @SerializedName("fecha_alta") var fecha_alta: String?,
                        @Expose @SerializedName("usuario_alta") var usuario_alta: Int,
                        @Expose @SerializedName("producto_codigo") var producto_codigo: String?) : Parcelable
@Parcelize
class ListVentaDetalle(@Expose @SerializedName("ventas_detalle")var ventasDetalle: ArrayList<VentaDetalleModel>) : Parcelable