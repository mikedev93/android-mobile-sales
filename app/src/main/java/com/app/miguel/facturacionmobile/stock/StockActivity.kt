package com.app.miguel.facturacionmobile.stock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.miguel.facturacionmobile.R

class StockActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock)

        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

        val fragment = StockFragment()
        this.supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(0, R.anim.slide_out_left, 0, R.anim.slide_out_right)
                .add(R.id.container, fragment, fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
                .commit()
    }

    override fun onBackPressed() {
        this.finish()
    }
}
