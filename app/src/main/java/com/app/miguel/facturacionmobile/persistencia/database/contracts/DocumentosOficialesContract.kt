package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class DocumentosOficialesContract {

    class DocumentosOficialesEntry : BaseColumns {
        companion object {
            const val TABLE_NAME: String = "mdocumentosoficiales"

            const val _ID: String = BaseColumns._ID
            const val ID_DOCUMENTO_OFICIAL: String = "iddocumentooficial"
            const val ID_PEDIDO_REMISION: String = "idpedido_remision"
            const val EMPRESA: String = "empresa"
            const val SUCURSAL: String = "sucursal"
            const val CAJA: String = "caja"
            const val TIMBRADO: String = "timbrado"
            const val VIGENTE_HASTA: String = "vigentehasta"
            const val OPERACION_NUMERO: String = "operacionnumero"
            const val DOCUMENTO_TIPO: String = "documentotipo" //FC=factura NC=nota de crédito ND=nota de débito RR=recibo de retención
            const val FECHA_DOCUMENTO: String = "fechadocumento"
            const val ID_DOCUMENTO_NUMERO: String = "iddocumentonumero"
            const val ESTADO_FISICO: String = "estadofisico" //1=libre 2 =impreso 3=anulado 4=anulado operativamente
            const val IMPRESION_FECHA_HORA: String = "impresionfechahora"
            const val IMPRESION_USUARIO: String = "impresionusuario"
            const val REIMPRESION_FECHA_HORA: String = "reimpresionfechahora"
            const val REIMPRESION_USUARIO: String = "reimpresionusuario"
            const val ANULACION_FECHA_HORA: String = "anulacionfechahora"
            const val ANULACION_USUARIO: String = "anulacionusuario"
            const val ALTA_FECHA_HORA: String = "altafechahora"
            const val ALTA_USUARIO: String = "altausuario"
            const val DIAS_VENCIMIENTO: String = "diasvencimiento"
            const val ENTIDAD_SUCURSAL: String = "entidadsucursal"
            const val BRUTO_EXENTO: String = "brutoexento"
            const val BRUTO_GRAVADO_5: String = "brutogravado5"
            const val BRUTO_IVA_5: String = "brutoiva5"
            const val BRUTO_GRAVADO_10: String = "brutogravado10"
            const val BRUTO_IVA_10: String = "brutoiva10"
            const val DESC_EXENTO: String = "descexento"
            const val DESC_GRAVADO_5: String = "descgravado5"
            const val DESC_IVA_5: String = "desciva5"
            const val DESC_GRAVADO_10: String = "descgravado10"
            const val DESC_IVA_10: String = "desciva10"
            const val NETO_EXENTO: String = "netoexento"
            const val NETO_GRAVADO_5: String = "netogravado5"
            const val NETO_IVA_5: String = "netoiva5"
            const val NETO_GRAVADO_10: String = "netogravado10"
            const val NETO_IVA_10: String = "netoiva10"
            const val TOTAL_PAGADO: String = "totalpagado"

        }
    }
}

/*
val FECHA_DOCUMENTO = "fechadocumento"
val ID_DOCUMENTO_NUMERO = "iddocumentonumero"
val ESTADO_FISICO = "estadofisico"
val IMPRESION_FECHA_HORA = "impresionfechahora"
val IMPRESION_USUARIO = "impresionusuario"
val REIMPRESION_FECHA_HORA = "reimpresionfechahora"
val REIMPRESION_USUARIO = "reimpresionusuario"
val ANULACION_FECHA_HORA = "anulacionfechahora"
val ANULACION_USUARIO = "anulacionusuario"
val ALTA_FECHA_HORA = "altafechahora"
val ALTA_USUARIO = "altausuario"
val DIAS_VENCIMIENTO = "diasvencimiento"
val ENTIDAD_SUCURSAL = "entidadsucursal"
val BRUTO_EXENTO = "brutoexento"
val BRUTO_GRAVADO_5 = "brutogravado5"
val BRUTO_IVA_5 = "brutoiva5"
val BRUTO_GRAVADO_10 = "brutogravado10"
val BRUTO_IVA_10 = "brutoiva10"
val DESC_EXENTO = "descexento"
val DESC_GRAVADO_5 = "descgravado5"
val DESC_IVA_5 = "desciva5"
val DESC_GRAVADO_10 = "descgravado10"
val DESC_IVA_10 = "desciva10"
val NETO_EXENTO = "netoexento"
val NETO_GRAVADO_5 = "netogravado5"
val NETO_IVA_5 = "netoiva5"
val NETO_GRAVADO_10 = "netogravado10"
val NETO_IVA_10 = "netoiva10"
val TOTAL_PAGADO = "totalpagado"
 */