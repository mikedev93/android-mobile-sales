package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class VentasDetContract {

    class VentasDetEntry : BaseColumns {
        companion object {
            const val TABLE_NAME: String = "mpedidosventadet"

            const val _ID = BaseColumns._ID

            const val ID_PEDIDO_VENTA_DET: String = "idpedidoventadet"
            const val ID_PEDIDO_VENTA: String = "pedidoventa"
            const val ID_PRODUCTO: String = "producto"
            const val IVA: String = "iva"
            const val CANTIDAD_UNIDADES: String = "cantidadunidades"//Siempre cero
            const val CANTIDAD_CAJAS: String = "cantidadcajas"
            const val LISTA_PRECIO: String = "listaprecio"
            const val PRECIO_LISTA: String = "preciolista"
            const val TIPO_DESCUENTO: String = "tipodescuento"
            const val VALOR_DESCUENTO: String = "valordescuento"
            const val MONTO_DESCUENTO: String = "montodescuento"
            const val PRECIO_FINAL: String = "preciofinal"
            const val EXENTO: String = "exento"
            const val GRAVADO_5: String = "gravado5"
            const val IVA_5: String = "iva5"
            const val GRAVADO_10: String = "gravado10"
            const val IVA_10: String = "iva10"
            const val COSTO: String = "costo"
            const val EMPRESA_FC: String = "empresafc"
            const val DEPSTK: String = "depstk"
            const val COMENTARIO: String = "comentario"
            const val ALTA_FECHA_HORA: String = "altafechahora"
            const val ALTA_USUARIO: String = "altausuario"
            const val PRODUCTO_CODIGO: String = "productocodigo"

        }
    }
}
