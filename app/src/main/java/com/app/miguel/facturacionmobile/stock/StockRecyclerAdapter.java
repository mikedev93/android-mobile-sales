package com.app.miguel.facturacionmobile.stock;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.miguel.facturacionmobile.R;
import com.app.miguel.facturacionmobile.persistencia.models.ProductModel;
import com.app.miguel.facturacionmobile.persistencia.models.ProductStockModel;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class StockRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<ProductStockModel> productList;

    public StockRecyclerAdapter(Context context, ArrayList<ProductStockModel> productList) {
        this.context = context;
        this.productList = productList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.view_product_stock_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final ProductStockModel product = productList.get(position);
        final ViewHolder holder = (ViewHolder) viewHolder;

        DecimalFormat formatter = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);

        holder.vTvCodigo.setText(product.getCodigo());
        holder.vTvNombre.setText(product.getNombre());
        holder.vTvPresentacion.setText(product.getPresentacion());
        holder.vTvCantidad.setText(formatter.format(product.getStock()));
        holder.vTvCantidadInicial.setText(formatter.format(product.getStockInicial()));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayout vLinearLayout;
        private final TextView vTvCodigo;
        private final TextView vTvNombre;
        private final TextView vTvPresentacion;
        private final TextView vTvCantidad;
        private final TextView vTvCantidadInicial;

        public ViewHolder(View itemView) {
            super(itemView);
            vLinearLayout = itemView.findViewById(R.id.ll_content);
            vTvCodigo = itemView.findViewById(R.id.tv_codigo);
            vTvNombre = itemView.findViewById(R.id.tv_descripcion);
            vTvPresentacion = itemView.findViewById(R.id.tv_presentacion);
            vTvCantidad = itemView.findViewById(R.id.tv_cantidad);
            vTvCantidadInicial = itemView.findViewById(R.id.tv_cantidad_inicial);
        }
    }

}
