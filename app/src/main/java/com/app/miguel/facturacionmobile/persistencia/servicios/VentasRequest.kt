package com.app.miguel.facturacionmobile.persistencia.servicios

import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasCabMobileContract
import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasCabMobileContract.VentasCabMobileEntry.Companion.ID_VENTA
import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasCabMobileContract.VentasCabMobileEntry.Companion._ID
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VentasRequest(@Expose @SerializedName("mpedidos") var pedidos: ArrayList<VentasCabRequest>?)

class VentasCabRequest(@Expose @SerializedName("id") var id: Int?,
                    @Expose @SerializedName("id_venta") var id_venta: Int?,
                    @Expose @SerializedName("id_pedido_remision") var id_pedido_remision: Int?,
                    @Expose @SerializedName("id_entidad") var id_entidad: Int?,
                    @Expose @SerializedName("id_entidad_sucursal") var id_entidad_sucursal: Int?,
                    @Expose @SerializedName("sucursal") var sucursal: Int?,
                    @Expose @SerializedName("empresa") var empresa: Int?,
                    @Expose @SerializedName("tipo_pedido") var tipoPedido: Int?,
                    @Expose @SerializedName("condicion_venta") var condicion_venta: Int?,
                    @Expose @SerializedName("fecha_emision") var fecha_emision: String?,
                    @Expose @SerializedName("nro_documento") var nro_documento: String?,
                    @Expose @SerializedName("fecha_vencimiento") var fecha_vencimiento: String?,
                    @Expose @SerializedName("fecha_alta") var fecha_alta: String?,
                    @Expose @SerializedName("usuario_alta") var usuario_alta: Int?,
                    @Expose @SerializedName("estado") var estado: String?,
                    @Expose @SerializedName("fecha_anulado") var fecha_anulado: String?,
                    @Expose @SerializedName("usuario_anulado") var usuario_anulado: Int?,
                    @Expose @SerializedName("bruto_exento") var bruto_exento: Long?,
                    @Expose @SerializedName("bruto_gravado_5") var bruto_gravado_5: Long?,
                    @Expose @SerializedName("bruto_iva_5") var bruto_iva_5: Long?,
                    @Expose @SerializedName("bruto_gravado_10") var bruto_gravado_10: Long?,
                    @Expose @SerializedName("bruto_iva_10") var bruto_iva_10: Long?,
                    @Expose @SerializedName("neto_exento") var neto_exento: Long?,
                    @Expose @SerializedName("neto_gravado_5") var neto_gravado_5: Long?,
                    @Expose @SerializedName("neto_iva_5") var neto_iva_5: Long?,
                    @Expose @SerializedName("neto_gravado_10") var neto_gravado_10: Long?,
                    @Expose @SerializedName("neto_iva_10") var neto_iva_10: Long?,
                    @Expose @SerializedName("total_pagado") var total_pagado: Long?,
                    @Expose @SerializedName("tipo_pago") var tipo_pago: Int?,
                    @Expose @SerializedName("reimpresion_fecha_hora") var reimpresionFechaHora: String?,
                    @Expose @SerializedName("reimpresion_usuario") var reimpresionUsuario: Int?,
                    @Expose @SerializedName("reimpresion_cantidad") var reimpresionCantidad: Int?,
                    @Expose @SerializedName("comentario") var comentario: String?,
                    @Expose @SerializedName("vendedor") var vendedor: Int?,
                    @Expose @SerializedName("detalle") var detalle: List<VentasDetalleReq>?)

class VentasDetalleReq(@Expose @SerializedName("_id") var id: Int?,
                       @Expose @SerializedName("id_venta_det") var id_venta_det: Int?,
                       @Expose @SerializedName("id_venta") var id_venta: Int?,
                       @Expose @SerializedName("id_producto") var id_producto: Int,
                       @Expose @SerializedName("iva") var iva: Int,
                       @Expose @SerializedName("cantidad") var cantidad: Int,
                       @Expose @SerializedName("precio") var precio: Int,
                       @Expose @SerializedName("precio_venta") var precio_venta: Int,
                       @Expose @SerializedName("total_det") var total_detalle: Double,
                       @Expose @SerializedName("exento") var exento: Double,
                       @Expose @SerializedName("gravado_5") var gravado_5: Double,
                       @Expose @SerializedName("iva_5") var iva_5: Double,
                       @Expose @SerializedName("gravado_10") var gravado_10: Double,
                       @Expose @SerializedName("iva_10") var iva_10: Double,
                       @Expose @SerializedName("fecha_alta") var fecha_alta: String?,
                       @Expose @SerializedName("usuario_alta") var usuario_alta: Int,
                       @Expose @SerializedName("producto_codigo") var producto_codigo: String?)