package com.app.miguel.facturacionmobile.persistencia.servicios.tasks

import android.content.Context
import android.os.AsyncTask
import com.app.miguel.facturacionmobile.SyncActivity
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.servicios.EntidadesRequest
import com.app.miguel.facturacionmobile.persistencia.servicios.NewEntidadesResponse
import com.app.miguel.facturacionmobile.persistencia.servicios.SyncAPI
import com.google.gson.JsonElement
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SyncNewEntidadesTask (var context: Context, var mainActivity: SyncActivity) : AsyncTask<Void, Void, String>() {

    private var syncroApi: SyncAPI? = null
    private var mRestAdapter: Retrofit? = null

    override fun doInBackground(vararg params: Void?): String {
        val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(40, TimeUnit.MINUTES)
                .writeTimeout(40, TimeUnit.MINUTES)
                .build()

        mRestAdapter = Retrofit.Builder()
                .baseUrl(Utils.getBaseURL(context)!!)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        syncroApi = mRestAdapter!!.create(SyncAPI::class.java)
        var entidades = EntidadesRequest(DBUtils.getNewEntidades(context))
        if (entidades.entidades!!.isNotEmpty()) {
            syncroApi!!.postNewClients(entidades).enqueue(object : retrofit2.Callback<List<NewEntidadesResponse>> {
                override fun onFailure(call: retrofit2.Call<List<NewEntidadesResponse>>?, t: Throwable?) {
                    mainActivity.showLoading(false)
                    mainActivity.showSnackbarError(t!!.message!!)
                }

                override fun onResponse(call: retrofit2.Call<List<NewEntidadesResponse>>?, response: Response<List<NewEntidadesResponse>>?) {
                    var servResponse = response!!.body()
                    for (item in servResponse){
                        DBUtils.updateEntidadSync(context, item)
                    }
                    Thread.sleep(2000)
                    mainActivity.syncVentas()
                }
            })
        } else {
            mainActivity.syncVentas()
        }

        return ""
    }
}