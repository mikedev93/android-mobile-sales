package com.app.miguel.facturacionmobile.persistencia.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ProductModel (@Expose @SerializedName("idproducto") var id: Int,
                    @Expose @SerializedName("nombre") var nombre: String,
                    @Expose @SerializedName("presentacion") var presentacion: String?,
                    @Expose @SerializedName("unidad_medida") var unidadMedida: Int,
                    @Expose @SerializedName("iva") var iva: Int,
                    @Expose @SerializedName("composicion_venta") var composicionVenta: Int,
                    @Expose @SerializedName("codigo") var codigo: String?,
                    @Expose @SerializedName("precio_venta") var precio_venta: Double,
                    @Expose @SerializedName("precio_unidad") var precioUnidad: Double?,
                    @Expose @SerializedName("precio_caja") var precioCaja: Double?,
                    @Transient @Expose @SerializedName("cantidad") var precioBase: Double?,
                    @Transient @Expose @SerializedName("cantidad") var cantidad: Int = 1,
                    @Transient @Expose @SerializedName("total_detalle") var total: Double = precio_venta,
                    @Expose var checked: Boolean = false) : Parcelable

@Parcelize
class ListProductModel(@Expose @SerializedName("productos") var productList: ArrayList<ProductModel>) : Parcelable