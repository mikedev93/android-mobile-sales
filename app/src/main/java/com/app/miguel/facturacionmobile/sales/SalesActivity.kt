package com.app.miguel.facturacionmobile.sales

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.app.miguel.facturacionmobile.LoginActivity
import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.persistencia.SalesManager
import com.app.miguel.facturacionmobile.persistencia.models.EntidadSucursalModel
import com.app.miguel.facturacionmobile.persistencia.models.GeoCiudadModel
import com.app.miguel.facturacionmobile.persistencia.models.ProductModel
import com.app.miguel.facturacionmobile.sales.clients.CityFinderFragment
import com.app.miguel.facturacionmobile.sales.clients.ClientFinderFragment
import com.app.miguel.facturacionmobile.sales.clients.NewClientFragment
import com.app.miguel.facturacionmobile.sales.products.ProductEditFragment
import com.app.miguel.facturacionmobile.sales.products.ProductFinderFragment
import kotlinx.android.synthetic.main.activity_sales.*

class SalesActivity : AppCompatActivity(), ProductFinderFragment.OnProductSelectedListener, ProductEditFragment.OnProductChangedListener,
        ClientFinderFragment.OnClientSelectedListener, NewClientFragment.OnClientCreatedListener, CityFinderFragment.OnCitySelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sales)

        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

        val fragment = SalesFragment()
        this.supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(0, R.anim.slide_out_left, 0, R.anim.slide_out_right)
                .add(R.id.container, fragment, SalesFragment::class.simpleName)
                .addToBackStack(SalesFragment::class.simpleName)
                .commit()
    }

    override fun onResume() {
        super.onResume()
        if (SalesManager.loggedUser == null) {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.putExtra("mensaje_error", "Sesión expirada")
            startActivity(intent)
        }
    }

    override fun productSelected(product: ProductModel) {
        var fragment = supportFragmentManager.findFragmentByTag(SalesFragment::class.simpleName) as SalesFragment
        if (fragment != null) {
            fragment.addProduct(product)
        }
    }

    override fun deleteProduct(product: ProductModel) {
        var fragment = supportFragmentManager.findFragmentByTag(SalesFragment::class.simpleName) as SalesFragment
        if (fragment != null) {
            fragment.removeProduct(product)
        }
    }

    override fun updateProduct(product: ProductModel, position: Int) {
        var fragment = supportFragmentManager.findFragmentByTag(SalesFragment::class.simpleName) as SalesFragment
        if (fragment != null) {
            fragment.updateProduct(product, position)
        }
    }

    override fun clientSelected(client: EntidadSucursalModel) {
        var fragment = supportFragmentManager.findFragmentByTag(SalesFragment::class.simpleName) as SalesFragment
        if (fragment != null) {
            fragment.updateClient(client)
        }
    }

    override fun clientCreated(client: EntidadSucursalModel) {
        var fragment = supportFragmentManager.findFragmentByTag(ClientFinderFragment::class.simpleName) as ClientFinderFragment
        if (fragment != null) {
            fragment.addClient(client)
        }
    }

    override fun citytSelected(ciudad: GeoCiudadModel) {
        var fragment = supportFragmentManager.findFragmentByTag(NewClientFragment::class.simpleName) as NewClientFragment
        if (fragment != null) {
            fragment.setCity(ciudad)
        }
    }

    override fun onBackPressed() {
//        var count = this.supportFragmentManager.backStackEntryCount
//        if (count == 1 && this.supportFragmentManager.findFragmentByTag(SalesFragment::class.simpleName)!!.isVisible) {
//            this.finish()
//        } else if (count > 1 && this.supportFragmentManager.findFragmentByTag(ProductEditFragment::class.simpleName) != null) {
//            if (this.supportFragmentManager.findFragmentByTag(ProductEditFragment::class.simpleName)!!.isVisible)
//                this.supportFragmentManager.popBackStackImmediate()
//            showCurtain(false)
//
//        } else {
//            this.supportFragmentManager.popBackStack()
//        }
        when{
            getVisibleFragment()!!::class.simpleName == SalesFragment::class.simpleName -> {
                SalesManager.selectedProductsMemory = null
                this.finish()
            }
            getVisibleFragment()!!::class.simpleName == ProductEditFragment::class.simpleName -> {
                this.supportFragmentManager.popBackStackImmediate()
                showCurtain(false)
            }
            else -> {
                this.supportFragmentManager.popBackStack()
            }
        }
    }

    fun showCurtain(show: Boolean) {
        rl_curtain.visibility = if (show) View.VISIBLE else View.GONE
        popup_container.bringToFront()
    }

    fun getVisibleFragment(): Fragment? {
        val fragmentManager = supportFragmentManager
        val fragments = fragmentManager.fragments
        if (fragments != null) {
            for (fragment in fragments) {
                if (fragment != null && fragment.isVisible)
                    if (fragments[fragments.size -1].tag == "SupportLifecycleFragmentImpl"){
                        return fragments[fragments.size-2]
                    } else {
                        return fragments[fragments.size - 1]
                    }
            }
        }
        return null
    }
}
