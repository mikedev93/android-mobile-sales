package com.app.miguel.facturacionmobile.sales

import android.app.Dialog
import androidx.fragment.app.DialogFragment
import android.widget.NumberPicker
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog


class NumberPickerDialog: DialogFragment() {

    private var valueChangeListener: NumberPicker.OnValueChangeListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val numberPicker = NumberPicker(activity)

        numberPicker.minValue = 1
        numberPicker.maxValue = 30

        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Dias de Vencimiento")
        builder.setMessage("Elija cantidad de días:")

        builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
            valueChangeListener!!.onValueChange(numberPicker,
                    numberPicker.value, numberPicker.value)
        })

        builder.setNegativeButton("CANCELAR", DialogInterface.OnClickListener { dialog, which ->
            valueChangeListener!!.onValueChange(numberPicker,
                    numberPicker.value, numberPicker.value)
        })

        builder.setView(numberPicker)
        return builder.create()
    }

    fun getValueChangeListener(): NumberPicker.OnValueChangeListener? {
        return valueChangeListener
    }

    fun setValueChangeListener(valueChangeListener: NumberPicker.OnValueChangeListener) {
        this.valueChangeListener = valueChangeListener
    }
}