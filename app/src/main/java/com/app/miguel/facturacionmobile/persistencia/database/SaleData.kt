package com.app.miguel.facturacionmobile.persistencia.database

import android.os.Parcelable
import com.app.miguel.facturacionmobile.persistencia.models.Client
import com.app.miguel.facturacionmobile.persistencia.models.EntidadSucursalModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class SaleData (@Expose @SerializedName("client") var client: EntidadSucursalModel?,
                @Expose @SerializedName("nro_factura") var factura: String,
                @Expose @SerializedName("vencimiento") var vencimiento: String,
                @Expose @SerializedName("estado") var estado: String,
                @Expose @SerializedName("tipo_venta") var condicionVenta: Int,
                @Expose @SerializedName("id_venta") var id_venta: Int) : Parcelable