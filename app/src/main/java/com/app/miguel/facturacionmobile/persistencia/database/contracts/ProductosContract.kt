package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class ProductosContract {

    class ProductosEntry : BaseColumns {
        companion object {
            const val TABLE_NAME: String = "mproductos"

            const val _ID = BaseColumns._ID
            const val ID_PRODUCTO: String = "idproducto"
            const val CODIGO: String = "codigo"
            const val NOMBRE: String = "nombre"
            const val PRESENTACION: String = "presentacion"
            const val UNIDAD_MEDIDA: String = "unidadmedida"
            const val IVA: String = "iva"
            const val STOCK: String = "stock"
            const val STOCK_INICIAL: String = "stock_inicial"
            const val COMPOSICION_VENTA: String = "composicion_venta"
            const val ID_PEDIDO_REMISION: String = "idpedido_remision"
            const val PRECIO_UNIDAD: String = "precio_unidad"
            const val PRECIO_CAJA: String = "precio_fabrica"
        }
    }
}