package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class VentasCabContract {

    class VentasCabEntry : BaseColumns {
        companion object {
            const val TABLE_NAME: String = "mpedidosventa"

            const val _ID = BaseColumns._ID
            const val ID_PEDIDO_VENTA: String = "idpedidoventa"
            const val ENTIDAD_SUCURSAL: String = "entidadsucursal"
            const val PEDIDO_FECHA: String = "pedidofecha"
            const val VENDEDOR: String = "vendedor"
            const val CONDICION_VENTA: String = "condicionventa" //Contado = 1, Cŕedito = 2
            const val DOCUMENTO_PAGO: String = "documentopago"
            const val NRO_SUCURSAL_CAMBIAR: String = "nro_sucursal_ajustar"
            const val NRO_PUNTO_EXPEDICION_CAMBIAR: String = "nro_punto_expedicion_ajustar"
            const val NRO_FACTURA_CAMBIAR: String = "nro_factura_ajustar"
            const val DIAS_VENCIMIENTO_FACTURA: String = "diasvencimientofactura"
            const val PEDIDO_COMPLETO_FECHA_HORA: String = "pedidocompletofechahora"
            const val PEDIDO_COMPLETO_USUARIO: String = "pedidocompletousuario"
            const val ALTA_FECHA_HORA: String = "altafechahora"
            const val ALTA_USUARIO: String = "altausuario"
            const val ANULADO_FECHA_HORA: String = "anuladofechahora"
            const val ANULADO_USUARIO: String = "anuladousuario"
            const val CONFIRMADO_FECHA_HORA: String = "confirmadofechahora"
            const val CONFIRMADO_USUARIO: String = "confirmadousuario"
            const val APROBADO_FECHA_HORA: String = "aprobadofechahora"
            const val APROBADO_USUARIO: String = "aprobadousuario"
            const val COMENTARIO: String = "comentario"
        }
    }
}

/*
idpedidoventa
entidadsucursal
pedidofecha
vendedor
condicionventa
documentopago
diasvencimientofactura
altafechahora
altausuario
anuladofechahora
anuladousuario
aprobadofechahora
aprobadousuario
 */