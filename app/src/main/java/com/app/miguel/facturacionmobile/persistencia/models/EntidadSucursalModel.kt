package com.app.miguel.facturacionmobile.persistencia.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class EntidadSucursalModel (@Expose @SerializedName("idEntidadSucursal")var identidadsucursal: Int,
                            @Expose @SerializedName("entidad")var entidad: Int?,
                            @Expose @SerializedName("razonSocial") var razonSocial: String?,
                            @Expose @SerializedName("ruc") var ruc: String?,
                            @Expose @SerializedName("nombreComercial")var nombrecomercial: String?,
                            @Expose @SerializedName("tipoCliente")var tipocliente: Int?,
                            @Expose @SerializedName("vendedor")var vendedor: Int?,
                            @Expose @SerializedName("estado")var estado: Int?,
                            @Expose @SerializedName("departamento")var departamento: Int?,
                            @Expose @SerializedName("ciudad")var ciudad: Int,
                            @Expose @SerializedName("barrio")var barrio: Int,
                            @Expose @SerializedName("direccion")var direccion: String?,
                            @Expose @SerializedName("telefono")var telefono: String?,
                            @Expose @SerializedName("idPedidoRemision")var idpedido_remision: Int,
                            @Expose @SerializedName("altaFechaHora")var altafechahora: String?,
                            @Expose @SerializedName("altaUsuario")var altausuario: Int?,
                            @Expose @SerializedName("modificadoFechaHora")var modificadofechahora: String?,
                            @Expose @SerializedName("modificadoUsuario")var modificadousuario: Int?,
                            @Expose var checked: Boolean = false) : Parcelable


class ListEntidadSucursal(@Expose @SerializedName("entidad_sucursales")var entidadSucursales: ArrayList<EntidadSucursalModel>)
