package com.app.miguel.facturacionmobile.sales.clients;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.miguel.facturacionmobile.R;
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils;
import com.app.miguel.facturacionmobile.persistencia.models.Client;
import com.app.miguel.facturacionmobile.persistencia.models.EntidadSucursalModel;
import com.app.miguel.facturacionmobile.sales.SalesActivity;

import java.util.ArrayList;

public class ClientFinderRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<EntidadSucursalModel> clients;
    private int rowIndex = -1;
    private SalesActivity mainActivity;
    private ClientFinderClickListener mListener;

    public ClientFinderRecyclerAdapter(Context context, ArrayList<EntidadSucursalModel> clients, ClientFinderClickListener mListener) {
        this.context = context;
        this.clients = clients;
        this.mListener = mListener;
        this.mainActivity = (SalesActivity) context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.view_find_client_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final EntidadSucursalModel client = clients.get(position);
        final ViewHolder holder = (ViewHolder) viewHolder;

        holder.vTvCodigo.setText(String.valueOf(client.getIdentidadsucursal()));
        holder.vTvNombre.setText(client.getNombrecomercial());
        holder.vTvRuc.setText(client.getRuc());

        holder.vLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rowIndex = position;
                notifyDataSetChanged();
            }
        });

        if (rowIndex == position) {
            if (client.getChecked()) {
                holder.viewSeparador.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                holder.vLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.vTvCodigo.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
                holder.vTvNombre.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
                holder.vTvRuc.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));

                client.setChecked(false);
                unselectClient(client);

                mListener.onClick(null);
            } else {
                holder.viewSeparador.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.vLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                holder.vTvCodigo.setTextColor(context.getResources().getColor(R.color.white));
                holder.vTvNombre.setTextColor(context.getResources().getColor(R.color.white));
                holder.vTvRuc.setTextColor(context.getResources().getColor(R.color.white));

                client.setChecked(true);
                unselectClient(client);

                mListener.onClick(client);
            }
        } else {
            holder.viewSeparador.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.vLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));

            holder.vTvCodigo.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
            holder.vTvNombre.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
            holder.vTvRuc.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
        }
    }

    private void unselectClient(EntidadSucursalModel client) {
        if (client != null) {
            for (int index = 0; index < clients.size(); index++) {
                if (!clients.get(index).equals(client)) {
                    clients.get(index).setChecked(false);
                }
            }
        } else {
            for (int index = 0; index < clients.size(); index++) {
                clients.get(index).setChecked(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return clients.size();
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayout vLinearLayout;
        private final TextView vTvCodigo;
        private final TextView vTvNombre;
        private final TextView vTvRuc;
        private final View viewSeparador;

        public ViewHolder(View itemView) {
            super(itemView);
            vLinearLayout = itemView.findViewById(R.id.ll_content);
            vTvCodigo = itemView.findViewById(R.id.tv_codigo);
            vTvNombre = itemView.findViewById(R.id.tv_nombre_cliente);
            vTvRuc = itemView.findViewById(R.id.tv_ruc);
            viewSeparador = itemView.findViewById(R.id.separador);
        }
    }

    public interface ClientFinderClickListener {
        void onClick(EntidadSucursalModel client);
    }
}
