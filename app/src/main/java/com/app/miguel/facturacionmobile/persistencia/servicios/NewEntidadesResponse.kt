package com.app.miguel.facturacionmobile.persistencia.servicios

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NewEntidadesResponse (@Expose @SerializedName("_id") var id: Int?,
                            @Expose @SerializedName("identidadSucursal") var idEntidadSucursal: Int?,
                            @Expose @SerializedName("success") var success: Boolean?)