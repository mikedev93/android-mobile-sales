package com.app.miguel.facturacionmobile.cancelInvoice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.app.miguel.facturacionmobile.R
import com.app.miguel.facturacionmobile.persistencia.database.SaleData
import kotlinx.android.synthetic.main.activity_cancel_invoice.*

class CancelInvoiceActivity : AppCompatActivity(), CancelInvoicePopupFragment.OnSaleItemCancelledListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cancel_invoice)

        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

        val fragment = CancelInvoiceFragment()
        this.supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(0, R.anim.slide_out_left, 0, R.anim.slide_out_right)
                .add(R.id.container, fragment, fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
                .commit()
    }

    override fun cancelInvoice(saleData: SaleData, position: Int) {
        var fragment = supportFragmentManager.findFragmentByTag(CancelInvoiceFragment::class.simpleName) as CancelInvoiceFragment
        if (fragment != null) {
            fragment.cancelInvoice(saleData, position)
        }
    }

    override fun onBackPressed() {
        if (this.supportFragmentManager.backStackEntryCount > 1) {
            this.supportFragmentManager.popBackStack()
            showCurtain(false)
        } else {
            this.finish()
        }
    }

    fun showCurtain(show: Boolean) {
        rl_curtain.visibility = if (show) View.VISIBLE else View.GONE
        popup_container.bringToFront()
    }
}
