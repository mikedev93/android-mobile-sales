package com.app.miguel.facturacionmobile.persistencia.database.contracts

import android.provider.BaseColumns

class ParametroVentaContract {

    class ParametroVentasEntry : BaseColumns {
        companion object {
            const val TABLE_NAME: String = "mparam_vta_mobile"

            const val _ID = BaseColumns._ID
            const val ID_PARAM_VTA_MOBILE: String = "idparam_vta_mobile"
            const val ID_PEDIDO_REMISION: String = "idpedido_remision"
            const val NRO_INI_FACT_AUTO: String = "nroini_fcauto"
            const val NRO_DESDE_FACT_MANUAL: String = "nrodesde_fcmanual"
            const val NRO_HASTA_FACT_MANUAL: String = "nrohasta_fcmanual"
            const val VENDEDOR: String = "vendedor"
            const val ID_SUCURSAL: String = "id_sucursal"
            const val CAJA: String = "caja"
            const val ID_EMPRESA: String = "empresa"
            const val TIMBRADO: String = "timbrado"
            const val VALIDO_DESDE: String = "valido_desde"
            const val VALIDO_HASTA: String = "valido_hasta"
            const val PORCENTAJE_VARIACION_PRECIO: String = "porcentaje_variacion_precio"
            const val DIRECCION: String = "direccion"
            const val PAIS: String = "pais"
            const val DEPARTAMENTO: String = "departamento"
            const val CIUDAD: String = "ciudad"
            const val RUC: String = "ruc"
            const val RAZON_SOCIAL: String = "razon_social"
            const val TIPO_PEDIDO_VENTA: String = "tipo_pedido_venta"
            const val TELEFONO: String = "telefono"
            const val ULTIMO_NRO_FACTURA: String = "nro_factura"
        }
    }
}
