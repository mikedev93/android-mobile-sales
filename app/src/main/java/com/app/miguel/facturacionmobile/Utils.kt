package com.app.miguel.facturacionmobile

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.URLUtil
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar


class Utils {

    companion object {
        const val TYPE_ERROR = "error"
        const val TYPE_WARNING = "warning"
        const val TYPE_INFO = "info"
        const val TYPE_DEMO = "demo"
        const val TYPE_SYNC = "sync"
        const val NO_DATA_REMISION_ERROR = "¡No existen datos para ese ID Operativo Venta!"

        fun saveDefaultServerConf(context: Context) {
            val sharedPreferences = context.getSharedPreferences(context.resources.getString(R.string.preference_file_key_config_data), Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putBoolean(context.resources.getString(R.string.other_config_key), false)
            editor.putString(context.resources.getString(R.string.host_key), context.resources.getString(R.string.default_host))
            editor.putString(context.resources.getString(R.string.port_key), context.resources.getString(R.string.default_port))
            editor.putString(context.resources.getString(R.string.service_key), context.resources.getString(R.string.default_service))
            editor.commit()
        }

        fun getBaseURL(context: Context): String? {
            val sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key_config_data), Context.MODE_PRIVATE)
            val host = sharedPreferences.getString(context.resources.getString(R.string.host_key), null)
            val port = sharedPreferences.getString(context.resources.getString(R.string.port_key), null)
            var service = sharedPreferences.getString(context.resources.getString(R.string.service_key), null)
            if (host != null && port != null && service != null) {
                if (service.isNotEmpty()) service += "/"
                var url = "$host:$port/$service"
                if (!URLUtil.isValidUrl(url)) {
                    url = "http://$url"
                }
                return url
            }
            return null
        }

        fun getNroFacturaFormatted(rawDocumentNumber: String): String {
            var documentNumber = rawDocumentNumber.replace("-", "").trim()
            var numeroFact = extraerNroFactura(documentNumber)
            var puntoVenta = extraerPuntoVenta(documentNumber)
            var sucursal = extraerSucursal(documentNumber)
            var nroFactuarDisplay = "$sucursal-$puntoVenta-$numeroFact"
            return nroFactuarDisplay
        }

        fun extraerNroFactura(nroFacturaCompleto: String): String {
            return nroFacturaCompleto.substring(nroFacturaCompleto.length - 7)
        }

        fun extraerPuntoVenta(nroFacturaCompleto: String): String {
            var nroFactura = nroFacturaCompleto.substring(nroFacturaCompleto.length - 7)
            var puntoVentaSucursal = nroFacturaCompleto.replace(nroFactura, "", true)
            var puntoVenta = puntoVentaSucursal.substring(puntoVentaSucursal.length - 3)
            return puntoVenta
        }

        fun extraerSucursal(nroFacturaCompleto: String): String {
            var nroFactura = nroFacturaCompleto.substring(nroFacturaCompleto.length - 7)
            var puntoVentaSucursal = nroFacturaCompleto.replace(nroFactura, "", true)
            var puntoVenta = puntoVentaSucursal.substring(puntoVentaSucursal.length - 3)
            var sucursal = puntoVentaSucursal.replace(puntoVenta, "")
            var sucursalFormated = String.format("%3s", sucursal).replace(' ', '0')
            return sucursalFormated
        }

        fun showSnackbar(context: Context, view: View, message: String, type: String?) {
            var type = type
            val backgroundColor: Int
            val textColor: Int
            val duration: Int
            if (type == null) type = ""
            when (type) {
                TYPE_ERROR -> {
                    backgroundColor = ContextCompat.getColor(context, R.color.warning)
                    duration = Snackbar.LENGTH_LONG
                    textColor = ContextCompat.getColor(context, R.color.white)
                }
                TYPE_WARNING -> {
                    backgroundColor = ContextCompat.getColor(context, R.color.colorPrimary)
                    duration = Snackbar.LENGTH_LONG
                    textColor = ContextCompat.getColor(context, R.color.white)
                }
                TYPE_INFO -> {
                    backgroundColor = ContextCompat.getColor(context, R.color.indigo)
                    duration = Snackbar.LENGTH_LONG
                    textColor = ContextCompat.getColor(context, R.color.white)
                }
                TYPE_DEMO, TYPE_SYNC -> {
                    backgroundColor = ContextCompat.getColor(context, R.color.indigo)
                    duration = Snackbar.LENGTH_LONG
                    textColor = ContextCompat.getColor(context, R.color.white)
                }
                else -> {
                    backgroundColor = ContextCompat.getColor(context, R.color.colorPrimaryDark)
                    duration = Snackbar.LENGTH_SHORT
                    textColor = ContextCompat.getColor(context, R.color.white)
                }
            }
            val snackbar = Snackbar.make(view, message, duration)
            val snackView = snackbar.view
            snackView.setBackgroundColor(backgroundColor)
            snackbar.show()
        }

        fun showSnackbar(context: Context, view: View, message: String, type: String?, callback: Snackbar.Callback) {
            var type = type
            val backgroundColor: Int
            val textColor: Int
            val duration: Int
            if (type == null) type = ""
            when (type) {
                TYPE_ERROR -> {
                    backgroundColor = ContextCompat.getColor(context, R.color.warning)
                    duration = Snackbar.LENGTH_LONG
                    textColor = ContextCompat.getColor(context, R.color.white)
                }
                TYPE_WARNING -> {
                    backgroundColor = ContextCompat.getColor(context, R.color.colorPrimary)
                    duration = Snackbar.LENGTH_LONG
                    textColor = ContextCompat.getColor(context, R.color.white)
                }
                TYPE_INFO -> {
                    backgroundColor = ContextCompat.getColor(context, R.color.indigo)
                    duration = Snackbar.LENGTH_LONG
                    textColor = ContextCompat.getColor(context, R.color.white)
                }
                TYPE_DEMO, TYPE_SYNC -> {
                    backgroundColor = ContextCompat.getColor(context, R.color.indigo)
                    duration = Snackbar.LENGTH_LONG
                    textColor = ContextCompat.getColor(context, R.color.white)
                }
                else -> {
                    backgroundColor = ContextCompat.getColor(context, R.color.colorPrimaryDark)
                    duration = Snackbar.LENGTH_SHORT
                    textColor = ContextCompat.getColor(context, R.color.white)
                }
            }
            val snackbar = Snackbar.make(view, message, duration)
            val snackView = snackbar.view
            snackView.setBackgroundColor(backgroundColor)
            snackbar.addCallback(callback)
            snackbar.show()
        }

        fun padRight(text: String, paddingLenght: Int): String {
            return String.format("%-${paddingLenght}s", text)
        }

        fun padLeft(text: String, paddingLenght: Int): String {
            return String.format("%${paddingLenght}s", text);
        }

        fun formatLineCenter(text: String): String {
            var maxCharactersPrinter = 30
            var availablePadding = maxCharactersPrinter - text.length
            if (availablePadding < 0) {
                //Multiple lines
                return text
            } else {
                var padding: Int = availablePadding / 2
                var paddedText = " "
                paddedText = paddedText.repeat(padding)
                var padText = paddedText + text
                return padText
            }
        }

        fun formatLineLeftRight(textLeft: String, textRight: String): String {
            var maxCharactersPrinter = 30
            var availablePadding = maxCharactersPrinter - (textLeft.length + textRight.length)
            var paddedText = " "
            paddedText = paddedText.repeat(availablePadding)
            return textLeft.plus(paddedText).plus(textRight)
        }

        fun formatLineLeftRight(textLeft: String, textRight: String, maxCharacters: Int): String {
            var availablePadding = maxCharacters - (textLeft.length + textRight.length)
            if (availablePadding < 0) availablePadding = 0
            var paddedText = " ".repeat(availablePadding)
            return textLeft.plus(paddedText).plus(textRight)
        }

        fun fitTextInChars(text: String, availableChars: Int): String {
            var textLengthFilter = text.length
            if (textLengthFilter > availableChars) {
                var desiredText = text.substring(0, availableChars) + "\n" + text.substring(availableChars)
                if (desiredText.length > availableChars * 2) {
                    desiredText = desiredText.substring(0, availableChars * 2)
                }
                return desiredText
            } else {
                return text
            }
        }

        fun hideSoftKeyboard(activity: Activity) {
            val inputMethodManager: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus.windowToken, 0)
        }
    }
}