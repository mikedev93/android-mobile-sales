package com.app.miguel.facturacionmobile.persistencia.servicios.tasks

import android.content.Context
import android.os.AsyncTask
import com.app.miguel.facturacionmobile.HomeActivity
import com.app.miguel.facturacionmobile.SyncActivity
import com.app.miguel.facturacionmobile.Utils
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.database.contracts.ParametroVentaContract
import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasCabMobileContract
import com.app.miguel.facturacionmobile.persistencia.database.contracts.VentasDetMobileContract
import com.app.miguel.facturacionmobile.persistencia.models.ParametroVentaModel
import com.app.miguel.facturacionmobile.persistencia.servicios.SyncAPI
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SyncParametersTask(var context: Context, var mainActivity: SyncActivity, var idRemision: Int): AsyncTask<Void, Void, String>(){

    private var syncroApi: SyncAPI? = null
    private var mRestAdapter: Retrofit? = null

    override fun doInBackground(vararg params: Void?): String {
        val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(40, TimeUnit.MINUTES)
                .writeTimeout(40, TimeUnit.MINUTES)
                .build()

        mRestAdapter = Retrofit.Builder()
                .baseUrl(Utils.getBaseURL(context)!!)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        syncroApi = mRestAdapter!!.create(SyncAPI::class.java)
        var paramResponse = syncroApi!!.getParamVta(idRemision)
        paramResponse.enqueue(object : retrofit2.Callback<List<ParametroVentaModel>> {
            override fun onFailure(call: retrofit2.Call<List<ParametroVentaModel>>?, t: Throwable?) {
                mainActivity.showLoading(false)
                mainActivity.showSnackbarError(t!!.message!!)
            }

            override fun onResponse(call: retrofit2.Call<List<ParametroVentaModel>>?, response: Response<List<ParametroVentaModel>>?) {
                var paramResponse = response!!.body()
                if (paramResponse != null && paramResponse.isNotEmpty()) {
                    if (paramResponse != null) {
                        DBUtils.vaciarTabla(context, ParametroVentaContract.ParametroVentasEntry.TABLE_NAME)
                        DBUtils.vaciarTabla(context, VentasCabMobileContract.VentasCabMobileEntry.TABLE_NAME)
                        DBUtils.vaciarTabla(context, VentasDetMobileContract.VentasDetMobileEntry.TABLE_NAME)
                    }
                    DBUtils.saveSaleParameters(context, paramResponse[0])
                    Thread.sleep(1000)
                    mainActivity.syncProductos(idRemision)
                } else {
                    Thread.sleep(1000)
                    mainActivity.showLoading(false)
                    mainActivity.showSnackbarError(Utils.NO_DATA_REMISION_ERROR)
                }
            }
        })

        return ""
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
    }
}