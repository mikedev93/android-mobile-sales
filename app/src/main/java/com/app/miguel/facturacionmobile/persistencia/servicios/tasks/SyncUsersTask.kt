package com.app.miguel.facturacionmobile.persistencia.servicios.tasks

import android.content.Context
import android.os.AsyncTask
import com.app.miguel.facturacionmobile.BuildConfig
import com.app.miguel.facturacionmobile.LoginActivity
import com.app.miguel.facturacionmobile.Utils.Companion.getBaseURL
import com.app.miguel.facturacionmobile.persistencia.database.DBUtils
import com.app.miguel.facturacionmobile.persistencia.database.contracts.UsuariosContract
import com.app.miguel.facturacionmobile.persistencia.models.UsuarioModel
import com.app.miguel.facturacionmobile.persistencia.servicios.SyncAPI
import okhttp3.OkHttpClient
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SyncUsersTask (context: Context, mainActivity: LoginActivity): AsyncTask<Void, Void, String>(){

    private var syncroApi: SyncAPI? = null
    private var mRestAdapter: Retrofit? = null
    var mContext: Context = context
    var mainActivity = mainActivity

    override fun doInBackground(vararg params: Void?): String {

        val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(40, TimeUnit.MINUTES)
                .writeTimeout(40, TimeUnit.MINUTES)
                .build()

        mRestAdapter = Retrofit.Builder()
                .baseUrl(getBaseURL(mContext)!!)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        syncroApi = mRestAdapter!!.create(SyncAPI::class.java)
        var userResponse = syncroApi!!.getUsuarios()
        userResponse.enqueue(object : Callback<List<UsuarioModel>>{
            override fun onFailure(call: retrofit2.Call<List<UsuarioModel>>?, t: Throwable?) {
                mainActivity.showSnackbarError(t!!.message!!)
            }

            override fun onResponse(call: retrofit2.Call<List<UsuarioModel>>?, response: Response<List<UsuarioModel>>?) {
                var userResponse = response!!.body()
                if (userResponse.size > 0) {
                    DBUtils.vaciarTabla(mContext, UsuariosContract.UsuariosEntry.TABLE_NAME)
                    for (item in userResponse) {
                        DBUtils.saveUsuario(mContext, item)
                    }
                    if (BuildConfig.DEMO_USER) {
                        var usuarioDemo = UsuarioModel(114, "DEMO", "Demo Demo", "DEMO", null, 5019, "A")
                        DBUtils.saveUsuario(mContext, usuarioDemo)
                    }
                    Thread.sleep(1000)
                    mainActivity.showLoading(false)
                    mainActivity.showSnackbar("Usuarios sincronizados con éxito")
                }
            }
        })

        return ""
    }
}