package com.app.miguel.facturacionmobile.sales.clients;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.miguel.facturacionmobile.R;
import com.app.miguel.facturacionmobile.persistencia.models.GeoCiudadModel;
import com.app.miguel.facturacionmobile.sales.SalesActivity;

import java.util.ArrayList;

public class CityFinderRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<GeoCiudadModel> ciudades;
    private int rowIndex = -1;
    private SalesActivity mainActivity;
    private CityFinderClickListener mListener;

    public CityFinderRecyclerAdapter(Context context, ArrayList<GeoCiudadModel> ciudades, CityFinderClickListener mListener) {
        this.context = context;
        this.ciudades = ciudades;
        this.mListener = mListener;
        this.mainActivity = (SalesActivity) context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.view_find_city_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final GeoCiudadModel ciudad = ciudades.get(position);
        final ViewHolder holder = (ViewHolder) viewHolder;

        holder.vTvCiudad.setText(String.valueOf(ciudad.getCiudadDescripcion()));
        holder.vTvDepartamento.setText(ciudad.getDepartamentoDescripcion());

        holder.vLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rowIndex = position;
                notifyDataSetChanged();
            }
        });

        if (rowIndex == position) {
            if (ciudad.getChecked()) {
                holder.vLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.vTvCiudad.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
                holder.vTvDepartamento.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));

                ciudad.setChecked(false);
                unselectCiudad(ciudad);

                mListener.onClick(null);
            } else {
                holder.vLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                holder.vTvCiudad.setTextColor(context.getResources().getColor(R.color.white));
                holder.vTvDepartamento.setTextColor(context.getResources().getColor(R.color.white));

                ciudad.setChecked(true);
                unselectCiudad(ciudad);

                mListener.onClick(ciudad);
            }
        } else {
            holder.vLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));

            holder.vTvCiudad.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
            holder.vTvDepartamento.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_light));
        }
    }

    private void unselectCiudad(GeoCiudadModel ciudad) {
        if (ciudad != null) {
            for (int index = 0; index < ciudades.size(); index++) {
                if (!ciudades.get(index).equals(ciudad)) {
                    ciudades.get(index).setChecked(false);
                }
            }
        } else {
            for (int index = 0; index < ciudades.size(); index++) {
                ciudades.get(index).setChecked(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return ciudades.size();
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayout vLinearLayout;
        private final TextView vTvCiudad;
        private final TextView vTvDepartamento;

        public ViewHolder(View itemView) {
            super(itemView);
            vLinearLayout = itemView.findViewById(R.id.ll_content);
            vTvCiudad = itemView.findViewById(R.id.tv_ciudad);
            vTvDepartamento = itemView.findViewById(R.id.tv_departamento);
        }
    }

    public interface CityFinderClickListener {
        void onClick(GeoCiudadModel ciudad);
    }
}
